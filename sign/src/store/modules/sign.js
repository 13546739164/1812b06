import { getSignList,getSignDetail } from "@/services";

const state = {
    signList: [],
    signDetail: {}
}

const mutations = {
    update(state, payload){
        for (let key in payload) {
            state[key] = payload[key];
        }
    }
};

const actions = {
    async getSignList({commit, state}, payload){
        let res = await getSignList(payload);
        if (res.code === 0){
            let signList = res.data;
            if (payload.page && payload.page > 1){
                signList = [...state.signList, ...signList];
            }
            commit('update', {
                signList
            });
        }
    },
    async getSignDetail({commit, state}, payload){
        let res = await getSignDetail(payload);
        if (res.code === 0){
            commit('update', {
                signDetail: res.data
            });
        }
    }
}

export default {
    namespaced: true,
    state,
    mutations,
    actions
}
