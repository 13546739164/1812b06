//防抖函数
export function debounce(fn, delay = 300) {
    let timer = 0;
    return function () {
        clearTimeout(timer)
        timer = setTimeout(() => {
            fn(...arguments)
        }, delay)
    }
}
//节流函数
export function throttle(fn, delay = 300) {
    let start = +new Date();
    return function () {
        let now = +new Date();
        if (now - start > delay) {
            fn(...arguments);
            start = now
        }
    }
}