import request from '@/utils/request';

// 获取面试列表
export function getSignList(params){
    return request.get('/sign', params)
}

// 获取面试详情
export function getSignDetail(id){
    return request.get(`/sign/${id}`)
}
