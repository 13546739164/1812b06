import request from '@/utils/request';

// 用户登陆
export function userLogin(code){
    return request.post('/user/code2session', {code})
}

// 解密数据
export function decrypt(data){
    return request.post('/user/decrypt', data);
}

// 微信支付
export function pay(total_fee){
    return request.post('/sign/pay', {total_fee});
}