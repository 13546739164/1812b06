import { defineConfig } from 'umi';
export default defineConfig({
  nodeModulesTransform: {
    type: 'none',
  },
  fastRefresh: {},
  // 开启antd
  antd: {
  },
    // 引入dva
    dva: {
      immer: true,
      hmr: true,
    }
});
