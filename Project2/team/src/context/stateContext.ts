import React from 'react';
import store from '@/models/index'

export default React.createContext(store);