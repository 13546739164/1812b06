import stateContext from "./stateContext";
import {useContext} from 'react';

export default function(){
    return useContext(stateContext)
};