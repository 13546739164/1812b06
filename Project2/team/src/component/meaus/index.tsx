import  React,{FC} from 'react'
import {  Menu} from 'antd';
import {
    PieChartOutlined,
    UserOutlined,
  } from '@ant-design/icons';
import {data} from '@/assets/data'
import { Link } from 'umi';
const { SubMenu } = Menu;

const Meaus:FC=()=>{
    return(
        <Menu theme="dark" defaultSelectedKeys={['1']} mode="inline">
        {
            data.map((item,index)=>{
               return !item.children? <Menu.Item key={index} icon={<PieChartOutlined />}>
                <Link to={item.path}>{item.name}</Link>
              </Menu.Item>: <SubMenu key="sub1" icon={<UserOutlined />} title={item.name}>
                  {
                      item.children.map((item1,index1)=>{
                          return <Menu.Item key={item1.path} icon={<PieChartOutlined />}>
                          <Link to={item1.path}>{item1.name}</Link>
                        </Menu.Item>
                      })
                  }
                </SubMenu>
            })
        }
    </Menu>
    )
}

export default Meaus



