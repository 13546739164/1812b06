import React ,{FC, useState} from 'react'
import styles from './index.less'
import { Breadcrumb, message, Popover } from 'antd';
import {
    MenuUnfoldOutlined,
    MenuFoldOutlined,
  } from '@ant-design/icons';
import classNames from 'classnames';
import { useHistory, useLocation } from 'umi';
import { data } from '@/assets/data';
import { removeToken } from "@/utils";


interface propsItem{
    collapsed:boolean,
    setcollapsed:Function
}



const Header:FC<propsItem>=(props)=>{
    const Localtion=useLocation()
    const history=useHistory()
    let obj=data.filter(item=>item.path==Localtion.pathname )[0]    
    let arr=data.filter(item=>!item.path)[0].children
    const content = (
      <div onClick={e=>jumpevent(e)}>
        <p>个人中心</p>   {/*  /ownspace */}
        <p>用户管理</p>   {/*  /user */}
        <p>系统设置</p>   {/*  /setting */}
        <p>退出登录</p>   {/*  history.replace('/login?from='+encodeURIComponent('/')) */}
      </div>
    );

    const jumpevent=(e:React.MouseEvent<HTMLDivElement, MouseEvent>)=>{
        const ele=e.target
      console.log(ele.innerHTML);
        if(ele.innerHTML==="个人中心"){
          history.replace('/ownspace')
        }else if(ele.innerHTML==="用户管理"){
          history.replace('/user')
        }else if(ele.innerHTML==="系统设置"){
          history.replace('/setting')
        }else if(ele.innerHTML==="退出登录"){
          removeToken()
          setTimeout(() => {
            history.replace('/login?from='+encodeURIComponent(location.pathname))
          }, 1000);
        }
    }
    return(
    <div>
        <header className={classNames(styles.header)}>
            {React.createElement(props.collapsed ? MenuUnfoldOutlined : MenuFoldOutlined, {
              style:{fontSize:"20px",fontWeight:"bolder",paddingLeft:"10px",color:"black"},
              onClick: () => props.setcollapsed(!props.collapsed),
            })}
            <p>
                <span className={styles.right_tubiao}>
                  <svg viewBox="64 64 896 896" focusable="false" data-icon="github" width="1em" height="1em" fill="currentColor" aria-hidden="true">
                    <path d="M511.6 76.3C264.3 76.2 64 276.4 64 523.5 64 718.9 189.3 885 363.8 946c23.5 5.9 19.9-10.8 19.9-22.2v-77.5c-135.7 15.9-141.2-73.9-150.3-88.9C215 726 171.5 718 184.5 703c30.9-15.9 62.4 4 98.9 57.9 26.4 39.1 77.9 32.5 104 26 5.7-23.5 17.9-44.5 34.7-60.8-140.6-25.2-199.2-111-199.2-213 0-49.5 16.3-95 48.3-131.7-20.4-60.5 1.9-112.3 4.9-120 58.1-5.2 118.5 41.6 123.2 45.3 33-8.9 70.7-13.6 112.9-13.6 42.4 0 80.2 4.9 113.5 13.9 11.3-8.6 67.3-48.8 121.3-43.9 2.9 7.7 24.7 58.3 5.5 118 32.4 36.8 48.9 82.7 48.9 132.3 0 102.2-59 188.1-200 212.9a127.5 127.5 0 0138.1 91v112.5c.8 9 0 17.9 15 17.9 177.1-59.7 304.6-227 304.6-424.1 0-247.2-200.4-447.3-447.5-447.3z"></path>
                  </svg>
                </span>
                <Popover content={content} className={styles.right_yhm}>
                  <span role="img" aria-label="user" className={styles.right_yuan}><svg viewBox="64 64 896 896" focusable="false" data-icon="user" width="1em" height="1em" fill="currentColor" aria-hidden="true"><path d="M858.5 763.6a374 374 0 00-80.6-119.5 375.63 375.63 0 00-119.5-80.6c-.4-.2-.8-.3-1.2-.5C719.5 518 760 444.7 760 362c0-137-111-248-248-248S264 225 264 362c0 82.7 40.5 156 102.8 201.1-.4.2-.8.3-1.2.5-44.8 18.9-85 46-119.5 80.6a375.63 375.63 0 00-80.6 119.5A371.7 371.7 0 00136 901.8a8 8 0 008 8.2h60c4.4 0 7.9-3.5 8-7.8 2-77.2 33-149.5 87.8-204.3 56.7-56.7 132-87.9 212.2-87.9s155.5 31.2 212.2 87.9C779 752.7 810 825 812 902.2c.1 4.4 3.6 7.8 8 7.8h60a8 8 0 008-8.2c-1-47.8-10.9-94.3-29.5-138.2zM512 534c-45.9 0-89.1-17.9-121.6-50.4S340 407.9 340 362c0-45.9 17.9-89.1 50.4-121.6S466.1 190 512 190s89.1 17.9 121.6 50.4S684 316.1 684 362c0 45.9-17.9 89.1-50.4 121.6S557.9 534 512 534z"></path></svg></span>
                  <span className={styles.right_name} style={{color:"black"}}>Hi,admin</span>
                </Popover>
            </p>
          </header>
            <Breadcrumb className={styles.Breadcrumb}>
            { 
              obj && <Breadcrumb.Item > {obj.name==="工作台"? obj.name:"工作台/"+obj.name}</Breadcrumb.Item> 
            }   
            {
                arr?.map((item,index)=>{                        
                    return !obj && item.path===Localtion.pathname && <Breadcrumb.Item key={index}>
                                    {item.name==="所有文章"?"工作台/"+item.name:'工作台/所有文章/'+item.name}
                                </Breadcrumb.Item>})
            }
            </Breadcrumb>
    </div>
        
    )
}
export default Header