// /api/article
import { EarticleItem } from '@/types/Earticle'
import {request} from 'umi'

export function getEarticle(data:EarticleItem){
    return request('/api/article',{
        method:'POST',
        data
    })
}