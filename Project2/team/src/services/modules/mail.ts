import { IMailQuery, IReplyMail } from '@/types';
import { request } from 'umi';

// 分页获取评论
// https://creationapi.shbwyz.com/api/comment?page=1&pageSize=12
export function getComment(page = 1, pageSize = 12) {
    return request(`/api/comment?page=${page}&pageSize=${pageSize}`)
}
// 更改评论状态（通过或拒绝）
export function updateComment(id:string, data:{[key:string]:boolean}={}){
    return request(`/api/comment/${id}`, {
        method: 'PATCH',
        data
    })
}

// 删除评论
export function deleteComment(id:string){
    return request(`/api/comment/${id}`, {
        method: 'DELETE'
    })
}

// 获取邮件数据

// 获取邮件接口
export function getmailData(page=1, params:Partial<IMailQuery>={}, pageSize=12){
    return request(`/api/smtp?page=${page}&pageSize=${pageSize}`, {params})
}

// 删除邮件
export function deleteMail(id: string){
    return request(`/api/smtp/${id}`, {
        method: 'DELETE'
    })
}

// 回复邮件
export function replyMail(data: IReplyMail){
    return request('http://127.0.0.1:7001/mail', {
        method: 'POST',
        data
    })
}
