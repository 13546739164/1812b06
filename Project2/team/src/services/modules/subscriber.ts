
import { request } from 'umi';

// 访问统计的接口
export function getUser(page: number,params:{[key:string]:string},pageSize = 12){
    return request(`/api/user`, {
        // 表示请求地址栏上面的查询参数
        params: {
            page,
            pageSize,
            ...params  
        },
    })
}


// 删除
export function deleteUser(id:string){
    return request(`/api/search/${id}`, {
        method:"DELETE"
    })
}
