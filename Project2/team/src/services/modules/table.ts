import { request } from 'umi';

// 页面管理数据接口
export function getKonw(page: number,pageSize = 12){
    return request(`/api/page`, {
        // 表示请求地址栏上面的查询参数
        params: {
            page,
            pageSize,   
        },
    })
}
// 更改评论状态（通过或拒绝）
export function updateComments(id:string, data:{[key:string]:boolean}={}){
    return request(`/api/comment/${id}`, {
        method: 'PATCH',
        data
    })
}

// 删除评论
export function deleteComments(id:string){
    return request(`/api/comment/${id}`, {
        method: 'DELETE'
    })
}
