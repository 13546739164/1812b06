
import { request } from 'umi';

// 访问统计的接口
export function getView(page: number,params:{[key:string]:string},pageSize = 12){
    return request(`/api/view`, {
        // 表示请求地址栏上面的查询参数
        params: {
            page,
            pageSize,
            ...params  
        },
    })
}


// 删除
export function deleteView(id:string){
    return request(`/api/view/${id}`, {
        method:"DELETE"
    })
}
