
import { request } from 'umi';

// 系统设置的接口
export function getSet(page: number,pageSize = 12){
    return request(`/api/file`, {
        // 表示请求地址栏上面的查询参数
        params: {
            page,
            pageSize,   
        },
    })
}