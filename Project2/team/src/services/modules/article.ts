
import { request } from 'umi';
import {CategoryArticle} from '@/types'
import {obj} from '@/types/article'
// 获取数据接口
export function getArticle(page=1,pageSize=12){
    return request(`/api/Article?page=${page}&pageSize=${pageSize}`)
}

// 更改发布状态（草稿或者已发布）
export function updateArticle(id:string, data:{[key:string]:boolean}={}){
    console.log(id,data);
    
    return request(`/api/article/${id}`, {
        method: 'PATCH',
        data
    })
}

// 删除评论
export function deleteArticle(id:string){
    return request(`/api/article/${id}`, {
        method: 'DELETE'
    })
}

// 更改首焦状态（首焦推荐或撤销首焦   stick置顶）
export function stickArticle(id:string, data:{[key:string]:boolean}={}){
    return request(`/api/article/${id}`, {
        method: 'PATCH',
        data
    })
}

// 文章分类页面数据获取
export function getCategory(){
    return request(`/api/category`)
}
// 文章分类数据添加
export function postCategory(data:CategoryArticle){
    return request(`/api/category`,{
        method: 'POST',
        data
    })
}

// 文章标签页面数据获取
export function getTags(){
    return request(`/api/tag`)
}
// 文章标签页面数据添加
export function postTags(data:CategoryArticle){
    return request(`/api/tag`,{
        method: 'POST',
        data
    })
}