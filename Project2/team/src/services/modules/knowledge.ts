import { request } from 'umi';

// 搜索记录的接口
export function getKonwle(page: number, pageSize = 12) {
    return request(`/api/knowledge?page=${page}&pageSize=${pageSize}`)
}