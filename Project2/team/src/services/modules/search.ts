import { request } from 'umi';

// 搜索记录的接口
export function getSearch(page: number,params:{[key:string]:string},pageSize = 12){
    return request(`/api/search`, {
        // 表示请求地址栏上面的查询参数
        params: {
            page,
            pageSize,
            ...params  
        },
    })
}

// 删除
export function deleteSearch(id:string){
    return request(`/api/search/${id}`, {
        method:"DELETE"
    })
}
