import React, { useState } from 'react'
import styles from '@/assets/search.less'
import { Button, Drawer, Form, Input, Tabs } from 'antd'

const { TabPane } = Tabs;

function index() {

    const [form] = Form.useForm()
    const [isVisible, setIsVisible] = useState(false)

    const onClose = () => {
        setIsVisible(false);
    };

    return (
        <div className={styles.index}>
            <main className={styles.main}>
                <Tabs tabPosition={"left"}>

                    <TabPane tab="系统设置" key="1">
                        <Form
                            form={form}
                        >
                            <Form.Item>
                                <div>系统设置</div>
                                <Input defaultValue='creationad.shbwyz.co12'></Input>
                            </Form.Item>
                            <Form.Item>
                                <div>后台地址</div>
                                <Input defaultValue="creationadmin.shbwyz.com"></Input>
                            </Form.Item>
                            <Form.Item>
                                <div>系统标题</div>
                                <Input defaultValue='八维创作平台'></Input>
                            </Form.Item>
                            <Form.Item>
                                <div>Logo</div>
                                <Input addonAfter={<svg
                                    onClick={() => setIsVisible(true)}
                                    viewBox="64 64 896 896" focusable="false" data-icon="file-image" width="1em" height="1em" fill="currentColor" aria-hidden="true"><path d="M553.1 509.1l-77.8 99.2-41.1-52.4a8 8 0 00-12.6 0l-99.8 127.2a7.98 7.98 0 006.3 12.9H696c6.7 0 10.4-7.7 6.3-12.9l-136.5-174a8.1 8.1 0 00-12.7 0zM360 442a40 40 0 1080 0 40 40 0 10-80 0zm494.6-153.4L639.4 73.4c-6-6-14.1-9.4-22.6-9.4H192c-17.7 0-32 14.3-32 32v832c0 17.7 14.3 32 32 32h640c17.7 0 32-14.3 32-32V311.3c0-8.5-3.4-16.7-9.4-22.7zM790.2 326H602V137.8L790.2 326zm1.8 562H232V136h302v216a42 42 0 0042 42h216v494z"></path></svg>} defaultValue='https://bwcreation.oss-cn-beijing.aliyuncs.com/2021-08-30/001545-16159113459e8c.jpg'></Input>
                            </Form.Item>
                            <Form.Item>
                                <div>Favicon</div>
                                <Input addonAfter={<svg
                                    onClick={() => setIsVisible(true)}
                                    viewBox="64 64 896 896" focusable="false" data-icon="file-image" width="1em" height="1em" fill="currentColor" aria-hidden="true"><path d="M553.1 509.1l-77.8 99.2-41.1-52.4a8 8 0 00-12.6 0l-99.8 127.2a7.98 7.98 0 006.3 12.9H696c6.7 0 10.4-7.7 6.3-12.9l-136.5-174a8.1 8.1 0 00-12.7 0zM360 442a40 40 0 1080 0 40 40 0 10-80 0zm494.6-153.4L639.4 73.4c-6-6-14.1-9.4-22.6-9.4H192c-17.7 0-32 14.3-32 32v832c0 17.7 14.3 32 32 32h640c17.7 0 32-14.3 32-32V311.3c0-8.5-3.4-16.7-9.4-22.7zM790.2 326H602V137.8L790.2 326zm1.8 562H232V136h302v216a42 42 0 0042 42h216v494z"></path></svg>} defaultValue='https://bwcreation.oss-cn-beijing.aliyuncs.com/2021-07-26/logo2.4a842ec4.png'></Input>
                            </Form.Item>
                            <Form.Item>
                                <div>页脚信息</div>
                                <Input.TextArea style={{ height: '200px' }} defaultValue='wrer，学习使你快乐'></Input.TextArea>
                            </Form.Item>
                            <Form.Item>
                                <Button type="primary" htmlType="submit">
                                    保存
                                </Button>
                            </Form.Item>
                        </Form>
                    </TabPane>


                    <TabPane tab="国际化设置" key="2">

                    </TabPane>


                    <TabPane tab="SEO设置" key="3">
                        <Form>
                            <Form.Item>
                                <div>关键词</div>
                                <Input defaultValue='八维创作平台'></Input>
                            </Form.Item>
                            <Form.Item>
                                <div>描述信息</div>
                                <Input.TextArea style={{ height: '120px' }} defaultValue='博客，创作，blog，学习使你快乐'></Input.TextArea>
                            </Form.Item>
                            <Form.Item>
                                <Button type="primary" htmlType="submit">
                                    保存
                                </Button>
                            </Form.Item>
                        </Form>
                    </TabPane>


                    <TabPane tab="数据统计" key="4">
                        <Form>
                            <Form.Item>
                                <div>百度统计</div>
                                <Input defaultValue='8bd099eec64421b1831043373289e'></Input>
                            </Form.Item>
                            <Form.Item>
                                <div>谷歌分析</div>
                                <Input placeholder='请输入谷歌分析Id'></Input>
                            </Form.Item>
                            <Form.Item>
                                <Button type="primary" htmlType="submit">
                                    保存
                                </Button>
                            </Form.Item>
                        </Form>
                    </TabPane>


                    <TabPane tab="OSS设置" key="5">
                        Content of Tab 2
                    </TabPane>


                    <TabPane tab="SMTP服务" key="6">
                        <Form>
                            <Form.Item>
                                <div>SMTP 地址</div>
                                <Input defaultValue='学习使你快乐11'></Input>
                            </Form.Item>
                            <Form.Item>
                                <div>SMTP 端口（强制使用 SSL 连接）</div>
                                <Input defaultValue='学习使你不快乐11'></Input>
                            </Form.Item>
                            <Form.Item>
                                <div>SMTP 用户</div>
                                <Input defaultValue='学习使你快乐111'></Input>
                            </Form.Item>
                            <Form.Item>
                                <div>SMTP 密码</div>
                                <Input defaultValue='学习使你快乐111'></Input>
                            </Form.Item>
                            <Form.Item>
                                <div>发件人</div>
                                <Input defaultValue='学习是我快乐'></Input>
                            </Form.Item>
                            <Form.Item>
                                <Button type="primary" htmlType="submit" style={{ marginRight: "20px" }}>
                                    保存
                                </Button>
                                <Button>
                                    测试
                                </Button>
                            </Form.Item>
                            <Form.Item>

                            </Form.Item>
                        </Form>
                    </TabPane>
                </Tabs>
            </main>
            <Drawer title="文件选择" visible={isVisible} onClose={onClose} width='50%'>
                <Form
                form={form}
                >
                    <Form.Item
                    label='文件名'
                    >
                        <Input></Input>
                    </Form.Item>
                    <Form.Item
                     label='文件类'
                    >
                        <Input></Input>
                    </Form.Item>
                    <Form.Item>
                        <Button type='primary' htmlType='submit'>搜索</Button>
                        <Button>重置</Button>
                    </Form.Item>
                </Form>
            </Drawer>
        </div>
    )
}

export default index
