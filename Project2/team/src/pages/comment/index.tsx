import useStore from '@/context/useStore';
import { ICommentItem } from '@/types';
import { Form, Input, Select, Button, Table, Badge,Popover } from 'antd'
import { Key, useEffect, useState } from 'react';
import { observer } from 'mobx-react-lite'
import styles from './index.less'; // 等于启用
import style from '@/assets/index.less'; // 等于启用

interface IForm{
    [key: string]: string|boolean
}
const className = require("classnames")
const { useForm } = Form;
const Comment: React.FC = () => {
  const [form] = useForm();
  const store = useStore();
  const [page, setPage] = useState(1);
  const [pageSize, setpageSize] = useState(12);
  
  const [params, setParams] = useState<IForm>({})
  const [selectedRowKeys, setSelectedRowKeys] = useState<Key[]>([]);
  useEffect(()=>{
      store.comment.getComment(page,pageSize);
  }, [page,pageSize])
  function submit(){
    let values = form.getFieldsValue();
    let params: IForm= {};
    for (let key in values){
        values[key] && (params[key] = values[key]);
    }
    setParams(params);
}
  const columns = [{
    title: '状态',
    render: (row: ICommentItem) => {
      return row.pass ? <Badge status="success" text="通过" /> : <Badge status="warning" text="未通过" />
    }
  }, {
    title: '称呼',
    dataIndex: 'name'
  }, {
    title: '联系方式',
    dataIndex: 'email'
  }, {
    title: '原始内容',
    render: (item:ICommentItem)=>{
        // <p style={{height:'375px',overflowY:'auto'}} dangerouslySetInnerHTML={{__html:this.state.resultText}} ></p>

        // content={ <p dangerouslySetInnerHTML={{ __html: item.content }} key={item.id}></p>}
        return <Popover content={ <p dangerouslySetInnerHTML={{ __html: item.content }} key={item.id}></p>} title="评论详情-原始内容">
            <span className={styles.spancss}>查看内容</span>
        </Popover>
    }
  }, {
    title: 'HTML内容',
    render: (item:ICommentItem)=>{
        return <Popover  content={ <p dangerouslySetInnerHTML={{ __html: item.html }} key={item.id}></p>} title="评论详情-HTML 内容">
            <span className={styles.spancss}>查看内容</span>
        </Popover>
    }
  }, {
    title: '管理文章',
    render: (item:ICommentItem)=>{
        return <Popover content={<iframe src={'https://creation.shbwyz.com'+item.url}></iframe>} title="页面预览">
        <span   className={styles.spancss}>文章</span> 
    </Popover>
    }
  }, {
    title: '创建时间',
    dataIndex: 'createAt'
  }, {
    title: '父级评论',
    dataIndex: 'replyUserName'
  }, {
    title: '操作',
    render: (row: ICommentItem) => {
      return <p>
        <span className={styles.btncses} onClick={()=>store.comment.updateComment([row.id], {pass:true})}>通过</span>
        <span  className={styles.btncses}  onClick={()=>store.comment.updateComment([row.id], {pass:false})}>拒绝</span>
        <span  className={styles.btncses}>回复</span>
        <span  className={styles.btncses} onClick={()=>store.comment.deleteComment([row.id])}>删除</span>
      </p>
    }
  }]
  function onSelectChange(selectedRowKeys:Key[], items:ICommentItem[]){
    setSelectedRowKeys(selectedRowKeys);
}
const rowSelection = {
    selectedRowKeys,
    onChange: onSelectChange
}


  return <div className={style.index}>
    <div className={style.main}>
      <div className={styles.legacy}>
        <Form className={styles.legacy}
          form={form}
          onFinish={submit}
        >
          <div className={styles.call}>
            <Form.Item 
              name="replyUserName"
              label="称呼"
            >
              
              <Input type="text" placeholder=" 请输入称呼" />
            </Form.Item>
          </div>
          <div className={styles.email}>
            <Form.Item
              name="replyUserEmail"
              label="Email"
            >
           
              <Input type="text" placeholder=" 请输入联系方式" />
            </Form.Item>
          </div>
          <div className={styles.states}>
            <Form.Item
              name="pass"
              label="状态"
            >
              <Select className={styles.stateless}>
                <Select.Option value="true">已通过</Select.Option>
                <Select.Option value="false">未通过</Select.Option>
              </Select>
            </Form.Item>
          </div>
          <div className={styles.btncss}>
          <Button htmlType="submit" className={styles.searchcss}>搜索</Button>
          <Button htmlType="reset" className={styles.resetcss}>重置</Button>

          </div>
         
        </Form>
      </div>
    </div>
    {selectedRowKeys.length?<section>
            <Button onClick={()=>store.comment.updateComment(selectedRowKeys as string[], {pass:true})}>通过</Button>    
            <Button onClick={()=>store.comment.updateComment(selectedRowKeys as string[], {pass:false})}>拒绝</Button>    
            <Button onClick={()=>store.comment.deleteComment(selectedRowKeys as string[])}>删除</Button>    
        </section>:null}
    <div className={style.footer}>
     
      <Table   rowSelection={rowSelection} columns={columns} dataSource={store.comment.commentList}  rowKey='id'></Table>
    </div>
  </div>
}

export default observer(Comment);
