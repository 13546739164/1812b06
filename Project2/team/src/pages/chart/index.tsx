import { useEffect, useRef, useState } from 'react';
import io, { Socket } from 'socket.io-client';
import styles from './index.less'
interface IMessage{
    title?: string;
    message: string;
}
const index = () => {
    const [username, setUsername] = useState('');
    const [content, setContent] = useState('');
    const [message, setMessage] = useState<IMessage []>([]);
    const [showDialog, setShowDialog] = useState(true);
    const instance = useRef<Socket>();
    useEffect(() => {
        // 建立连接
        instance.current = io('ws://127.0.0.1:3000');

        //  instance.current events
        // Whenever the server emits 'login', log the login message
        instance.current.on('login', (data) => {
            // connected = true;
            setMessage(message=>[...message, {message: `Welcome to Socket.IO Chat –
            there's ${data.numUsers} participant`}]);
            // // Display the welcome message
            // const message = 'Welcome to  instance.current.IO Chat – ';
            // log(message, {
            //     prepend: true
            // });
            // addParticipantsMessage(data);
        });

        // Whenever the server emits 'new message', update the chat body
        instance.current.on('new message', (data) => {
            // addChatMessage(data);
            setMessage(message=>[...message, {
                title: data.username,
                message: data.message,
            }]);
        });

        // Whenever the server emits 'user joined', log it in the chat body
        instance.current.on('user joined', (data) => {
            // log(`${data.username} joined`);
            // addParticipantsMessage(data);
        });

        // Whenever the server emits 'user left', log it in the chat body
        instance.current.on('user left', (data) => {
            // log(`${data.username} left`);
            // addParticipantsMessage(data);
            // removeChatTyping(data);
        });

        // Whenever the server emits 'typing', show the typing message
        instance.current.on('typing', (data) => {
            // addChatTyping(data);
        });

        // Whenever the server emits 'stop typing', kill the typing message
        instance.current.on('stop typing', (data) => {
            // removeChatTyping(data);
        });

        instance.current.on('disconnect', () => {
            // log('you have been disconnected');
        });

        instance.current.on('reconnect', () => {
            // log('you have been reconnected');
            // if (username) {
            //      instance.current.emit('add user', username);
            // }
        });

        instance.current.on('reconnect_error', () => {
            // log('attempt to reconnect has failed');
        });
    }, []);

    // 用户加入聊天室
    function joinChatRoom(e: React.KeyboardEvent){
        if (e.keyCode === 13 && username) {
            setShowDialog(false);
            instance.current!.emit('add user', username);
        }
    }

    // 用户发送消息
    function sendMessage(e: React.KeyboardEvent){
        if (e.keyCode === 13 && content) {
            setMessage(message=>[
                ...message, {
                    title: username,
                    message: content
                }
            ])
            instance.current!.emit('new message', content);
            setContent('');
        }
    }
    return <div>
        {showDialog ? <div className={styles.in}>
            <p>What`s your nickname?</p>
            <input
                type="text" value={username}
                onChange={e => setUsername(e.target.value)}
                onKeyDown={joinChatRoom}
            />
        </div> : <div>
            <div>{
                message.map(item=>{
                    return <p key={item.message}>
                        {item.title && <span>{item.title}</span>}
                        <span>{item.message}</span>
                    </p>
                })    
            }</div>
            <input 
                placeholder="Typeing here..." 
                type="text" value={content} 
                onChange={e => setContent(e.target.value)}
                onKeyDown={sendMessage}
            />
        </div>}
    </div>
}

export default index;
