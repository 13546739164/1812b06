import React from 'react'
import styles from './index.less'
import {Input,Button,message,Form} from 'antd'
import { observer } from 'mobx-react-lite';
import { CategoryArticle } from '@/types';
import { useEffect, useState } from 'react'
import useStore from '@/context/useStore';

const Index:React.FC=()=>{
    const [user,setUser]=useState('')
    const [pwd,setPwd]=useState('')
    const store=useStore();
    const [form] = Form.useForm();
    useEffect(()=>{
        store.article.getTags();
    },[])
   console.log(store.article.TagsList);
   
   async function submit() {
       if(!user){
           message.warn('请输入标签名称');
           return;
       }
       if(!pwd){
        message.warn('请输入标签值');
        return;  
       }
       const values=form.getFieldsValue();
       values.articleCount=0;
       values.createA= new Date();
       values.updateAt=new Date();
       values.id=new Date().getTime()+'';
       values.label=user;
       values.value=pwd;
       values.status='publish';
       let result=await store.article.postTags(values);
       console.log('result...',result);
       if (result.data) {
        message.success('文章发布成功');
    }
   }
   console.log('form..', form.getFieldsValue());
   
    return (
        <div className={styles.cate}>
            <div className={styles.left}>
                <div className={styles.left_top} >
                    添加标签
                </div>
                <div className={styles.left_foot}>
                    <Input value={user}  onChange={e=>setUser(e.target.value)} type="text" placeholder="输入分类名称" ></Input>
                    <Input value={pwd}  onChange={e=>setPwd(e.target.value)} type="text"  placeholder="输入分类值（请输入英文，作为路由使用）"></Input>
                    <Button onClick={submit}>保存</Button>
                </div>
            </div>
            <div className={styles.right}>
                <div className={styles.right_top}>
                    所有标签
                </div>
                <div className={styles.right_foot}>
                    <ul>
                        {
                            store.article.TagsList.map((item,index)=>{
                                return <li key={index}>
                                    {item.label}
                                </li>
                            })
                        }
                    </ul>
                </div>
            </div>
        </div>
    )
}

export default observer(Index)
