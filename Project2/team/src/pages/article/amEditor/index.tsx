import React, { useState } from 'react'
import Editor from 'for-editor'
import {Button,Input, message, Popover,Drawer,Form, Select, Menu} from 'antd'
import styles from './index.less'
import { SmallDashOutlined } from '@ant-design/icons'
import useStore from '@/context/useStore'
import { useEffect } from 'react'
import { makeHtml, makeToc } from '@/utils/showdown'

const amEditor: React.FC = () => {
    const store = useStore()
    const [content, setcontent] = useState('')
    const [Title, setTitle] = useState('')
    const [chinese, setchinese] = useState(true)
    const [visible, setVisible] = useState(false)
    const contents=((<Menu>
            <Menu.Item>查看</Menu.Item>
            <Menu.Item onClick={showDrawer}>设置</Menu.Item>
            <Menu.Item>保存草稿</Menu.Item>
            <Menu.Item>删除</Menu.Item>
        </Menu>
    ))
    const toolbar={
        h1: true, // h1
        h2: true, // h2
        h3: true, // h3
        h4: true, // h4
        img: true, // 图片
        link: true, // 链接
        code: true, // 代码块
        preview: true, // 预览
        expand: true, // 全屏
        /* v0.0.9 */
        undo: true, // 撤销
        redo: true, // 重做
        save: true, // 保存
        /* v0.2.3 */
        subfield: true, // 单双栏模式
    }
    const [cover, setCover] = useState('');
    const [form] = Form.useForm();
    
    async function submit() {
        if(!Title){
            message.warning('输入文章标题')
            return
        }
        const values = form.getFieldsValue();
        // 添加文章md内容
        values.content = content;
        // 添加文章html内容
        values.html = makeHtml(content);
        // 添加文章大纲
        values.toc = makeToc(values.html);
        // 添加文章标题
        values.title = Title;
        // 添加文章发布状态
        values.status = 'publish';
        let result = await store.eAarticle.getEarticle(values);
        console.log('result...', result);
        if (result.data) {
            message.success('文章发布成功');
        }

        
        
    }

    function showDrawer() {
        if (Title) {
            setVisible(true)
        } else {
            message.warning('输入文章内容')
        }
    }
    //  函数调用，拿到分类数据
    useEffect(() => {
        store.species.getSpecies()
        store.tages.getTages()
    }, [])
      // 监听iframe发过来的值
      useEffect(()=>{
        window.addEventListener('message', value=>{
            console.log('value...', value);
            setcontent (value.data);
        })
    }, []);

    return (
        <div className={styles.amEditor}>
            <header>
                <div className={styles._left}>
                    <button>X</button>
                    <input type="text" value={Title} onChange={e => setTitle(e.target.value)} />
                </div>
                <div className={styles._right}>
                    <Button onClick={submit}>发布</Button>
                    <Popover content={contents} placement="bottom">
                        <SmallDashOutlined style={{ fontSize: '32px', fontWeight: "bolder", color: "rgb(35, 199, 199)" }} />
                    </Popover>
                </div>
            </header>
            <div className={styles.main}>
                <Editor value={content} 
                        onChange={value=>setcontent(value)} 
                        subfield={true}
                        preview={true}
                        toolbar={toolbar} />

            <Drawer title="文章设置" placement="right" onClose={()=>setVisible(false)} visible={visible}>
            <Form
            form={form}
        >
            <Form.Item name="summary" label="文章摘要">
                <Input.TextArea></Input.TextArea>
            </Form.Item>
            <Form.Item name="password" label="访问密码">
                <Input.Password placeholder="" />
            </Form.Item>
            <Form.Item name="totalAmount" label="付费查看">
                <Input.Password placeholder="" />
            </Form.Item>
            <Form.Item name="isCommentable" label="开启评论">
                <Input type="checkbox" placeholder="" />
            </Form.Item>
            <Form.Item name="isRecommended" label="首页推荐">
                <Input type="checkbox" placeholder="" />
            </Form.Item>
            <Form.Item name="category" label="选择分类">
                <Select>{
                    // store.category.categoryList.map(item=>{
                    //     return <Select.Option value={item.id}>{item.label}</Select.Option>
                    // })
                }</Select>
            </Form.Item>
            <Form.Item name="tags" label="选择标签">
            <Select>{
                    // store.tag.tagList.map(item=>{
                    //     return <Select.Option value={item.id}>{item.label}</Select.Option>
                    // })
                }</Select>
            </Form.Item>
            <img src={cover} alt=""/>
            <Form.Item name="cover" label="文章封面">
                <Input type="text" placeholder="" onChange={e=>setCover(e.target.value)} />
            </Form.Item>
            <Button onClick={()=>{
                    // let values = form.getFieldsValue();
                    // form.setFieldsValue({...values, cover:'' })
                    // setCover('');
                }}>移除</Button>
            <Button htmlType="submit">确认</Button>
        </Form>

            </Drawer>
            </div>
            

        </div>
    )
}
export default amEditor
