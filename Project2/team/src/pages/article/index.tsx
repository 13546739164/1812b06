import { useEffect, useState,Key } from 'react'
import style from "@/assets/index.less"
import styles from "./index.less"
import {Table, Button } from 'antd';
import useStore from '@/context/useStore';
import { ArticleItem, Category } from '@/types';
import { observer } from 'mobx-react-lite';
import moment from '@/component/moment'

// const {useForm} = Form;
const Index:React.FC=()=>{
  
    const store = useStore();
    // const [form] = useForm();
    const [hasSelected,setHasSelected]=useState(1)
    const [selectedRowKeys,setSelectedRowKeys]=useState<Key[]>([])
    const [loading,setLoading]=useState(false)
    // const [data,setData] = useState([])
    const [page,setPage] = useState(1)  
    const [pageSize,setpageSize] = useState(12)  
    

     useEffect(() => {
       store.article.getArticle(page,pageSize)
       console.log(pageSize);
       
    }, [page,pageSize])
  //   function submit(){
  //     let values = form.getFieldsValue();
  //     console.log(values);
  // }

    const columns = [
        {
          title: '标题',
          dataIndex:'title',
          width:100,
          fixed: 'left',
        },
        {
          title: '状态',
          width:100,
          render:(data:ArticleItem)=>{
            return <>{data.isCommentable?'已发布':'草稿'}</>
        },
        },
        {
          title: '分类',
          width:100,
          render:(data:ArticleItem)=>{
            return <div className={styles.classify}>{data.category?.label}</div>
         }
        },
        
        {
            title: '标签',
            width:400,
            render:(data:ArticleItem)=>{
               return data.tags.map((item,index)=>{
                return <div className={styles.tags} key={index}>{item.label}</div>
              })
            }
          },
        {
          title: '阅读量',
          dataIndex: 'views',
          width:100,
        },
        {
          title: '喜欢数',
          dataIndex: 'likes',
          width:100,
        },
          {
            title: '发布时间',
            render: (data: ArticleItem) => {
                return <>{moment(data.updateAt).format()}</>
            },
            width:250,
        },
        {
          title:'操作',
          fixed: 'right',
          width:400,
          render:(data:ArticleItem)=>{
            return <p className={styles.footer_operation}>
              <Button>编辑</Button>
                <Button onClick={()=>store.article.stickArticle([data.id], {isRecommended:false})}>{data.isRecommended?'撤销首焦':'首焦推荐'}</Button>
                <Button>查看访问</Button>
                <Button onClick={()=>store.article.deleteArticle([data.id])}>删除</Button>
            </p>
          }
        }
      ];

      function onSelectChange(selectedRowKeys:Key[],items:ArticleItem[]){
        setSelectedRowKeys(selectedRowKeys)
      }
  
      const rowSelection={
        selectedRowKeys,
        onChange: onSelectChange,
      }
    
    function start(){
        setLoading((loading)=>loading=!loading)
        if(!loading){
            setTimeout(() => {
                setSelectedRowKeys([])
                setLoading(false) 
            }, 1000);
        }
    }
    function onShowSizeChange(current:number,pageSize:number){
      setpageSize(pageSize)
    }
    return (
        <div className={style.index}>
            <main className={style.main}>
                <div className={styles.main_cli}>
                    <div className={styles.main_div}>
                    <span>标题：</span><input style={{width:"176px",height:"32px",padding:"4px 11px",border:"1px solid #ccc",borderRadius:"3px"}} type="text" placeholder="请输入文章标题" />
                    </div>
                    <div className={styles.main_div}>
                    <span>状态：</span><select style={{width:"180px",height:"32px",padding:"0 11px",border:"1px solid #ccc",borderRadius:"3px"}} name="" id="" >
                        <option value=""> </option>
                        <option value="">已发布</option>
                        <option value="">草稿</option>
                    </select>
                    </div>
                    <div className={styles.main_div}>
                        <span>分类：</span><select style={{width:"180px",height:"32px",padding:"0 11px",border:"1px solid #ccc",borderRadius:"3px"}} name="" id="">
                        <option value=""> </option>
                        <option value="">leetcode</option>
                        <option value="">前端</option>
                        <option value="">后端</option>
                        <option value="">123</option>
                    </select></div></div>
                    <div className={styles.main_cli}>
                    <Button>重置</Button>
                    <Button className={styles.main_sel}>搜索</Button>
                    </div>
                
            </main>
            <footer className={style.footer}>
            <div>
           <div className={styles.footer_left}> {selectedRowKeys.length?<section>
            <Button onClick={()=>store.article.updateArticle(selectedRowKeys as string[], {isCommentable:true})}>发布</Button>    
            <Button onClick={()=>store.article.updateArticle(selectedRowKeys as string[], {isCommentable:false})}>草稿</Button>
            <Button onClick={()=>store.article.stickArticle(selectedRowKeys as string[], {isRecommended:true})}>首焦推荐</Button>    
            <Button onClick={()=>store.article.stickArticle(selectedRowKeys as string[], {isRecommended:false})}>撤销首焦</Button>    
            <Button style={{borderColor:"red",color:'red'}} onClick={()=>store.article.deleteArticle(selectedRowKeys as string[])}>删除</Button>    
        </section>:null}</div>
              <div className={styles.footer_right}>
              <Button className={styles.footer_right_xinjian}>+新建</Button>
            <Button className={styles.footer_right_load} onClick={()=>start()} disabled={!hasSelected} loading={loading}>
            <svg viewBox="64 64 896 896" focusable="false" data-icon="reload" width="1em" height="1em" fill="currentColor" aria-hidden="true"><path d="M909.1 209.3l-56.4 44.1C775.8 155.1 656.2 92 521.9 92 290 92 102.3 279.5 102 511.5 101.7 743.7 289.8 932 521.9 932c181.3 0 335.8-115 394.6-276.1 1.5-4.2-.7-8.9-4.9-10.3l-56.7-19.5a8 8 0 00-10.1 4.8c-1.8 5-3.8 10-5.9 14.9-17.3 41-42.1 77.8-73.7 109.4A344.77 344.77 0 01655.9 829c-42.3 17.9-87.4 27-133.8 27-46.5 0-91.5-9.1-133.8-27A341.5 341.5 0 01279 755.2a342.16 342.16 0 01-73.7-109.4c-17.9-42.4-27-87.4-27-133.9s9.1-91.5 27-133.9c17.3-41 42.1-77.8 73.7-109.4 31.6-31.6 68.4-56.4 109.3-73.8 42.3-17.9 87.4-27 133.8-27 46.5 0 91.5 9.1 133.8 27a341.5 341.5 0 01109.3 73.8c9.9 9.9 19.2 20.4 27.8 31.4l-60.2 47a8 8 0 003 14.1l175.6 43c5 1.2 9.9-2.6 9.9-7.7l.8-180.9c-.1-6.6-7.8-10.3-13-6.2z"></path></svg>
            </Button>
              </div>
          </div>
          <Table rowSelection={rowSelection} pagination={{showSizeChanger:true,onShowSizeChange:onShowSizeChange}} scroll={{ x: 1000}}  columns={columns} dataSource={store.article.articleList} rowKey="id"></Table>
            </footer>
        </div>
    )
}

export default observer(Index)

