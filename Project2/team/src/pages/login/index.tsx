import useStore from '@/context/useStore';
import React from 'react'
import { ILoginForm } from '@/types';
import { Button, Form, Input } from 'antd';
import { Link, useHistory, useLocation } from 'umi';
import { observer } from 'mobx-react-lite';
import styles from './login.less'


interface ILocation{
    query: {
        from: string;
    }
}


const Login:React.FC = () => {
   
    const store = useStore();
    const history = useHistory();
    const location = useLocation();    
    async function submit(values:ILoginForm){
        let result = await store.user.login(values);
        if (Boolean(result)){
            let redirect = '/';
            if ((location as unknown as ILocation).query.from){
                redirect = decodeURIComponent((location as unknown as ILocation).query.from);
            }
            history.replace(redirect);
        }
    }

    return <div className={styles.login}>
        <Form
            onFinish={submit}
            initialValues={{name:'admin', password:'admin'}}
        >
            <Form.Item
                name="name"
                label="账户"
                rules={[
                    {required: true, message:'请输入用户名'},
                    {pattern: /\w{5,12}/, message:'请输入合法的用户名'}
                ]}
            >
                <Input type="text" />
            </Form.Item>
            <Form.Item
                name="password"
                label="密码"
                rules={[{required: true, message:'请输入密码'}]}
            >
                <Input type="password" />
            </Form.Item>
            <Button htmlType="submit">登陆</Button>
            <p>Or
                <Link to="/register">注册用户</Link>
            </p>
        </Form>
    </div>

}
export default observer(Login);


