import React, { useEffect, useState } from 'react'
import { getData } from '@/utils/index'
import { Link, useHistory } from 'umi';
import styles from './index.less'
import { navlist } from "@/assets/data"
import useStore from '@/context/useStore';
import { Table, Tag, Space, Badge, Popover } from 'antd';
import echarts from 'echarts'

const Index: React.FC = () => {
  const store = useStore()
  const history = useHistory()
  const [page, setPage] = useState(1)
  const [pageSize, setpageSize] = useState(6)
  const [Obj,setObj]=useState({})
  if(getData()==undefined){
    history.replace('/login')
  }
  let obj=JSON.parse(getData()!)
  useEffect(() => {
    store.article.getArticle(page, pageSize)
    store.comment.getComment(page, pageSize)
    // console.log(store.comment.getComment(page, pageSize));
    
  }, [page, pageSize])





//  let myChart = echarts.init(document.getElementById('echarts')!);
 
  const option = {
      title: {
          text: 'ECharts 入门示例'
      },
      tooltip: {},
      legend: {
          data:['销量']
      },
      xAxis: {
          data: ["衬衫","羊毛衫","雪纺衫","裤子","高跟鞋","袜子"]
      },
      yAxis: {},
      series: [{
          name: '销量',
          type: 'bar',
          data: [5, 20, 36, 10, 10, 20]
      }]
  };
  // myChart.setOption(option)
  const allArticle = () => {
    history.replace('/article')
  }
  const allComment = () => {
    history.replace('/comment')
  }
  

  return (
    <div className={styles.pages}>
      { getData()&& obj !=undefined?<div className={styles.top}>
              <h1>您好，{obj.name} </h1>
              <h3>你的角色是，{obj.role}</h3>
          </div>:null
        }
      
      <div className={styles.content}>
        <div className={styles.Statistics} id="echarts">
          12345
            </div>
        <div className={styles.nav}>
          <h1>快速导航</h1>
          <p>
            {
              navlist.map((item, index) => {
                return <Link to={item.path} key={index}>  {item.name} </Link>
              })
            }
          </p>
        </div>
        <div className={styles.article}>
          <h1><span>最新文章</span><span onClick={allArticle}>全部文章</span></h1>
          <div className={styles.inarticle}>
            {
              store.article.articleList!.map((item, index) => {
                return <div className={styles.item} key={item.id} >
                  {item.cover ? <img src={item.cover} alt="" /> : <span className={styles.covers}>文章封面</span>}
                  <span> {item.title} </span>
                </div>
              })
            }
          </div>
        </div>
        <div className={styles.comment}>
          <h1><span>最新评论</span><span onClick={allComment}>全部评论</span></h1>
          {
            store.comment.commentList!.map((item, index) => {
              return  <div key={item.id}>
                        <p>  {item.name}在 <Popover content={<iframe src={'https://creation.shbwyz.com' + item.url}></iframe>} title="页面预览">
                          <span className={styles.spancss}>文章</span>
                        </Popover> 评论 <Popover content={<p dangerouslySetInnerHTML={{ __html: item.html }} key={item.id}></p>} title="评论详情-HTML 内容">
                            <span className={styles.spancss}>查看内容</span>
                          </Popover> {item.pass ? <Badge status="success" text="通过" /> : <Badge status="warning" text="未通过" />} </p>
                        <p><span className={styles.btncses} onClick={() => store.comment.updateComment([item.id], { pass: true })}>通过</span>
                          <span className={styles.btncses} onClick={() => store.comment.updateComment([item.id], { pass: false })}>拒绝</span>
                          <span className={styles.btncses}>回复</span>
                          <span className={styles.btncses} onClick={() => store.comment.deleteComment([item.id])}>删除</span>
                        </p>
                      </div>
            })
          }
        </div>
      </div>
    </div>
  )
}


export default Index


