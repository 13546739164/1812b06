import React, { Key, useEffect } from 'react'
import { Table, Tag, Space, Input, TablePaginationConfig, Popconfirm, message } from 'antd';
import useStore from '@/context/useStore';
import { useState } from 'react';
import { observer } from 'mobx-react-lite';
import { ISearch } from '@/types';
import styles from '@/assets/search.less'
import moment from '@/component/moment'
import { Button } from 'antd';
import { Form } from 'antd';

interface IForm {
    [key: string]: string
}

function index() {

    const columns = [
        {
            title: '搜索词',
            dataIndex: 'keyword',
        },
        {
            title: '搜索量',
            render: (row: ISearch) => {
                return <p
                    style={{
                        backgroundColor: "rgb(82, 196, 26)",
                        width: "20px",
                        height: "20px",
                        borderRadius: "50%",
                        color: "#fff",
                        textAlign: "center",
                        lineHeight: "20px"
                    }}
                >{row.count}</p>
            }
        },
        {
            title: '搜索时间',
            render: (row: ISearch) => {
                return <>{moment(row.updateAt).format("YYYY-MM-DD HH:mm:ss")}</>
            }
        },
        {
            title: '操作',
            render: (text: string, record: any) => (

                <Space size="middle">
                    <Popconfirm
                    title="确认要删除吗？"
                    onConfirm={confirm}
                    onCancel={cancel}
                    okText="确认"
                    cancelText="取消"
                  >
                    <Button type="primary">删除</Button>
                  </Popconfirm>
                </Space>
            ),
        },
    ];

    const { useForm } = Form;
    const [form] = useForm();
    const store = useStore();
    const [page, setPage] = useState(1)
    const [pageSize, setPageSize] = useState(8)
    const [params, setParams] = useState({})
    const [selectedRowKeys, setSelectedRowKeys] = useState<Key[]>([]);
    const [pagination, setpagination] = useState({
        current: 1,
        pageSize: 8,
        total: 36,
        showSizeChanger: true,
    })


    useEffect(() => {
        store.search.getSearch(page, params,pageSize)
    }, [page, params,pageSize])

    // 改变选框状态
    const onSelectChange = (selectedRowKeys: Key[]) => {
        setSelectedRowKeys(selectedRowKeys)
    }
    const rowSelection = {
        selectedRowKeys,
        onChange: onSelectChange,
    };
    // 提交搜索
    function submit() {
        let values = form.getFieldsValue();
        let params: IForm = {}
        for (let key in values) {
            values[key] && (params[key] = values[key])
        }
        setParams(values)
    }

    const handleTableChange=(pagination:any)=>{
        // 修改每页几条
        setpagination(pagination)
        // 修改当前页数
        setPage(pagination.current)
        // 修改条数
        setPageSize(pagination.pageSize)
    }

    function confirm(e?: React.MouseEvent<HTMLElement, MouseEvent> | undefined) {
        console.log(e);
        store.view.deleteView(selectedRowKeys as string[])
       
      }
      
      function cancel(e?: React.MouseEvent<HTMLElement, MouseEvent> | undefined) {
        message.error('Click on No');
    }


    return (
        <div className={styles.index}>

            <header className={styles.head}>

                <Form
                    form={form}
                    onFinish={submit}
                    style={{ display: "flex", flexDirection: "column" }}
                >

                    <div style={{ display: "flex" }}>
                        <Form.Item
                            name="type"
                            label="类型"
                            style={{ width: "300px", marginRight: "20px" }}
                        >
                            <Input type="text" placeholder=" 请输入类型" />
                        </Form.Item>
                        <Form.Item
                            name="keyword"
                            label="搜索词"
                            style={{ width: "300px", marginRight: "20px" }}
                        >
                            <Input type="text" placeholder=" 请输入搜索词" />
                        </Form.Item>
                        <Form.Item
                            name="count"
                            label="搜索量"
                            style={{ width: "300px", marginRight: "20px" }}
                        >

                            <Input type="text" placeholder=" 请输入搜索量" />
                        </Form.Item>
                    </div>

                    <div style={{ display: "flex", justifyContent: "space-between" }}>
                        <div></div>
                        <div>
                            <Button htmlType="submit" type="primary" style={{ marginRight: "20px" }}>搜索</Button>
                            <Button htmlType="reset">重置</Button>
                        </div>
                    </div>
                </Form>

            </header>

            <main className={styles.main}>
                {
                    selectedRowKeys.length > 0 &&  <Popconfirm
                    title="确认要删除吗？"
                    onConfirm={confirm}
                    onCancel={cancel}
                    okText="确认"
                    cancelText="取消"
                  >
                    <Button type="primary">删除</Button>
                  </Popconfirm>
                }
                <Table
                    columns={columns}
                    dataSource={store.search.searchList}
                    rowSelection={rowSelection}
                    pagination={pagination}
                    rowKey="id"
                    onChange={handleTableChange}
                />
            </main>

        </div>
    )
}

export default observer(index);

