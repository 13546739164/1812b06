import React, { Key, useEffect } from 'react'
import { Table, Tag, Space, Input, Button, Pagination, Popconfirm, message } from 'antd';
import useStore from '@/context/useStore';
import { useState } from 'react';
import { observer } from 'mobx-react-lite';
import styles from '@/assets/search.less'
import style from './index.less'
import moment from '@/component/moment'
import { Form } from 'antd';
import { ColumnGroupType, ColumnType, TablePaginationConfig } from 'antd/lib/table';


interface IForm{
  [key:string]:string
}

function index() {

    const columns:(ColumnGroupType<never> | ColumnType<never>)[] = [
        {
            title: 'URL',
            width: 200,
            fixed:'left',
            render:(row:IViewItem)=>{
                return <a href={row.url}>{row.url}</a>
            },
        },
        {
            width:200,
            title: 'IP',
            dataIndex: 'ip',
        },
        {
            width:200,
            title: '浏览器',
            dataIndex: 'browser',
        },
        {
            width:200,
            title: '内核',
            dataIndex: 'engine',
        },
        {
            width:200,
            title: '操作系统',
            dataIndex: 'os',
        },
        {
            width:100,
            title: '设备',
            dataIndex: 'keyword',
        },
        {
            width:120,
            title: '地址',
            dataIndex: 'address',
        },
        {
            width:150,
            title: '访问量',
            render: (row: IViewItem) => {
                return <p
                    style={{
                        backgroundColor: "rgb(82, 196, 26)",
                        width: "20px",
                        height: "20px",
                        borderRadius: "50%",
                        color: "#fff",
                        textAlign: "center",
                        lineHeight: "20px"
                    }}
                >{row.count}</p>
            }
        },
        {
            width:200,
            title: '访问时间',
            render: (row: IViewItem) => {
                return <>{moment(row.createAt).format("YYYY-MM-DD HH:mm:ss")}</>
            }
        },
        {
            width:120,
            title: '操作',
            fixed:'right',
            render: (text: string, record: any) => (
                <Space size="middle">
                   <Popconfirm
                    title="确认要删除吗？"
                    onConfirm={confirm}
                    onCancel={cancel}
                    okText="确认"
                    cancelText="取消"
                  >
                    <Button type="primary">删除</Button>
                  </Popconfirm>
                </Space>
            ),
        },
    ];
    const store = useStore();
    const { useForm } = Form;
    const [form] = useForm();
    const [page, setPage] = useState(1)
    const [pageSize, setPageSize] = useState(8)
    const [params,setParams] = useState({})
    const [selectedRowKeys, setSelectedRowKeys] = useState<Key[]>([]);
    const [pagination,setPagination] = useState<TablePaginationConfig>({
        current: 1,
        pageSize: 8,
        total: 36,
        showSizeChanger: true,
    })

    useEffect(() => {
        store.view.getview(page,params,pageSize)
    }, [page,params])

    console.log(store);

     // 改变选框状态
    const onSelectChange = (selectedRowKeys: Key[]) => {
        setSelectedRowKeys(selectedRowKeys)
    }

    const rowSelection = {
        selectedRowKeys,
        onChange: onSelectChange,
    };

    // 提交搜索
    function submit() {
        let values = form.getFieldsValue();
        let params:IForm={}
        for(let key in values){
            values[key] && (params[key]=values[key])
        }
        setParams(params)
    }

    const handleTableChange=(pagination:TablePaginationConfig)=>{
             // 修改每页几条
          setPagination(pagination)
          // 修改当前页数
          setPage(pagination.current as number)
          // 修改条数
          setPageSize(pagination.pageSize as number)
    }

    function confirm(e?: React.MouseEvent<HTMLElement, MouseEvent> | undefined) {
        console.log(e);
        store.view.deleteView(selectedRowKeys as string[])
       
      }
      
      function cancel(e?: React.MouseEvent<HTMLElement, MouseEvent> | undefined) {
        message.error('Click on No');
    }

    return (
        <div className={styles.index}>
            <header className={styles.head} style={{height:"200px"}}>

                <Form
                    form={form}
                    onFinish={submit}
                >
                      <div style={{display:'flex',flexWrap:"wrap"}}>
                      <Form.Item
                            name="ip"
                            label="IP"
                            style={{ width: "23%", marginRight: "20px",textAlign:"center"}}
                        >
                            <Input type="text" placeholder=" 请输入类型" />
                        </Form.Item>
                        <Form.Item
                            name="UAEmail"
                            label="UA"
                            style={{ width: "23%", marginRight: "20px",textAlign:"center"}}
                        >
                            <Input type="text" placeholder=" 请输入搜索词" />
                        </Form.Item>
                        <Form.Item
                            name="url"
                            label="URL"
                            style={{ width: "23%", marginRight: "20px",textAlign:"center"}}
                        >

                            <Input type="text" placeholder=" 请输入搜索量" />
                        </Form.Item>
                        <Form.Item
                            name="address"
                            label="地址"
                            style={{ width: "23%", marginRight: "20px",textAlign:"center"}}
                        >

                            <Input type="text" placeholder=" 请输入搜索量" />
                        </Form.Item>

                        <Form.Item
                            name="browser"
                            label="浏览器"
                            style={{ width: "23%", marginRight: "20px",textAlign:"center"}}
                        >

                            <Input type="text" placeholder=" 请输入搜索量" />
                        </Form.Item>
                        <Form.Item
                            name="engine"
                            label="内核"
                            style={{ width: "23%", marginRight: "20px",textAlign:"center"}}
                        >

                            <Input type="text" placeholder=" 请输入搜索量" />
                        </Form.Item>
                        <Form.Item
                            name="os"
                            label="OS"
                            style={{ width: "23%", marginRight: "20px",textAlign:"center"}}
                        >

                            <Input type="text" placeholder=" 请输入搜索量" />
                        </Form.Item>
                        <Form.Item
                            name="p"
                            label="设备"
                            style={{ width: "23%", marginRight: "20px",textAlign:"center"}}
                        >

                            <Input type="text" placeholder=" 请输入搜索量" />
                        </Form.Item>

                      </div>
                    <div style={{ display: "flex", justifyContent: "space-between" }}>
                        <div></div>
                        <div>
                            <Button htmlType="submit"  type="primary" style={{ marginRight: "20px" }}>搜索</Button>
                            <Button htmlType="reset">重置</Button>
                        </div>
                    </div>
                </Form>

            </header>
            <main className={styles.main}>
                {
                    selectedRowKeys.length>0&& <Popconfirm
                    title="确认要删除吗？"
                    onConfirm={confirm}
                    onCancel={cancel}
                    okText="确认"
                    cancelText="取消"
                  >
                    <Button type="primary">删除</Button>
                  </Popconfirm>
                }
                <Table
                    columns={columns}
                    dataSource={store.view.viewList}
                    rowSelection={rowSelection}
                    scroll={{ x: "100%" }}
                    pagination={pagination}
                    rowKey="id"
                    onChange={handleTableChange}
                />
            </main>
        </div>
    )
}

export default observer(index);