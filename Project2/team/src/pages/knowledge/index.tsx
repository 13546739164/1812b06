import React, { Key } from 'react'
import './index.less'
import useStore from '@/context/useStore';
import { Ionwleges } from '@/types';
import { Form, Input, Select, Button, Table, Badge, Drawer } from 'antd'
import { useEffect, useState } from 'react';
import { observer } from 'mobx-react-lite'
import knowledge from '@/models/modules/knowledge';
import { RouteComponentProps } from 'react-router';
const { useForm } = Form;
const knowledges: React.FC<RouteComponentProps> = (props) => {
    const [form] = useForm();
    const store = useStore();
    const [page, setPage] = useState(1);
    const [selectedRowKeys, setSelectedRowKeys] = useState<Key[]>([]);
    useEffect(() => {
        store.knowledge.getKonwle(page);
    }, [page])
    function submit() {
        let values = form.getFieldsValue();
        console.log(values);
    }
    const [visible, setVisible] = useState(false);
    const showDrawer = () => {
        setVisible(true);
    };
    const onClose = () => {
        setVisible(false);
    };

    return (
        <div className="knowledgea">

            <header className="header">
                <Form
                    form={form}
                    onFinish={submit}
                >
                    <Form.Item
                        name="replyUserName"
                        label="名称"

                    >
                        <Input type="text" placeholder=" 请输入知识库名称" />
                    </Form.Item>
                    <div className="pass">
                        <Form.Item
                            name="pass"
                            label="状态"
                            style={{ marginLeft: "50px" }}
                        >
                            <Select>
                                <Select.Option value="true">已发布</Select.Option>
                                <Select.Option value="false">草稿</Select.Option>
                            </Select>
                        </Form.Item>
                    </div>
                    <div className="buttons">
                        <Button htmlType="submit">搜索</Button>
                        <Button htmlType="reset">重置</Button>
                    </div>
                </Form>
            </header>

            <main className="main">
                <div className='main_box'>
                    <Button type="primary" onClick={showDrawer}>
                        新建+
                    </Button>
                    <Drawer width="600" title="Basic Drawer" placement="right" onClose={onClose} visible={visible}>

                    </Drawer>

                </div>
                7                   <div className="box">
                    {
                        store.knowledge.konseList.map((item, index) => {
                            return <div key={index}>
                                <img src={item.cover} alt="" />
                                <span>{item.title}</span>
                                <span>{item.summary}</span>
                                <div className="box_ext">
                                    <span>编辑</span>
                                    <span>线上发布</span>
                                    <span>  <Button type="primary" onClick={showDrawer}>
                                        设置
                                    </Button>
                                        <Drawer width="600" title="Basic Drawer" placement="right" onClose={onClose} visible={visible}>

                                        </Drawer></span>
                                    <span>删除</span>
                                </div>
                            </div>
                        })
                    }
                </div>
            </main>
            <footer className="footer"></footer>
        </div>
    )
}
export default knowledges
