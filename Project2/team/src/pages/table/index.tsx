import React, { Key } from 'react'
import './index.less'
// import classNames from 'classnames';
import useStore from '@/context/useStore';
import { Itable } from '@/types';
import { Form, Input, Select, Button, Table, Badge } from 'antd'
import { useEffect, useState } from 'react';
import { observer } from 'mobx-react-lite'
import { RouteComponentProps } from 'react-router';
const { useForm } = Form;
const tabels: React.FC<RouteComponentProps> = (props) => {
    const [form] = useForm();
    const store = useStore();
    const [page, setPage] = useState(1);
    const [selectedRowKeys, setSelectedRowKeys] = useState<Key[]>([]);
    useEffect(() => {
        store.table.getKonw(page);
    }, [page])
    function submit() {
        let values = form.getFieldsValue();
        console.log(values);
    }
    function onSelectChange(selectedRowKeys: Key[], items: Itable[]) {
        setSelectedRowKeys(selectedRowKeys);
    }
    const rowSelection = {
        selectedRowKeys,
        onChange: onSelectChange
    }

    const columns = [
        {
            title: '称呼',
            dataIndex: 'name'
        }, {
            title: '路径',
            dataIndex: 'path'
        }, {
            title: '顺序',
            dataIndex: 'order'
        }, {
            title: '阅读量',
            dataIndex: 'views'
        },
        {
            title: '状态',
            render: (row: Itable) => {
                return row.pass ? <Badge status="success" text="已发布" /> : <Badge status="warning" text="草稿" />
            }
        }, {
            title: '发布时间',
            dataIndex: 'createAt'
        }, {
            title: '操作',
            render: (row: Itable) => {
                return <p>
                    <Button>编辑</Button>
                    <Button>下线</Button>
                    <Button>查看访问</Button>
                    <Button>删除</Button>
                </p>
            }
        }]
        const gitNew=() => {
            props.history.push('/article/editor')
        }
    return (

        <div className="index">
            <header className="header">
                <Form
                    form={form}
                    onFinish={submit}
                >
                    <Form.Item
                        name="replyUserName"
                        label="名称"
                    >
                        <Input type="text" placeholder=" 请输入页面名称" />
                    </Form.Item>
                    <Form.Item
                        name="replyUserEmail"
                        label="路径"
                    >
                        <Input type="text" placeholder=" 请输入搜索路径" />
                    </Form.Item>
                    <div className="pass">
                        <Form.Item
                            name="pass" 
                            label="状态"
                        >
                            <Select>
                                <Select.Option value="true">已发布</Select.Option>
                                <Select.Option value="false">草稿</Select.Option>
                            </Select>
                        </Form.Item>
                    </div>
                    <div className="buttons">
                        <Button htmlType="submit">搜索</Button>
                        <Button htmlType="reset">重置</Button>
                    </div>
                </Form>
            </header>
            <main className="main">
                <div className="san">
                    <div className="tops">
                        <span onClick={()=>gitNew()}>新建+</span>
                    </div>
                </div>
                {selectedRowKeys.length ? <section>
                    <Button onClick={() => store.table.updateComment(selectedRowKeys as string[], { pass: true })}>发布</Button>
                    <Button onClick={() => store.table.updateComment(selectedRowKeys as string[], { pass: false })}>下线</Button>
                    <Button onClick={() => store.table.deleteComment(selectedRowKeys as string[])}>删除</Button>
                </section> : null}

                <div className="mains"> <Table rowSelection={rowSelection} columns={columns} dataSource={store.table.konList} /></div>
            </main>
            <footer className="footer"></footer>
        </div>
    )
}
export default observer(tabels)


