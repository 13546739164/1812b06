import useStore from '@/context/useStore';
import { ICommentItem, MailItem } from '@/types';
import { Form, Input, Select, Button, Table, Badge, message,Modal  } from 'antd'
import { useEffect, useState,Key } from 'react';
import { observer } from 'mobx-react-lite'
import styles from './index.less'; // 等于启用
import style from '@/assets/index.less'; // 等于启用
import { makeHtml } from '@/utils/showdown';
const { useForm } = Form;

const Comment: React.FC = () => {
  const [form] = useForm();
  const store = useStore();
  const [page, setPage] = useState(1);
  const [current, setCurrent] = useState<MailItem>();   //储存数组中电击的该县
  const [showModal, setShowedModal] = useState(false)//弹出框显示隐藏
  const [selectedRowKeys, setSelectedRowKeys] = useState<Key []>([])
  const [params, setParams] = useState({});   //储存表单元素
  const [reply, setReply]=useState('')//多行文本框里的值

  useEffect(()=>{
      store.comment.getmailData(page,params)      
  }, [page,params])

  // 得到输入框的数据
  // params：表单元素 输入框里的值
  function submit() {
    let values = form.getFieldsValue();
    // 获取数据添加到params对象里
    let params:{[key:string]:boolean}={}
    for(let key in values){
      values[key]&& (params[key]=values[key])
    }
    // 1：回到第一页
    setPage(1)
    // 更改
    setParams(params)
  }
  // 复选框
  function onSelectChange(selectedRowKeys:Key[], selectedRows:MailItem[]) {
    // selectedRowKeys：所有id
    // selectedRows：得到数组
    setSelectedRowKeys(selectedRowKeys)
  }
    // 回复邮件
    async function replyMail(){
      // reply多行文本框里的值
      
      if (!reply){
          message.warn('请输入回复内容');
          return;
      }
      console.log(current);
      
      let result = await store.comment.replyMail({
          to: current?.from!,
          subject: `回复: ${current?.subject}`,
          html: makeHtml(reply)
      })
      if(result.message !== '邮件发送成功'){
          message.warn(result.message);
      }else{
          message.success(result.message);
      }
      setShowedModal(false);
  }

  const columns = [
   {
    title: '发件人',
    dataIndex: 'from'
  }, {
    title: '收件人',
    dataIndex:'to'
  }, {
    title: '主题',
    dataIndex: 'subject'
  },{
    title:"发送时间",
    dataIndex:"createAt"
  },{
    title: '操作',
    render: (item: MailItem)=>{
        return <div>
          
            <Button onClick={()=>store.comment.deleteMail([item.id])}>删除</Button>
            {/* current储存item */}
            <Button onClick={()=>{
                setShowedModal(true);
                setCurrent(item);
            }}>回复</Button>
        </div>
    }

  }
]
const rowSelection = {
  selectedRowKeys,
  onChange: onSelectChange
}


  return <div className={style.index}>
    {/* 头部 表单 */}
    <div className={style.main}>
      <div className={styles.legacy}>
        <Form className={styles.legacy}
          form={form}
          onFinish={submit}
        >
          <div className={styles.call}>
            <Form.Item
              name="Sender"
              label="发件人"
            >
              <Input type="text" placeholder=" 请输入发件人" />
            </Form.Item>
          </div>
          <div className={styles.email}>
            <Form.Item
              name="Addressee"
              label="收件人"
            >
              <Input type="text" placeholder=" 请输入收件人" />
            </Form.Item>
          </div>
          <div className={styles.email}>
            <Form.Item
              name="Theme"
              label="主题"
            >
              <Input type="text" placeholder=" 请输入主题" />
            </Form.Item>
          </div>
          <div className={styles.btncss}>
            <Button htmlType="submit" className={styles.searchcss}>搜索</Button>
            <Button htmlType="reset" className={styles.resetcss}>重置</Button>
          </div>
        </Form>
      </div>
    </div>
    {/* 刷新按钮 */}
    <p>
      {/* 是否要删除全部 */}
        {selectedRowKeys.length?<Button onClick={()=>store.comment.deleteMail(selectedRowKeys as string[])}>删除</Button>:null}
        {/* 刷新  回到最初 */}
        <Button onClick={()=>{
            setPage(1);
            setParams({...params});
        }}>刷新</Button>
    </p>

    <Table 
            rowSelection={rowSelection}
            // loading={Boolean(store.comment.maildata.length)}
            columns={columns} 
            dataSource={store.comment.maildata} 
            rowKey="id" 
            pagination={{showSizeChanger:true}}
        />

      <Modal 
            visible={showModal}
            onCancel={()=>setShowedModal(false)}
            onOk={()=>replyMail()}   //回复
        >
            <Input.TextArea rows={10} style={{marginTop: '25px'}} placeholder="支持markdown，html和文本回复..." value={reply} onChange={e=>setReply(e.target.value)}></Input.TextArea>
      </Modal>

  </div>
}

export default observer(Comment);
