import React, { Key, useEffect } from 'react'
import { Table, Tag, Space, Input, Button, Select, Divider, Badge, Pagination } from 'antd';
import useStore from '@/context/useStore';
import { useState } from 'react';
import { observer } from 'mobx-react-lite';
import styles from '@/assets/search.less'
import style from './index.less'
import moment from '@/component/moment'
import { Form } from 'antd';
import { ColumnGroupType, ColumnType, TablePaginationConfig } from 'antd/lib/table';
import { IUsers } from '@/types';


interface IForm {
    [key: string]: string
}

function index() {

    const columns: (ColumnGroupType<never> | ColumnType<never>)[] = [
        {
            title: '账号',
            width: 200,
            dataIndex: "name"
        },
        {
            width: 200,
            title: '邮箱',
            dataIndex: 'ip',
        },
        {
            width: 200,
            title: '角色',
            render: (data: IUsers) => {
                return <div>
                    {
                        data.role === "visitor" ? '访客' : '管理员'
                    }
                </div>
            }
        },
        {
            width: 200,
            title: '状态',
            render: (data: IUsers) => {
                return <div>
                    {
                        data.status === "active" ? <Badge status="success" text="可用" /> : <Badge status="warning" text="已锁定" />
                    }
                </div>
            }
        },
        {
            width: 200,
            title: '注册日期',
            dataIndex: 'os',
        },
        {
            width: 120,
            title: '操作',
            render: (text: string, record: any) => (
                <Space size="middle">
                    <a>禁用</a>
                    <a>授权</a>
                </Space>
            ),
        },
    ];
    const store = useStore();
    const { useForm } = Form;
    const [form] = useForm();
    const [page, setPage] = useState(1)
    const [params, setParams] = useState({})
    const [pageSize, setPageSize] = useState(8)
    const [pagination, setPagination] = useState({
        current: 1,
        pageSize: 8,
        total: 36,
        showSizeChanger: true,
    })
    const [selectedRowKeys, setSelectedRowKeys] = useState<Key[]>([]);

    useEffect(() => {
        store.subUser.getUser(page, params, pageSize)
    }, [page, params])

    console.log(store);

    // 改变选框状态
    const onSelectChange = (selectedRowKeys: Key[]) => {
        setSelectedRowKeys(selectedRowKeys)
    }

    const rowSelection = {
        selectedRowKeys,
        onChange: onSelectChange,
    };

    // 提交搜索
    function submit() {
        let values = form.getFieldsValue();
        let params: IForm = {}
        for (let key in values) {
            values[key] && (params[key] = values[key])
        }
        setParams(params)
    }

    const handleTableChange = (pagination: any) => {
        // 修改每页几条
        setPagination(pagination)
        // 修改当前页数
        setPage(pagination.current)
        // 修改条数
        setPageSize(pagination.pageSize)
    }

    return (
        <div className={styles.index}>
            <header className={styles.head} style={{ height: "160px" }}>

                <Form
                    form={form}
                    onFinish={submit}
                >
                    <div style={{ display: 'flex', flexWrap: "wrap" }}>
                        <Form.Item
                            name="ip"
                            label="账户"
                            style={{ width: "23%", marginRight: "20px", textAlign: "center" }}
                        >
                            <Input type="text" placeholder=" 请输入类型" />
                        </Form.Item>
                        <Form.Item
                            name="UAEmail"
                            label="邮箱"
                            style={{ width: "23%", marginRight: "20px", textAlign: "center" }}
                        >
                            <Input type="text" placeholder=" 请输入搜索词" />
                        </Form.Item>

                        <Form.Item
                            label="角色"
                            style={{ width: "23%", marginRight: "20px", textAlign: "center" }}
                        >
                            <Select>
                                <Select.Option value="false">管理员</Select.Option>
                                <Select.Option value="true">访客</Select.Option>
                            </Select>
                        </Form.Item>

                        <Form.Item
                            label="状态"
                            style={{ width: "23%", marginRight: "20px", textAlign: "center" }}
                        >
                            <Select>
                                <Select.Option value="true">锁定</Select.Option>
                                <Select.Option value="false">可用</Select.Option>
                            </Select>
                        </Form.Item>

                    </div>
                    <div style={{ display: "flex", justifyContent: "space-between" }}>
                        <div></div>
                        <div>
                            <Button htmlType="submit" type="primary" style={{ marginRight: "20px" }}>搜索</Button>
                            <Button htmlType="reset">重置</Button>
                        </div>
                    </div>
                </Form>

            </header>
            <main className={styles.main}>
                {
                    selectedRowKeys.length > 0 && <div>
                        <Button >启动</Button>
                        <Button style={{ marginLeft: "10px" }}>禁用</Button>
                        <Button style={{ marginLeft: "10px" }}>解除授权</Button>
                        <Button style={{ marginLeft: "10px" }}>授权</Button>
                    </div>
                }
                <Table
                    columns={columns}
                    dataSource={store.subUser.usersList}
                    rowSelection={rowSelection}
                    scroll={{ x: "100%" }}
                    pagination={pagination}
                    rowKey="id"
                    onChange={handleTableChange}
                />
            </main>
        </div>
    )
}

export default observer(index);