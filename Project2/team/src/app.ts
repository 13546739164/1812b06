import { RequestConfig, history } from 'umi';
import {message} from 'antd';
import React from 'react';
import {ResponseError} from 'umi-request'
// 引入mobx的Provider
import StateContext from '@/context/stateContext'
import store from '@/models/index'
import { getToken } from './utils';

// 关闭线上console
process.env.NODE_ENV === 'production'?console.log=()=>{}:null;


// 路由切换配置
const whiteList = ['/login','/register']
export function onRouteChange({ matchedRoutes,location }:any) {
    let authorization = getToken()    
      //  "Bearer undefined"
    if(authorization!==undefined){

    }else{
      if(whiteList.indexOf(location.pathname)===-1){
        history.replace('/login?from='+encodeURIComponent(location.pathname))        
      }
    }
  }

// 覆盖根组件
export function rootContainer(container: React.ReactNode) {
    return React.createElement(StateContext.Provider, {value: store}, container);
}

// 网络请求配置
const baseUrl = 'https://creationapi.shbwyz.com';
export const request: RequestConfig = {
  timeout: 10000,
  errorConfig: {},
  errorHandler: (error: ResponseError)=>{
    if (!error.data){
      message.error(error.toString());
      return;
    }
    if (error.data.statusCode === 400){
      message.error(error.data.msg);
    }else if(error.data.statusCode === 401){
      message.error(error.data.msg);
      history.replace('/login?form='+encodeURIComponent(history.location.pathname))
    }
    return error.data;
  },

  middlewares: [],
  // 请求拦截器
  requestInterceptors: [(url, options) => {
    let authorization = getToken();
    if (authorization){
      options = {...options, headers: {authorization:'Bearer '+authorization}};
    }
    if(!/https?/.test(url)){
      url=`${baseUrl}${url}`
    }
    return {
      url,
      options,
    };
  }],

  // 响应拦截器
  responseInterceptors: [response => {
    const codeMaps: {[key: number]: string} = {
      400: '错误的请求',
      403: '禁止访问',
      404: '找不到资源',
      500: '服务器内部错误',
      502: '网关错误。',
      503: '服务不可用，服务器暂时过载或维护。',
      504: '网关超时。',
    };
    // 处理网络请求错误
    if (Object.keys(codeMaps).indexOf(String(response.status)) !== -1){
      message.error(codeMaps[response.status]);
    }
    return response;
  }],
};