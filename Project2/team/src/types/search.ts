export interface ISearch {
  id: string;
  type: string;
  keyword: string;
  count: number;
  createAt: string;
  updateAt: string;
}