import {TagesItem} from './tage'
export interface EarticleItem{
    title: string;
    content: string;
    html: string;
    toc: string;
    status: string;
    needPassword: boolean;
    publishAt: string;
    tags: any[];
    cover?: any;
    summary?: any;
    password?: any;
    totalAmount?: any;
    id: string;
    views: number;
    likes: number;
    isRecommended: boolean;
    isPay: boolean;
    isCommentable: boolean;
    createAt: string;
    updateAt: string;
}