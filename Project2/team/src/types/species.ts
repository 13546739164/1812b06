export interface SpeciesItem{
    id: string,
    label: string,
    value: string,
    createAt: string,
    updateAt: string,
    articleCount:number
}