export interface ArticleItem{
    category?: Category,
    content: string,
    cover?: string,
    createAt: string,
    html: string,
    id: string,
    isCommentable: boolean,
    isPay: boolean,
    isRecommended: boolean,
    likes: number,
    needPassword: boolean,
    publishAt: string,
    status: string,
    summary?: string,
    tags: Category[],
    title: string,
    toc: string,
    totalAmount: string,
    updateAt: string,
    views: number,
}

export interface Category {

    id: string;
    label: string;
    value: string;
    createAt: string;
    updateAt: string;
  }

export interface CategoryArticle{
    articleCount: number,
    createAt: string,
    id: string,
    label: string,
    updateAt: string,
    value: string,
}
export interface obj{
    page:number,
    pageSize:number
}