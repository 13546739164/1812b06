export interface IUsers { 
  id: string;
  name: string;
  avatar?: any;
  email?: any;
  role: string;
  status: string;
  createAt: string;
  updateAt: string;
}