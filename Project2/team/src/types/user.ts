
export interface ILoginForm{
    name:string;
    password:string;
}

export interface Obj {
    id: string,
    name:  string,
    avatar:  string,
    email: null,
    role:  string,
    status: string,
    createAt: string,
    updateAt:  string,
    token: string
}