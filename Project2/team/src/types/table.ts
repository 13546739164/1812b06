export interface Itable{
    pass: any;
   content: string,
    cover?: string,
    createAt: string,
    html: string,
    id: string,
    isCommentable: boolean,
    isPay: boolean,
    isRecommended: boolean,
    likes: number,
    needPassword: boolean,
    publishAt: string,
    status: string,
    summary?: string,
    name:string,
    order:number,
    path: string,
    title: string,
    toc: string,
    totalAmount: string,
    updateAt: string,
    views: number,
}