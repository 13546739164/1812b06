export interface ICommentItem {
    id: string;
    name: string;
    email: string;
    content: string;
    html: string;
    pass: boolean;
    userAgent: string;
    hostId: string;
    url: string;
    parentCommentId: string;
    replyUserName: string;
    replyUserEmail: string;
    createAt: string;
    updateAt: string;
  }

export interface MailItem {
  id: string;
  from: string;
  to: string;
  subject: string;
  text?: any;
  html: string;
  createAt: string;
}
export interface IMailQuery {
  from: string;
  to: string;
  subject: string;
}
export interface IReplyMail{
  to: string;
  subject: string;
  html: string;
}
