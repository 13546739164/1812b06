const showdown  = require('showdown');
const converter = new showdown.Converter();

// markdown转html
export function makeHtml(md: string){
    return converter.makeHtml(md);
}

// 从html中提取大纲h1-h6
export function makeToc(html: string){
    const reg = /<h([\d]) id="([^<]+)">([^<]+)<\/h([\d])>/gi;
    let ret = null;
    const toc = [];
    while ((ret = reg.exec(html)) !== null) {
      toc.push({ level: ret[1], id: ret[2], text: ret[3] });
    }
    return JSON.stringify(toc);
}
