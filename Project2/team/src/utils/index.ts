import Cookie from 'js-cookie';
import {Obj} from '@/types/index'
const key = 'authorization';
const obj="data"
// 存储登陆态
export function setToken(data:Obj){
    Cookie.set(key, data.token, {expires: 1});
    // console.log(JSON.stringify(data));
    Cookie.set(obj,JSON.stringify(data));
}

// 获取登陆态
export function getToken(){
    return Cookie.get(key);
}

// 清除登陆态
export function removeToken(){
    Cookie.remove(key);
}

export function getData(){
    return Cookie.get(obj);
}
