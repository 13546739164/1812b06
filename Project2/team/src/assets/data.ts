export const data = [
    {
        path: "/",
        name: "工作台"
    },
    {
        name: "文章管理",
        children: [
            {
                path: "/article",
                name: "所有文章"
            },
            {
                path: "/article/category",
                name: "分类管理"
            },
            {
                path: "/article/tags",
                name: "标签管理"
            },
        ]
    },
    {
        path: "/table",
        name: "页面管理"
    },
    {
        path: "/knowledge",
        name: "知识小册"
    },
    {
        path: "/poster",
        name: "海报管理"
    },
    {
        path: "/comment",
        name: "评论管理"
    },
    {
        path: "/mail",
        name: "邮件管理"
    },
    {
        path: "/file",
        name: "文件管理"
    },
    {
        path: "/search",
        name: "搜索记录"
    },
    {
        path: "/viewss",
        name: "访问记录"
    },
    {
        path: "/user",
        name: "用户管理"
    },
    {
        path: "/settingss",
        name: "系统设置"
    },
]



export const navlist = [
    {
        name: "文章管理",
        path: "/article"

    },
    {
        path: "/comment",
        name: "评论管理"
    },
    {
        path: "/file",
        name: "文件管理"
    },
    {
        path: "/user",
        name: "用户管理"
    },
    {
        path: "/viewss",
        name: "访问管理"
    },
    {
        path: "/settingss",
        name: "系统设置"
    }
]