// 引入mobx模块
import User from './modules/user'
import Search from './modules/search'
import Setting from './modules/setting'
import View from './modules/view'
import Article from './modules/article'
import Table from './modules/table'
import Subcriber from './modules/subcriber'
import Knowledge from './modules/knowledge'
import Comment from './modules/mail'
import Tages from './modules/tage'
import Species from './modules/species'
import Earticle from './modules/Earticle'
export default {
    // 搜索记录
    search: new Search(),
    // 访问统计
    view: new View(),
    // 用户管理
    user: new User(),
    //页面管理
    table: new Table(),
    // 系统设置
    set: new Setting(),
    // 文章数据
    article:new Article(),
    // 用户管理
    subUser:new Subcriber(),
    //评论管理数据
    comment: new Comment(),

    /* article editor */ 
    // 分类选择 抽屉下拉框
    species:new Species(),
    // 选择标签
    tages:new Tages(),
    // 发布信息
    eAarticle:new Earticle(),
        // 小册数据
        knowledge: new Knowledge()
}



