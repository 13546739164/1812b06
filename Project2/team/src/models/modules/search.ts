import { deleteSearch, getSearch } from "@/services";
import { message } from "antd";
import { makeAutoObservable, runInAction } from "mobx"

class Search{
    searchList = []
    searchTotal=0
    constructor(){
        makeAutoObservable(this);
    }
    // 获取列表数据
    async getSearch(page=1,params:{[key:string]:string},pageSize:number){
        let result = await getSearch(page,params,pageSize);
        console.log('result...', result);
        if (result.data){
           runInAction(()=>{   
              this.searchList=result.data[0] 
              this.searchTotal = result.data[1]
           })
        }   
    }

    //删除数据
    async delete(ids:string[]){
       message.loading('操作中', 0);
       Promise.all(ids.map(id=>deleteSearch(id)))
       .then(res=>{
          message.destroy()
          message.success('删除成功');
          this.getSearch()
       })
       .catch(error=>{
          message.destroy()
          message.error('删除失败');
       })
    }
}

export default Search;