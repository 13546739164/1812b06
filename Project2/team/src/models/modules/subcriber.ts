import { getUser, deleteUser } from "@/services/modules/subscriber";
import { message } from "antd";
import { makeAutoObservable, runInAction } from "mobx"

class Subcriber{
    usersList = []
    constructor(){
        makeAutoObservable(this);
    }
    // 获取列表数据
    async getUser(page=1,params:{[key:string]:string},pageSize:number){
        let result = await getUser(page,params,pageSize);
        console.log('result...', result);
        if (result.data){
           runInAction(()=>{
              this.usersList=result.data[0] 
           })
        }   
    }

    //删除数据
    async delete(ids:string[]){
       message.loading('操作中', 0);
       Promise.all(ids.map(id=>deleteUser(id)))
       .then(res=>{
          message.destroy()
          message.success('删除成功');
          this.getUser()
       })
       .catch(error=>{
          message.destroy()
          message.error('删除失败');
       })
    }
}

export default Subcriber;