import { getSpecies } from "@/services";
import { makeAutoObservable, runInAction } from "mobx"
import { SpeciesItem } from '@/types/species'
class Species{
    specieslist: SpeciesItem[]=[];
    constructor(){
        makeAutoObservable(this);
    }
    async getSpecies(){
        let result = await getSpecies()
        if (result.data){
            runInAction(()=>{
                this.specieslist = result.data;
            })
        }
    }
}

export default Species;

