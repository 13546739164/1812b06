import { getKonw,updateComments,deleteComments } from "@/services";
import { Itable } from "@/types";
import { makeAutoObservable, runInAction } from "mobx";
import {message} from 'antd';
class knowledge{
  
    konList:Itable[]=[];
    updatetable:number=0;
        
    constructor(){
        makeAutoObservable(this);
    }
    async getKonw(page=1){
        let result = await getKonw(page);
        if (result.data){
           runInAction(()=>{
              this.konList=result.data[0] 
           })
        }   
        console.log(this.konList);
        
    }
     // 批量更改评论状态
     async updateComment(ids: string[], status:{[key:string]:boolean}){ 
        message.loading('操作中', 0);
        Promise.all(ids.map(id=>updateComments(id, status)))
        .then(res=>{
            message.destroy()
            message.success('操作成功');
            this.getComment();
        })
        .catch(err=>{
            message.destroy()
            message.error('操作失败');
        })
    }
    getComment() {
        throw new Error("Method not implemented.");
    }

    // 批量删除评论
    async deleteComment(ids: string[]){ 
        message.loading('操作中', 0);
        Promise.all(ids.map(id=>deleteComments(id)))
        .then(res=>{
            message.destroy()
            message.success('删除成功');
            this.getComment();
        })
        .catch(err=>{
            message.destroy()
            message.error('删除失败');
        })
    }
}



export default knowledge;