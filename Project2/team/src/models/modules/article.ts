import { getArticle,updateArticle,deleteArticle,stickArticle,getCategory,postCategory,getTags,postTags } from "@/services";
import {ArticleItem,CategoryArticle} from "@/types"
import { makeAutoObservable, runInAction } from "mobx"
import {message} from 'antd'

class Article{
    articleList:ArticleItem[]=[]
    CategoryList:CategoryArticle[]=[]
    TagsList:CategoryArticle[]=[]
    articleCount:number=0;
    constructor(){
        makeAutoObservable(this);
    }
    async getArticle(page=1,pageSize=12){
        let result = await getArticle(page,pageSize);
        console.log('999999999999999', result.data[0]);
        console.log('88888888888888',result.data[1]);
        
        if (result.data){
           runInAction(()=>{
              this.articleList=result.data[0] 
              this.articleCount=result.data[1]
           })
        }   
    }

    // 批量更改发布状态
    async updateArticle(ids: string[], status:{[key:string]:boolean}){ 
        message.loading('操作中', 0);
        Promise.all(ids.map(id=>updateArticle(id, status)))
        .then(res=>{
            message.destroy()
            message.success('操作成功');
            this.getArticle();
        })
        .catch(err=>{
            message.destroy()
            message.error('操作失败');
        })
    }

    // 批量更改首焦状态
    async stickArticle(ids: string[], status:{[key:string]:boolean}){ 
        message.loading('操作中', 0);
        Promise.all(ids.map(id=>stickArticle(id, status)))
        .then(res=>{
            message.destroy()
            message.success('操作成功');
            this.getArticle();
        })
        .catch(err=>{
            message.destroy()
            message.error('操作失败');
        })
    }

// 批量删除评论
async deleteArticle(ids: string[]){ 
    message.loading('操作中', 0);
    Promise.all(ids.map(id=>deleteArticle(id)))
    .then(res=>{
        message.destroy()
        message.success('删除成功');
        this.getArticle();
    })
    .catch(err=>{
        message.destroy()
        message.error('删除失败');
    })
}
// 文章分类页面数据获取
async getCategory(){
    let result = await getCategory();
    console.log('777777777777777', result.data);
    if (result.data){
       runInAction(()=>{
          this.CategoryList=result.data 
       })
    }   
}
// 文章分类页面数据添加
async postCategory(data:CategoryArticle){
    let result=await postCategory(data);
    if (result){
      this.getCategory()  
     }
     return result;
}

// 文章标签页面数据获取
async getTags(){
    let result = await getTags();
    console.log('6666666666666666', result.data);
    if (result.data){
       runInAction(()=>{
          this.TagsList=result.data 
       })
    }   
}
// 文章标签页面数据添加
async postTags(data:CategoryArticle){
    let result=await postTags(data);
    if (result){
      this.getTags()  
     }
     return result;
}

}

export default Article;