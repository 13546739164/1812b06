import { getTages } from "@/services";
import { TagesItem } from "@/types/tage";
import { makeAutoObservable, runInAction } from "mobx"

class Tages{
        TagesInfo: TagesItem[]= [];
    constructor(){
        makeAutoObservable(this);
    }
    async getTages(){
        const result=await getTages()
        runInAction(()=>{
           this.TagesInfo=result.data
        })
    }
}

export default Tages;

