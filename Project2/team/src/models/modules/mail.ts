
import { getComment ,updateComment,deleteComment, getmailData, deleteMail, replyMail} from "@/services";
import { ICommentItem,IMailQuery,IReplyMail,MailItem } from "@/types";
import { makeAutoObservable, runInAction } from "mobx"
import {message} from 'antd';
class Comment{
    commentList: ICommentItem[] = [];
    commentCount: number = 0;
    maildata:MailItem[]=[];
    mailNum:number=0
    constructor(){
        makeAutoObservable(this);
    }

    async getComment(page=1,pageSize=12){
        let result = await getComment(page,pageSize);
        console.log( result.data[0]);
        
        if (result.data){
            runInAction(()=>{
                this.commentCount = result.data[1];
                this.commentList = result.data[0];
            })
        }
    }
    async updateComment(ids: string[], status:{[key:string]:boolean}){ 
        message.loading('操作中', 0);
        Promise.all(ids.map(id=>updateComment(id, status)))
        .then(res=>{
            message.destroy()
            message.success('操作成功');
            this.getComment();
        })
        .catch(err=>{
            message.destroy()
            message.error('操作失败');
        })
    }
    async deleteComment(ids: string[]){ 
        message.loading('操作中', 0);
        Promise.all(ids.map(id=>deleteComment(id)))
        .then(res=>{
            message.destroy()
            message.success('删除成功');
            this.getComment();
        })
        .catch(err=>{
            message.destroy()
            message.error('删除失败');
        })
    }
    // 获取邮件数据
    async getmailData(page=1,params={}){
        let result=await getmailData(page,params)
        if(result.data){
            runInAction(()=>{
            this.maildata=result.data[0]
            this.mailNum=result.data[1]
        })
        }
        
    }
    
    // 删除邮件
    async deleteMail(ids: string[]){
        message.loading('操作中');
        Promise.all(ids.map(id=>deleteMail(id)))
        .then(res=>{
            message.destroy();
            message.success('批量删除成功');
            this.getmailData();
        })
        .catch(err=>{
            message.success('批量删除失败');
            this.getmailData();
        })
    }

    // 回复邮件
    async replyMail(data: IReplyMail){
        let result = await replyMail(data);
        return result;
    }

}

export default Comment;