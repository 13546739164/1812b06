import { getSet } from "@/services";
import { ISearchData } from "@/types";
import { makeAutoObservable } from "mobx"

class Setting{
    constructor(){
        makeAutoObservable(this);
    }
    async getSet(data: ISearchData){
        let result = await getSet(1);
        console.log('result...', result);
        if (result.data){
            return result.data;
        }   
    }
}

export default Setting;