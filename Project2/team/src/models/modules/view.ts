import { deleteSearch, deleteView, getView } from "@/services";
import { message } from "antd";
import { makeAutoObservable, runInAction } from "mobx"

class View{
    viewList = []
    constructor(){
        makeAutoObservable(this);
    }
    async getview(page=1,params:{[key:string]:string},pageSize:number){
        let result = await getView(page,params,pageSize);
        console.log('result...', result);
        if (result.data){
            runInAction(()=>{
                this.viewList=result.data[0]
            })
        }   
    }
    // 删除
    async deleteView(ids:string[]){
        message.loading('操作中')
        Promise.all(ids.map(id=>deleteView(id)))
        .then(res=>{
            message.destroy()
            message.success('删除成功')
            this.getview()
        })
        .catch(error=>{
            message.destroy()
            message.error('删除失败')
        })
    }


}

export default View;