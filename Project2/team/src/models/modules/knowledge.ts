import { getKonwle } from "@/services";
import { makeAutoObservable, runInAction } from "mobx"
import { Ionwleges } from "@/types";
import {message} from 'antd';
class knowledge{
    konseList:Ionwleges[] = []
    constructor(){
        makeAutoObservable(this);
    }
    async getKonwle(page=1){
        let result = await getKonwle(page);
        console.log('results...', result);
        if (result.data){
           runInAction(()=>{
              this.konseList=result.data[0] 
           })
        }   
    }
}

export default knowledge;