import { getEarticle } from '@/services'
import { EarticleItem } from '@/types/Earticle';
import {makeAutoObservable, runInAction} from 'mobx'

class Earticle{
    constructor(){
        makeAutoObservable(this)
    }
    async getEarticle(data:EarticleItem){
        let result=await getEarticle(data)
        return result
    }
}


export default Earticle





