'use strict';

const Controller = require('egg').Controller;
let OSS = require('ali-oss');
const path = require('path');
const fs = require('fs');
const savePath = path.resolve(__dirname, '../public');

const client = new OSS({
  region: 'oss-cn-hangzhou',
  accessKeyId: 'LTAI5tQUpWsha3xPJWqCfVht',
  accessKeySecret: 'VeunvyrEV7HoO3B9uaQABwChnlOwAu'
});

class FileController extends Controller {
  async index() {
    const { ctx } = this;
    ctx.body = 'hi, egg';
  }

  // 文件上传到oss
  async create() {
    client.useBucket('jason111');
    const { ctx } = this;
    if (ctx.header['content-type'] === 'application/octet-stream') {
      const stream = this.ctx.req
      const Dirstream = await ctx.getFileStream();
      console.log('stream...',  Dirstream);
      try {
        // let result = await client.put( Dirstream.fieldname,  Dirstream);
        // console.log(result);
        // ctx.body = result;
      } catch (err) {
        //  console.log (err);
      }
    } 
  }

  

  //  获取oss存储桶列表
  async listBuckets() {
    const { ctx } = this;
    try {
      let result = await client.listBuckets();
      ctx.body = result;
    } catch (err) {
      console.log(err)
    }
  }

  // 获取指定桶内的文件
  async listFile(){
    const { ctx } = this;
    const bucket = ctx.request.body.bucket || 'jason111';
    client.useBucket(bucket);
    try {
      let result = await client.list({
        'max-keys': 5
      })
      ctx.body = result.objects;
    } catch (err) {
      console.log (err)
    }
  }

  // 保存文件切片
  async saveChunk(){
    const {ctx} = this;
    // console.log('ctx........',ctx);
    let stream = await ctx.getFileStream();
    console.log('stream....',stream);
    let {index} = stream.fields;
    try{
      // 创建切片存储文件夹
      let chunkDirPath = path.join(savePath, '/chunk');
      if (!fs.existsSync(chunkDirPath)) {
        fs.mkdirSync(chunkDirPath);
      }
      // 存储切片
      let filePath = path.join(chunkDirPath,`/chunk_${index}`);
      let file = fs.createWriteStream(filePath);
      stream.pipe(file);
      ctx.body = {'msg': '文件切片存储成功'}
    }catch(e){
      ctx.body = {'msg': '文件切片存储失败'}
    }
  }

  // 合并文件切片
  async mergeChunk(){
    const {ctx} = this;
    let {total,filename, chunkname} = ctx.request.body;
    let index = 1;
    try{
      fs.writeFileSync(path.join(savePath, `/${filename}`), '');
      while(total >= index){
        fs.appendFileSync(path.join(savePath, `/${filename}`), fs.readFileSync(path.join(savePath, `/${chunkname}/chunk_${index}`)));
        index++;
      }
      ctx.body = {'msg': '合并切片成功'}
    }catch(e){
      ctx.body = {'msg': '合并切片失败'}
    }
  }
}

module.exports = FileController;
