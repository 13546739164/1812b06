

const Controller = require('egg').Controller;
  // TypeScript
  const AlipaySdk =require ('alipay-sdk');
  const  AlipayFormData =require ('alipay-sdk/lib/form');
  
  // 普通公钥模式
  const alipaySdk = new AlipaySdk({
      // 参考下方 SDK 配置
      appId: '2021000118608283',
      privateKey: 'MIIEogIBAAKCAQEAgD70t2UiOsAC7XeOHdrXNaQhHdQZNvr9grys2z75rPc7j+Yf0tTyCrZIN491TmwNXmfLvFoLFWdXe9OfQ6N6ZvLxC5KgmCJ8fKa38nMNcihYaUesMpu3dP400b1lE3EBCI3WrXEuq7gtrHTeygiJEoG1LeE7+SnBAnsmaZHxv3ULCE0/prPMEM55ywLY3OFw9F4nrx4ZSPrPB6SrRIWSvUAnqVUNkvtoCBBH4Dtu56zfPKgCNGwOkgLh842cpoxJCWuc9FaLYP+aFS4h52TwF/v4l1koFVY4MZ8uk7nR3OCm+FtUixV8FnKVbXZmqMF4Fv/ZfC0SiKpYVnouaoZnmwIDAQABAoIBAE719DiCtUuU83iAVuk816q4ToeOEqRObw4y2v72zEVZx7t2xftKj2hXCSnkUyag0q5gnAlxK0fXY/7isjm0CuYeCJnGgsiI2zrob3OAsTOrcZwIvBZA1/jZl23X/ca1mhxJMHeDq2ehM8XuA0pDbRoO8J26AvILakH7zw/LAxyMtQxeFwRn+9EBNX3RnIK45vTn4xt5ga2nTqoAk0hrMGjOMeCbq8Xdg7233fiT5uvC6bTojHm7Nzw3nlOu9IiPU8u2RMjwdKToAWtAZvIDydAz+n5cQ3LaUhZivbXXAqY+vFxRmOsIadM6b0SS636RtpJfudpHdpsCLdK77fWAMnkCgYEAxI0IiNpQOQhwJHZWoGpnNmy2g2xHxJrlvTqqeOFBdpI1UMoaX5HTGRjWg5F3l/JjFO9aK16cBLkQ0ZBHKJF8pZ6OSqoWplNcLiYDNJD9eWFwHPRUG044zTVQpXNB5mSBYD3aYu5qSARLDJW2UTbf4FUltjRnBp6FKyOLpXtal60CgYEApwkOk1FY2DyYR35cVKOmVIE6oiCJMGoxFYXZe9EqdgQ8kXW+s+heS3MwrwhGIubjPm+gEMuTKjNlx/ciZBJ/tWqCQ7WeliPlgIhb5dEci21nZIa2fuLakW8fBbnLKR9tt+4W9dVT7oUzahD+d5UIqQhEEu/1sxQOph9V8WwxBWcCgYAC5m3UzxvV65j0AMcAcPF67Rm4ULWyYfK32xC+3dwAauHcgk7nUBiES0iHAnN4S3xIx8tVznyQ2UY1QWK7Z4ImU3OYCHBzHdHDYI41jVogG17zHmvLR1kxNfMtrciwJJWAHQmG4iuxSlbtJZxeEGg3QCphSN6oS1h9rXWXCH3qtQKBgE6pPjr0MSHMD26DvFmDo93h6cnWBqVNLQZUvWHI0/U184wQ5hj1Ugq4GXIkNFtVXijWCow+QnS6NbFg2cpEx+2YVfYq/p74EqlgDd8Bhi7G+8zkf+brUEDzEqnwoBMioNngfuGQb/FAu0C09hE8lxJ7OQuuA4gs5uUmlqOV7tP7AoGAc8tT2lZYTm0U70g/hPqm2hVvr6rCvj1SUO2ccbscwZBlg6HYelAAOnq/78U06o4atX5FC4vcTYEDkSB9pf5j5ILkMBQyu2Nqml18MBfNaR1zXEeYx0Dqy8XAuhFmaAHf7D4rn1raSu+4hukQ0pmKUqgBB3SdXPRDY425NPHHla4=',
      //可设置AES密钥，调用AES加解密相关接口时需要（可选）
      encryptKey: 'K3jQygKog5YHzT3QQ8lRSQ=='
  });
class PayController extends Controller {
  async index() {
    const { ctx } = this;
    ctx.body = 'hi, egg';
  }
  create(){
    let {ctx}=this
    const {totalAmount,id}=ctx
     
      const formData = new AlipayFormData();
      formData.addField('notifyUrl', 'http://www.com/notify');
      
      formData.addField('bizContent', {
      outTradeNo: id,
      productCode: 'FAST_INSTANT_TRADE_PAY',
      totalAmount:totalAmount,
      subject: '商品',
      body: '商品详情',});
     alipaySdk.exec(
      'alipay.trade.page.pay',
      {},
      { formData: formData },
      ).then(result=>{
          // // result 为 form 表单
          console.log(result);
          ctx.body={
            code: 200, data: result
          }
         
      }).catch(err=>{
          ctx.body={
            code: 500, msg: err.toString()
          }
        
        })
      }
}

module.exports = PayController;
