import { request } from 'umi';
// 获取文章列表
export function getArticleList(page: number,type:string='', pageSize = 12, status = 'publish'){
    return request(`/api/article/${type}`, {
        // 表示请求地址栏上面的查询参数
        params: {
            page,
            pageSize,
            status,      
        },
        // // 表示请求体里面传递的数据 post请求等等
        // data: {},
    })
}

// 获取推荐文章
export function getRecommend(id:string){
    return request('/api/article/recommend',{params:{
        articleId:id
    }});
}

// 文章页的右侧tag请求

export function getTags(){
    return request(`/api/tag?articleStatus=publish`);
}

// 获取文章页标题

export function getTitles(){
    return request(`/api/category?articleStatus=publish`);
}

// 获取views数据

export function getViews(id:string){
    return request(`/api/article/${id}/views`,{
        method:"post"
    });
}

// 目录

export function getCatalog(id:string){
    return request(`/api/article/${id}`);
}

// 获取tag/git数据

export function getArticleTag(page: number,type:string, pageSize = 12, status = 'publish'){
    return request(`/api/article/tag/${type}`, {
        // 表示请求地址栏上面的查询参数
        params: {
            page,
            pageSize,
            status,      
        },
        // // 表示请求体里面传递的数据
        // data: {},
    })
}











