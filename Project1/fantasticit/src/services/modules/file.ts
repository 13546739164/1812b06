import { request } from 'umi';
//归档数据请求
export function gitFile(){
    return request('/api/article/archives');
}
// 
export function gitclass(publish:string){    
    return request(`/api/category?articleStatus=${publish}`);
}
// 获取文章详情
export function getArticleDetail(id: string){
    return request(`/api/article/${id}/views`, {
        method: 'POST'
    })
}

// 获取文章评论
export function getArticleComment(id: string, page=1, pageSize=6){
    return request(`/api/comment/host/${id}?page=${page}&pageSize=${pageSize}`)
}

