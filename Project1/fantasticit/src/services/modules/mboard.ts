import { request } from 'umi';
import {Comments} from '@/types'
import {message} from 'antd'
let hostId=""
export function getMboard(page:number,hostId:string, pageSize = 6,) {
    return request(`/api/comment/host/${hostId}`, {
        //    获取地址栏数据
        params:{
           page,
           pageSize
        },
        // 数据
        data:[]
    });
}
//添加数据
export function getComment(params:Comments) {
    if(params){
        message.info('添加成功，管理员待审核！')
    }
    params.hostId? hostId=params.hostId:""
    return request('/api/comment', {
        method:"POST",
        data:params
    });
}



