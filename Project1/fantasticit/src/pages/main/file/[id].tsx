import HightLight from '@/component/HighLight';
import ImageView from '@/component/ImageView';
import { IRootState } from '@/types';
import { fomatTime } from '@/utils';
import React, { useEffect, useState } from 'react';
import moment from '../../../component/moment'
import { IRouteComponentProps, useDispatch, useSelector } from 'umi'
import "./indexid.less"
const ArticleDetail: React.FC<IRouteComponentProps<{ id: string }>> = props => {
    let id = props.match.params.id;
    console.log('id..', id);
    const dispatch = useDispatch();
    const { articleDetail, articleComment } = useSelector((state: IRootState) => state.file)
    const [commentPage, setCommentPage] = useState(1);
    const recommends = useSelector((state: IRootState) => state.article);
    const classlist = useSelector((state: IRootState) => state.file.classList);
    // 分类数据
    useEffect(() => {
        dispatch({
            type: 'file/gitclass'
        })
    }, []);
    // 获取推荐读
    useEffect(() => {
        dispatch({
            type: 'article/getRecommend'
        })
    }, []);
    useEffect(() => {
        dispatch({
            type: 'file/getArticleDetail',
            payload: id
        })
    }, [])

    useEffect(() => {
        dispatch({
            type: 'file/getArticleComment',
            payload: {
                id,
                page: commentPage
            }
        })
    }, [commentPage]);
    const goId=(id:string)=>{
        props.history.push(`/main/article/detail/${id}`)
    }
    if (!Object.keys(articleDetail).length) {
        return null;
    }
    return (
        <div className="file_id">
            <div className='content'>
                <div className="file_id_left">
                    <ImageView>
                        {articleDetail.cover && <img src={articleDetail.cover} />}
                        <h1>{articleDetail.title}</h1>
                        <p>
                            发布于{fomatTime(articleDetail.publishAt!)}•阅读量{articleDetail.views}
                        </p>
                        <HightLight>
                            <div dangerouslySetInnerHTML={{ __html: articleDetail.html! }}></div>
                        </HightLight>
                        <p>发布于{fomatTime(articleDetail.publishAt!)} | 版权信息：非商用-署名-自由转载</p>
                    </ImageView>
                </div>

                <div className="file_id_right">
                    <div className="right_top">
                    <div className='right_top_tit'>
                        <h3>推荐阅读</h3>
                    </div>
                        {
                            recommends.recommend.map((item, index) => {
                                return (
                                    <div key={index} className="item_top" onClick={()=>goId(item.id)}>
                                        <span>{item.title}</span> ·
                                        <span>{moment(item.createAt).fromNow()}</span>
                                    </div>
                                )
                            })
                        }

                    </div>
                   
                </div>
            </div>

        </div>
    )

}

export default ArticleDetail;
