import React from "react"
import { useSelector, useDispatch } from 'umi'
import './index.less'
import { useEffect, useState } from 'react';
import { RouteComponentProps } from 'react-router-dom'
import Content from '@/component/content'
import MboardC from '@/component/mboardInput'
import {  Pagination } from 'antd';
import { IRootState} from '@/types'
const Contentmodule=React.memo(Content)
const Guestbook: React.FC<RouteComponentProps> = (props) => {
    const [page, setPage] = useState(1)
    const dispatch = useDispatch()
    // message board 
    const comment = useSelector((state: IRootState) => state.mboard)
    // 获取数据
    useEffect(() => {
        dispatch({
            type: 'mboard/getMboard',
            payload: {
                page,
                hostId:"8d8b6492-32e5-44e5-b38b-9a479d1a94bd"
            }
        })
    }, [page])

    // 跳详情
    const detail = (id: string) => {
        props.history.push(`/main/article/detail/${id}`)
    }
     // // 页码更改
    const pagechange = (page: number) => {
        setPage(()=>page=page)        
    }
 
    return <div className="mborad">
        {/* 头部 */}
        <div className="container"><div className="_1TDituBtews_ULM98bSKdc"><div className="markdown"><h2 style={{ textAlign: "center", margin: " 0 !important", border: "0" }}>留言板</h2>
            <p style={{ textAlign: "center", margin: " 0 !important" }}>
                <strong>请勿灌水 🤖</strong>
            </p>
        </div>
        </div>
        </div>

        <div className="wrap">
            <p>评论</p>
            <MboardC />
             {/* 页码 */}
             <Pagination defaultCurrent={1} total={comment.commentnum} pageSize={6} onChange={pagechange} />
            <h3>推荐文章</h3>
             {/* 文章页内容 */}
             <Contentmodule detail={detail}></Contentmodule>
        </div>
    </div>
}
export default Guestbook


