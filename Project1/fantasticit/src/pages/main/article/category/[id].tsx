import { IRootState } from '@/types';
import { useEffect, useState } from 'react';
import { RouteComponentProps } from 'react-router-dom'
import { useDispatch, useSelector } from 'umi';
import styles from './index.less'; // 等于启用了css-module
import Recommed from '@/component/recommed';
import Content from '@/component/content'
import Nav from '@/component/navlink'
import Tag from '@/component/tag'
import InfiniteScroll from 'react-infinite-scroll-component';

const index: React.FC<RouteComponentProps<{ id: string }>> = (props) => {
  // const type=props.history.location.pathname.split('/')[4];
  const type = props.match.params.id

  let [page, setPage] = useState(1);
  const dispatch = useDispatch();
  const article = useSelector((state: IRootState) => state.article);
  const tit = article.articleTitles.filter(item => item.value === type)

  //  文章标题
  useEffect(() => {
    dispatch({
      type: 'article/getTitles'
    })
  }, []);
  // 文章的tags
  useEffect(() => {
    dispatch({
      type: 'article/getTags'
    })
  }, []);
  // 获取右侧数据
  useEffect(() => {
    dispatch({
      type: 'article/getRecommend'
    })
  }, []);
  // 获取文章页数据
  useEffect(() => {
    dispatch({
      type: 'article/getArticleList',
      payload: {
        page,
        type: `category/${type}`
      }
    })
  }, [type, page]);
  // 跳详情
  const detail = (id: string) => {
    props.history.push(`/main/article/detail/${id}`)
  }
  // 跳tag详情页
  const tagDetail = (type: string) => {
    props.history.push(`/main/article/tag/${type}`)
  }
  // 无限滚动
  const pullupLoader = () => {
    setPage(page => page + 1)
  }


  return (
    <div className={styles.article}>
      <div className={styles.left}>
        <div className={styles.left_top}>
          <div>
            <p><span>{tit[0] && tit[0].label}</span> 分类文章</p>
            <p>共搜到 <span>{tit[0] && tit[0].articleCount}</span> 篇</p>
          </div>
        </div>
        {/* 左边部分 */}
        <div className={styles.left_bottom}>
          <div className={styles.left_bottom_title}>
            {/* 标题头 */}
            <Nav></Nav>
          </div>
          {/* 内容 */}
          <Content detail={detail}></Content>
        </div>
      </div>

      <div className={styles.right}>

        {/* 右上部分 */}
        <Recommed detail={detail}></Recommed>

        {/* 右下部分 */}
        <Tag tagDetail={tagDetail}></Tag>

      </div>
    </div>
  );
}

export default index


