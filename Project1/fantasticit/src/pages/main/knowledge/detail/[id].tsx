import { IRootState } from '@/types';
import { useEffect, useState } from 'react';
import styles from './detail.less'; // 等于启用了css-module
import { IRouteComponentProps, useDispatch, useSelector } from 'umi';
import moment from '@/component/moment'
const detail:React.FC<IRouteComponentProps<{id:string}>>=(props)=>{
    const dispatch = useDispatch();
    const knowledge=useSelector((state:IRootState)=>state.knowledge)
    const id=props.match.params.id;
    const obj = knowledge.knowledgedetail;
    const arr=knowledge.knowList.filter(item=>item.id!==id);
    console.log(arr);
    

    
    useEffect(() => {
        dispatch({
            type: 'knowledge/getDetail',
            payload:id
          })
      }, [id]);
      const getCli=(id:string)=>{
        props.history.push(`/main/knowledge/detail/${id}`)
    }
    const getClick=(id:string,parentId:string)=>{
      let childId=id;
       props.history.push(`/main/knowledge/request/${parentId},${childId}`)
    }
    return (
        <div className={styles.home}>
          <div className={styles.detail}>
           <p>知识小册/{obj.title}</p>
           <div className={styles.left}>
             <h3>{obj.title}</h3>
             <div className={styles.content}>
               <img src={obj.cover} alt="" />
               <h3 className={styles.cont}>{obj.title}</h3>
               <p className={styles.conter}>{obj.summary}</p>
               <p className={styles.conter}>{obj.views}次阅读·{moment(obj.publishAt).fromNow()}</p>
               <p className={styles.cont}><button onClick={()=>getClick(obj.children[0].id,obj.children[0].parentId)} >开始阅读</button></p>
               <div className={styles.foot}>
                 {
                   knowledge.knowledgedetail.children?knowledge.knowledgedetail.children.map((item,index)=>{
                     return <p key={index} onClick={()=>getClick(item.id,item.parentId)}>{item.title}</p>
                   }):''
                 }
               </div>
             </div>
           </div>
           <div className={styles.right}>
             <h3>其他知识笔记</h3>
             <ul className={styles.right_content}>
               {
                 arr.map((item,index)=>{
                   return <li key={index} onClick={()=>getCli(item.id)}>
                     <h3>{item.title} <span className={styles.fencha}>|</span><span className={styles.sj}>{moment(item.publishAt).fromNow()}</span></h3>
                     <img src={item.cover} alt="" />
                     <p className={styles.summ}>{item.summary}</p>
                    <p className={styles.pan}><span className={styles.tubiao} ><span><svg viewBox="64 64 896 896" focusable="false" data-icon="eye" width="1em" height="1em" fill="currentColor" aria-hidden="true"><path d="M942.2 486.2C847.4 286.5 704.1 186 512 186c-192.2 0-335.4 100.5-430.2 300.3a60.3 60.3 0 000 51.5C176.6 737.5 319.9 838 512 838c192.2 0 335.4-100.5 430.2-300.3 7.7-16.2 7.7-35 0-51.5zM512 766c-161.3 0-279.4-81.8-362.7-254C232.6 339.8 350.7 258 512 258c161.3 0 279.4 81.8 362.7 254C791.5 684.2 673.4 766 512 766zm-4-430c-97.2 0-176 78.8-176 176s78.8 176 176 176 176-78.8 176-176-78.8-176-176-176zm0 288c-61.9 0-112-50.1-112-112s50.1-112 112-112 112 50.1 112 112-50.1 112-112 112z"></path></svg></span>
                    <span>{item.views}</span></span><span>·</span><span className={styles.tubiao}><span><span role="img" aria-label="share-alt"><svg viewBox="64 64 896 896" focusable="false" data-icon="share-alt" width="1em" height="1em" fill="currentColor" aria-hidden="true"><path d="M752 664c-28.5 0-54.8 10-75.4 26.7L469.4 540.8a160.68 160.68 0 000-57.6l207.2-149.9C697.2 350 723.5 360 752 360c66.2 0 120-53.8 120-120s-53.8-120-120-120-120 53.8-120 120c0 11.6 1.6 22.7 4.7 33.3L439.9 415.8C410.7 377.1 364.3 352 312 352c-88.4 0-160 71.6-160 160s71.6 160 160 160c52.3 0 98.7-25.1 127.9-63.8l196.8 142.5c-3.1 10.6-4.7 21.8-4.7 33.3 0 66.2 53.8 120 120 120s120-53.8 120-120-53.8-120-120-120zm0-476c28.7 0 52 23.3 52 52s-23.3 52-52 52-52-23.3-52-52 23.3-52 52-52zM312 600c-48.5 0-88-39.5-88-88s39.5-88 88-88 88 39.5 88 88-39.5 88-88 88zm440 236c-28.7 0-52-23.3-52-52s23.3-52 52-52 52 23.3 52 52-23.3 52-52 52z"></path></svg></span>
                        </span><span>分享</span></span></p>
                     
                   </li>
                 })
               }
             </ul>
           </div>
           </div>
      </div>
    )
}
export default detail
