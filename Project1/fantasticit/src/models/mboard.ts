import {  ICommentItem,Comments} from '@/types';
import { Effect, ImmerReducer, Reducer, Subscription } from 'umi';
import { getMboard,getComment} from '@/services';

export interface MboardModelState {
  mboardcomment:ICommentItem[],
  commentnum:number,
}
 
export interface MboardModelType {
  namespace: 'mboard';
  state: MboardModelState;
  // 异步action，相当于vuex中的action
  effects: {
    getMboard:Effect,
  };
  // 同步action，相当于vuex中的mutation
  reducers: {
    save: Reducer<MboardModelState>;
  };
}

const MboardModel: MboardModelType = {
  namespace: 'mboard',
  state: {
    mboardcomment:[],
    commentnum:0,
  },

  effects: {
    // 留言版数据
    *getMboard({ payload }, { call, put }) {      
      let result = yield call(getMboard,payload.page,payload.hostId);
      if (result.success === true) {
        yield put({
          type: 'save',
          payload: {
            mboardcomment: result.data[0],
            commentnum:result.data[1]
          }
        })
      }
    },
  
  },
  reducers: {
    save(state, action) {
      return {
        ...state,
        ...action.payload,
      };
    },
  }
};

export default MboardModel;