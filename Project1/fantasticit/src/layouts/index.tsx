import classNames from 'classnames';
import React, { useEffect, useState } from 'react';
import { IRootState } from '@/types';
import { NavLink, useIntl, useDispatch, useSelector, setLocale ,Redirect, IRouteComponentProps} from 'umi';
import { Spin, Select, BackTop } from 'antd'
import styles from "./index.less"
import Search from '@/component/search'
import Loading from '@/component/Loading'
import {
    SearchOutlined
} from '@ant-design/icons';
import Theme from '@/component/Theme';

const Layouts:React.FC<IRouteComponentProps> = (props)=>{
    const intl = useIntl()
    const dispatch = useDispatch();
    let [isClose, setIsclose] = useState(false);  
    const { global, language } = useSelector((state: IRootState) => {
        return {
            global: state.loading.global,
            language: state.language
        }
    })
    useEffect(()=>{
        setLocale(language.locale, false);
    }, [language.locale]);

    function changeLocale(locale: string){
        dispatch({ 
            type: 'language/save',
            payload: {locale}
        });
    }

      // 跳详情
  const detail = (id: string) => {
    props.history.push(`/main/article/detail/${id}`)
  }

    const handleSearch=()=>{
        setIsclose(()=>!isClose)    
    }
    const menus = [{
        title: 'menu.artilce',
        link: '/main/article'
    }, { 
        title: 'menu.archives',
        link: '/main/file'
    }, { 
        title: 'menu.knowledge',
        link: '/main/knowledge'
    }, { 
        title: 'menu.message',
        link: '/main/guestbook'
    }, { 
        title: 'menu.about',
        link: '/main/about'
    }]

    return  <div className={classNames(styles.home)} >
        {global && <Loading></Loading>}
        {/* 公共头部 */}
        <div className={styles.head}>
        <header className={classNames(styles.header)}>
        <Redirect to="/main/article">文章</Redirect>
        <span>
        <img src="https://wipi.oss-cn-shanghai.aliyuncs.com/2021-02-20/wipi-logo.png" alt=""></img>
            </span><span>
        {
                menus.map((item,index)=>{
                    return <NavLink key={item.link} activeClassName={classNames(styles.active)} to={item.link}>{intl.formatMessage({
                        id: item.title
                    })}</NavLink>
                })
            }
            </span>
            <span>
            <Theme />
            <h6><SearchOutlined onClick={handleSearch}/></h6><span> </span>
            <Select value={language.locale} onChange={e=>changeLocale(e)}>{
                language.locales.map((item:any)=>{
                    return <Select.Option key={item.locale} value={item.locale}>{item.label}</Select.Option>
                })    
            }</Select> 
            </span> 
        </header>
        </div>

        {/* 路由区域 */}
        <main className={classNames(styles.container)} >
             {props.children}
        </main>

        <BackTop />
        {/* 公共底部 */}
        <footer className={classNames(styles.footer)} >
            <span className={classNames(styles.span)}><svg viewBox="0 0 1024 1024" version="1.1" p-id="4788"  width="24" height="24"><defs><style type="text/css"></style></defs><path d="M512 0C230.4 0 0 230.4 0 512s230.4 512 512 512 512-230.4 512-512S793.6 0 512 0z m-182.4 768C288 768 256 736 256 694.4s32-73.6 73.6-73.6 73.6 32 73.6 73.6-32 73.6-73.6 73.6z m185.6 0c0-144-115.2-259.2-259.2-259.2v-80c185.6 0 339.2 150.4 339.2 339.2h-80z m172.8 0c0-240-195.2-432-432-432V256c281.6 0 512 230.4 512 512h-80z" fill="currentColor"></path></svg>
               <svg viewBox="64 64 896 896" focusable="false" data-icon="github" width="1em" height="1em" fill="currentColor" aria-hidden="true"><path d="M511.6 76.3C264.3 76.2 64 276.4 64 523.5 64 718.9 189.3 885 363.8 946c23.5 5.9 19.9-10.8 19.9-22.2v-77.5c-135.7 15.9-141.2-73.9-150.3-88.9C215 726 171.5 718 184.5 703c30.9-15.9 62.4 4 98.9 57.9 26.4 39.1 77.9 32.5 104 26 5.7-23.5 17.9-44.5 34.7-60.8-140.6-25.2-199.2-111-199.2-213 0-49.5 16.3-95 48.3-131.7-20.4-60.5 1.9-112.3 4.9-120 58.1-5.2 118.5 41.6 123.2 45.3 33-8.9 70.7-13.6 112.9-13.6 42.4 0 80.2 4.9 113.5 13.9 11.3-8.6 67.3-48.8 121.3-43.9 2.9 7.7 24.7 58.3 5.5 118 32.4 36.8 48.9 82.7 48.9 132.3 0 102.2-59 188.1-200 212.9a127.5 127.5 0 0138.1 91v112.5c.8 9 0 17.9 15 17.9 177.1-59.7 304.6-227 304.6-424.1 0-247.2-200.4-447.3-447.5-447.3z"></path></svg></span>
            <span>Designed by Fantasticit . 后台管理</span>
            <span>Copyright © 2021. All Rights Reserved.</span>
            <span>皖ICP备18005737号</span>
        </footer>
        {
            isClose?<Search handleSearch={handleSearch} detail={detail}></Search>:""
        }
     </div>
}

export default Layouts;
