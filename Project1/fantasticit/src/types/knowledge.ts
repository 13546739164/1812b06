export interface IKnowledgeItem {
id: string,
parentId: string,
order: number,
title: string,
cover?: string,
summary?: string,
content: string,
html: string,
toc: string,
status: string,
views: number,
likes: number,
isCommentable: boolean,
publishAt: string,
createAt: string,
updateAt: string,
children: IKnowTable[],
value: any;
  map: any;
  name:string;
  articleCount: number;
  isRecommended: boolean;
  needPassword: boolean;
  tags: Roct[];
  label:string
}

export interface IKnowTable{
    id: string,
parentId: string,
order: number,
title: string,
cover: string,
summary: string,
content: string,
toc: string,
status: string,
views: number,
likes: number,
isCommentable: true
publishAt: string,
createAt: string,
updateAt: string,
value: any;
  map: any;
  name:string;
  articleCount: number;
  isRecommended: boolean;
  needPassword: boolean;
  tags: Roct[];
  label:string
}

export interface IKnowTable{
    html: string;
      id: string,
  parentId: string,
  order: number,
  title: string,
  cover: string,
  summary: string,
  content: string,
  toc: string,
  status: string,
  views: number,
  likes: number,
  isCommentable: true
  publishAt: string,
  createAt: string,
  updateAt: string,
  }

  export interface Roct {

    id: string;
    label: string;
    value: string;
    createAt: string;
    updateAt: string;
    articleCount:number;
  }