export interface IAboutItem {
    article: any;
    articleCount: number;
    id: string;
    name: string;
    email: string;
    content: string;
    html: string;
    pass: boolean;
    userAgent: string;
    hostId: string;
    url: string;
    parentCommentId?: number;
    replyUserName?: string;
    replyUserEmail?: string;
    createAt: string;
    updateAt: string;
    children?: IAboutItem;
    commentnum:number
}


