import { RequestConfig } from 'umi';
import {message} from 'antd';
import NProgress from 'nprogress';
import 'nprogress/nprogress.css';
import { createLogger } from 'redux-logger';
// NProgress.configure({showSpinner:false,trickleSpeed:500})

// 关闭console.log
process.env.NODE_ENV=== 'production' ?console.log=()=>{}:null

// dav 日志
// =process.env.NODE_ENV=== 'production' ?{}: ...关闭日志
export const dva =process.env.NODE_ENV=== 'production' ?{}: {
  config: {
    onAction: createLogger(),
    onError(e: Error) {
      message.error(e.message, 3);
    },
  },
};

// 路由切换配置
export function onRouteChange({ matchedRoutes }:any) {
  NProgress.start();
  let timer = setTimeout(() => {
    clearTimeout(timer);
    NProgress.done();
  }, 2000)
}

const baseUrl = 'https://creation.shbwyz.com';
export const request: RequestConfig = {
  timeout: 5000,
  errorConfig: {},
  middlewares: [],
  // 请求拦截器
  requestInterceptors: [(url, options) => {
    return {
      url: `${baseUrl}${url}`,
      options,
    };
  }],
  // 响应拦截器
  responseInterceptors: [response => {
    const codeMaps: {[key: number]: string} = {
      400: '错误的请求',
      403: '禁止访问',
      404: '找不到资源',
      500: '服务器内部错误',
      502: '网关错误。',
      503: '服务不可用，服务器暂时过载或维护。',
      504: '网关超时。',
    };
    // 处理网络请求错误
    if (Object.keys(codeMaps).indexOf(String(response.status)) !== -1){
      message.error(codeMaps[response.status]);
    }
    // 处理业务逻辑请求错误
    // if (response.data.success !== true){
      // message.error(response.data.msg)
    // }
    // console.log('response...', response);
    return response;
  }],
};