import React, { useEffect, useState } from 'react';
import { Modal } from 'antd';
import ReactDOM from 'react-dom';
import { IArticleItem } from '@/types';
import QRCode from 'qrcode'
import { genePoster } from '@/services/modules/poster';
interface IProps {
    item: IArticleItem;
}
export const Share: React.FC<IProps> = ({ item }) => {
    const [visible, setVisibile] = useState(true);
    const [imgSrc, setImgSrc] = useState('');
    const [downloadSrc, setDownloadSrc] = useState('');

    useEffect(() => {
        if (Object.keys(item).length) {
            console.log('geneQRCode');
            QRCode.toDataURL(`https://blog.wipi.tech/article/${item.category?.id}`)
                .then(url => {
                    console.log('url...', url)
                    setImgSrc(url);
                })
                .catch(err => {
                    console.error('err...', err)
                })
        }
    }, [item]);
    useEffect(() => {
        if (Object.keys(item).length && imgSrc) {
            const html = ``
            genePoster({
                "name": item.title,
                "html": document.querySelector('.ant-modal-body')?.innerHTML!,
                "width": 391,
                "height": 861,
                "pageUrl": `/article/${item.id}`
            }).then(res => {
                setDownloadSrc(res.data.url);
            })
        }
    }, [item, imgSrc]);
    function okClick() {
        window.location.href = downloadSrc;
    }
    function cacelClick() {
        setVisibile(false)
        ReactDOM.unmountComponentAtNode(document.querySelector('#shareDialog')!);
    }
    return (

        <Modal
            className="wang"
            visible={visible}
            title="分享海报"
            okText="下载"
            onOk={okClick}
            onCancel={cacelClick}
            bodyStyle={{ height: '100%'}}
        >
            {item.cover && <img src={item.cover} />}
            <p>{item.title}</p>
            <p>{item.summary}</p>
            <div>
                <img src={imgSrc} alt="" />
                <div>
                    <p>识别二维码查看文章</p>
                    <p>原文分享自
                        <a href={`https://blog.wipi.tech/article/${item.id}`}>小楼又清风</a>
                    </p>
                </div>
            </div>

        </Modal>

    );
}

export default function share(item: IArticleItem) {
    let shareDilaog = document.querySelector('#shareDialog');
    if (!shareDilaog) {
        shareDilaog = document.createElement('div');
        shareDilaog.id = 'shareDialog';
        document.body.appendChild(shareDilaog);
    }
    ReactDOM.render(<Share item={item} />, shareDilaog);
}
