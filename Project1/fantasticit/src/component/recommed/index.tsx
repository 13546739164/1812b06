import { IRootState } from '@/types';
import { Link, useDispatch, useSelector } from 'umi';
import styles from './index.less'; // 等于启用了css-module
import moment from '@/component/moment'

interface Props{
    detail(id:string):void
}
const Recommed:React.FC<Props>=(props)=> {
    const dispatch = useDispatch();
    const article = useSelector((state: IRootState) => state.article);
    console.log(article.articleTitles, '++++++++++++++++++++++++++++++++++++++');

    return (
        <div className={styles.right_top}>
            <p>推荐阅读</p>
            <div>
                {
                    article.recommend.map((item, index) => {
                        return <div
                            style={{color:'black'}}
                            onClick={()=>props.detail(item.id)}
                            key={item.id}
                        >
                            <div className={styles.detail_list}>{item.title}&nbsp;·&nbsp;<span style={{ color: '#999' }}>{moment(item.createAt, 'YYYYMMDD').fromNow()}</span></div>
                        </div>
                    })
                }
            </div>

        </div>
    )
}

export default Recommed;
