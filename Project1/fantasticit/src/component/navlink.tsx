import { articleNav } from '@/assets/data'
import React from 'react'
import { NavLink } from 'umi'

function index() {
    return (<>
        {
              articleNav.map((item,index)=>{
                 return <NavLink
                 to={item.path}
                 key={index}
                 >{item.text}</NavLink>
              })
            }
    </>)
}

export default index
