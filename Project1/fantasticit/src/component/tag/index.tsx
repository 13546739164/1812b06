import React from 'react'
import styles from './index.less'
import { useSelector } from 'umi';
import { IRootState } from '@/types';
interface Props{
    tagDetail(type:string):void
}
function index(props:Props) {

    const article = useSelector((state: IRootState) => state.article);
    return (
        <div className={styles.right_bottom}>
          <p>文章标签</p>
          <div className={styles.right_tags}>
            {
              article.articleTags.map((item, index) => {
                return <li
                  onClick={() => props.tagDetail(item.value)}
                  key={item.id}
                >
                  <span>{item.label}</span>
                  <span>[{item.articleCount}]</span>
                </li>
              })
            }
          </div>
        </div>
    )
}

export default index
