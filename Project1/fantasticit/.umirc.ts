import { defineConfig } from 'umi';
// const px2rem = require('postcss-px2rem');
export default defineConfig({
  nodeModulesTransform: {
    type: 'none',
  },
  fastRefresh: {},
  // 引入antd
  antd: {},
  // 引入dva
  dva: {
    immer: true,
    hmr: true,
  },
  locale: {},
  
  // 配置rem
  // extraPostCSSPlugins: [px2rem({remUnit: 75})],
  // scripts: ["//g.tbcdn.cn/mtb/lib-flexible/0.3.4/??flexible_css.js,flexible.js"],
  

  // 加入百度统计

    analytics: {
      ga: 'google analytics code',
      baidu: '8853402dbb3df12b6e2a548c11fdbd22'
    },

 
  // 文件名加上hash后缀
  // hash: true,
  // // 加入百度统计
  // analytics: {
  //   baidu: 'b7a0296bf19580a2cab8bb41ec0d0d4f'
  // },
  // 响应式
  // extraPostCSSPlugins: [px2rem({remUnit: 75})],

    // 自适应http或https，上吊自愿的自愿的请求协议，以//开头
  scripts: ["//g.tbcdn.cn/mtb/lib-flexible/0.3.4/??flexible_css.js,flexible.js"],  
  publicPath: process.env.NODE_ENV === 'production' ? '/1812B/anmurong/fantasticit/' : '/',
  // 服务器上的路有前缀
  // base:"/1812B/anmurong/fantasticit"
  base:process.env.NODE_ENV === 'production' ? '/1812B/anmurong/fantasticit': '/',
  // 按需加载
  dynamicImport: {
    loading: '@/component/Loading',
  },
  
});


