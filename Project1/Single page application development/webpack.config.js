const path=require('path')
module.exports={
    output:{
        publicPath:path.resolve(__dirname,'')
    },
    devServer:{
        port:999,
        open: true,
        // 404重定向到index.html
        historyApiFallback: true
    }
}
