# 许超杰
### 2021.9.26
1. 文章阅读  
2. 源码阅读  
3. LeeCode刷题  
- [字符串中的第一个唯一字符](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/xn5z8r/)
- [反转字符串](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/xnhbqj/)
4. 项目进度  
- 页面排版

## 2021.9.25
1. 文章阅读
- 看看面试题
2. 源码阅读
3. leecode 刷题
- [合并两个有序数组](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/xnumcr/)
4. 项目进度
- 页面排版

## 2021.9.24
1. 文章阅读
2. leetcode 刷题
3. 源码阅读
4. 项目进度  
- 页面排版
## 2021.9.23
1. 文章阅读
2. 源码阅读
3. leecode 刷题
### [合并两个有序数组](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/xnumcr/)
4. 项目进度
-- 页面排版
## 2021.9.22
1. 文章阅读
- 面试题阅读
2. 源码阅读
3. leecode 刷题
4. 项目进度
- 页面排版
## 2021.9.21
1. 文章阅读
- 面试题阅读
2. 源码阅读
3. leecode 刷题
4. 项目进度
- 无
---
## 2021.9.17
1. 文章阅读
- 面试题阅读
2. 源码阅读
3. leecode 刷题
4. 项目进度
- 我的页面
---
## 2021.9.16
1. 文章阅读
- 面试题阅读
2. 源码阅读
3. leecode 刷题
4. 项目进度
- tab列表传参，排版进行中
---
## 2021.9.15
1. 文章阅读
- 面试题阅读
2. 源码阅读
3. leecode 刷题
4. 项目进度
- 写了Tab的标题，tab列表还在尝试传参，获取数据
---
## 2021.9.14
1. 文章阅读
- 面试题阅读
2. 源码阅读
3. leecode 刷题
4. 项目进度
- 数据没获取到，再找方法
---
## 2021.9.13
1. 文章阅读
- [uni-app官网](https://uniapp.dcloud.io/vue-basics)
2. 源码阅读
3. leecode 刷题
4. 项目进度
- 轮播图
---
## 2021.9.12
1. 文章阅读
- [使用UmiJS框架开发React](https://juejin.cn/post/6950584624108011551)
2. 源码阅读
- [bind-new.js](https://github.com/jasonandjay/js-code/blob/master/original/bind-new.js)
3. LeeCode刷题
    无
4. 项目进度
## 2021.9.10
1. 文章阅读
### [响应式编程 —— RxJava 高阶指南--试读前两篇](https://juejin.cn/book/6844723714538340359)
2. 源码阅读
- 暂无
3. leecode 刷题
### [打乱数组--不太懂](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/xn6gq1/)
4. 项目进度
- 支付
- SSO练习
---
## 2021.9.9
1. 文章阅读
### [基于 JavaScript 开发灵活的数据应用--试读第十，第十一篇](https://juejin.cn/book/6844733715671695374)
2. 源码阅读
- 暂无
3. leecode 刷题
- [整数反转](https://leecode-cn.com/leetbook/read/top-interview-questions-easy/xnx13t/)
4. 项目进度
---
## 2021.9.8
1. 文章阅读
### [基于 JavaScript 开发灵活的数据应用--试读第一，第五篇](https://juejin.cn/book/6844733715671695374)
2. 源码阅读
- 暂无
3. leecode 刷题
### [三数之和](https://leetcode-cn.com/leetbook/read/top-interview-questions-medium/xvpj16/)
4. 项目进度
- 看着录屏练习了一下邮件
---
## 2021.9.7
1. 文章阅读
### [Vue 商城项目开发实战--试读两篇](https://juejin.cn/book/6844733826191589390)
2. 源码阅读
- 暂无
3. leecode 刷题
### [排序算法全解析](https://leetcode-cn.com/leetbook/detail/sort-algorithms/)
4. 项目进度
- 标签页排版完成
---
## 2021.9.6
1. 文章阅读
### [从入门到深入：IM聊天系统前端开发实践--试读两篇](https://juejin.cn/book/6844733781945876488)
2. 源码阅读
- 暂无
3. leecode 刷题
4. 项目进度
- 
---
## 2021.9.5
1. 文章阅读
- [Docker 部署](https://juejin.cn/book/6844733825164148744/section/6844733825226899463)
2. 源码阅读
- 暂无
3. leecode 刷题
- [  最后一个单词的长度](https://leetcode-cn.com/problems/length-of-last-word/)
4. 项目进度
- 
## 2021.9.3
1. 文章阅读
- [基于 NodeJS 构建 GraphQL API ](https://juejin.cn/book/6844733825164148744/section/6844733825214332942)
2. 源码阅读
- 暂无
3. leecode 刷题
- [ 最接近的三数之和](https://leetcode-cn.com/problems/3sum-closest/)
- [  搜索插入位置](https://leetcode-cn.com/problems/search-insert-position/)
4. 项目进度
- 分类页和标签页写完
---
## 2021.9.2
1. 文章阅读
- [基于 NodeJS 构建 GraphQL API 服务--前两节](https://juejin.cn/book/6844733825164148744)
2. leetcode 刷题
- 面试题
3. 源码阅读
- 暂无
4. 项目进度  
- 完善文章页
- 排版分类管理
## 2021.9.1
1. 文章阅读
- [栈内存与堆内存，深浅拷贝](https://juejin.cn/post/6875859084600410119)
2. 源码阅读
- 
3. LeeCode刷题
- [整数反转](https://leecode-cn.com/leetbook/read/top-interview-questions-easy/xnx13t/)
4. 项目进度
- 排版
## 2021.8.31
1. 文章阅读
- [通用篇 - 代码块的使用](https://juejin.cn/book/6844733783166418958/section/6844733783208361992)
2. leetcode 刷题
- 暂无
3. 源码阅读
- 暂无
4. 项目进度  
- 表格排版 
---
## 2021.8.30
1. 文章阅读
- [30分钟精通React Hooks](https://juejin.cn/post/6844903709927800846)
2. leetcode 刷题
- [买卖股票的最佳时机 II](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/x2zsx1/)
3. 源码阅读
- 暂无
4. 项目进度  
- 排版 
---
## 2021.8.28-8.29
1. 文章阅读
- 看看面试题
2. 源码阅读
### [hooks源码阅读]
3. leecode 刷题
4. 项目进度
- 后端页面路由
- 页面排版
---
## 2021.8.27
1. 文章阅读
- 看看面试题
2. 源码阅读
### [hooks源码阅读]
3. leecode 刷题
4. 项目进度
- 
---
## 2021.8.26
1. 文章阅读
- [优秀的前端团队是如何炼成的--试读前两章](https://juejin.cn/book/6844733800379842574)
2. 源码阅读
### [hooks源码阅读]
3. leecode 刷题
4. 项目进度
- 看录屏
- 练习老师写的路由
---
## 2021.8.25
1. 文章阅读
- [umi官网](https://umijs.org/zh-CN)
2. 源码阅读
### [hooks源码阅读]
3. leecode 刷题
4. 项目进度
- 知识小册点赞。
- 优化页面功能
---
## 2021.8.24
1. 文章阅读
2. 源码阅读
### [hooks源码阅读]
3. leecode 刷题
4. 项目进度
- 知识小册点赞。写了一天没写出来
- 优化页面功能
---
## 2021.8.23
1. 文章阅读
### [大厂 H5 开发实战手册--试读两篇](https://juejin.cn/book/6844733712102326279)
2. 源码阅读
- 暂无
3. leecode 刷题
### [合并两个有序数组](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/xnumcr/)
4. 项目进度
- 知识小册第二详情页上一页，下一页跳转。
- 优化页面功能
---
## 2021.8.22
1. 文章阅读
### [响应式编程 —— RxJava 高阶指南--试读前两篇](https://juejin.cn/book/6844723714538340359)
2. 源码阅读
- 暂无
3. leecode 刷题
### [打乱数组--不太懂](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/xn6gq1/)
4. 项目进度
- 知识小册第二层详情页排版。
- rem排版
---
## 2021.8.20
1. 文章阅读
### [前端工程师进阶 10 日谈--试读前三日](https://juejin.cn/book/6891929939616989188)
2. 源码阅读
- 暂无
3. leecode 刷题
### [合并两个有序数组](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/xnumcr/)
4. 项目进度
- 知识小册第二层详情页排版。
- 详情页顶部修改，显示两层ID
---
## 2021.8.19
1. 文章阅读
### [写给普通人看的网页开发课](https://juejin.cn/book/6902816312620220427)
2. 源码阅读
- 暂无
3. leecode 刷题
### [删除链表中的节点](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/xnarn7/)
4. 项目进度
- 知识小册第二层详情页排版与渲染。
---
## 2021.8.18
1. 文章阅读
### [webpack官网](https://webpack.docschina.org/)
2. 源码阅读
- 暂无
3. leecode 刷题
### [删除链表中的节点](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/xnarn7/)
4. 项目进度
- 知识小册第一层详情页排版与渲染。
---
## 2021.8.17
1. 文章阅读
### [Git 原理详解及实用指南](https://juejin.cn/book/6844733697996881928)
2. 源码阅读
- 暂无
3. leecode 刷题
4. 项目进度
- 知识小册页面渲染。
---
## 2021.8.16
1. 文章阅读
### [你不知道的 Chrome 调试技巧](https://juejin.cn/book/6844733783166418958/section/6844733783187390477)
2. 源码阅读
- 暂无
3. leecode 刷题
### [反转字符串](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/xnhbqj/)
### [整数反转](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/xnx13t/)
4. 项目进度
- 知识页的框架排版。
---
## 2021.8.15
1. 文章阅读
### [你不知道的 Chrome 调试技巧](https://juejin.cn/book/6844733783166418958/section/6844733783187390477)
2. 源码阅读
- 暂无
3. leecode 刷题
### [两数之和](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/x2jrse/)
### [有效的数独](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/x2f9gg/)
4. 项目进度
- 配置路由和高亮，搭建头部和尾部框架
---
## 2021.8.13
1. 文章阅读
### [你不知道的 Chrome 调试技巧](https://juejin.cn/book/6844733783166418958/section/6844733783187390477)
2. 源码阅读
- 暂无
3. leecode 刷题
### [加一](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/x2cv1c/)
### [移动零](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/x2ba4i/)
4. 项目进度
- 根据老师写的数据进行部分排版
---
## 2021.8.12
1. 文章阅读
 ### [你不知道的 Chrome 调试技巧](https://juejin.cn/book/6844733783166418958/section/6844733783187390477)
- Chrome 开发者工具  快捷键 Ctrl + Shift + I (Windows) 或 Cmd + Opt + I (Mac)
### 元素面板  
元素面板可以自由的操作 DOM 和 CSS 来迭代布局和设计页面
- 检查和调整页面
- 编辑样式
- 编辑 DOM
### console控制台
shell 在页面上与 JavaScript 交互
- 使用控制台面板
- 命令行交互
### 源面板  
设置断点来调试 JavaScript ，或者通过 Workspaces（工作区）连接本地文件来使用开发者工具的实时编辑器
- 断点调试
- 调试混淆的代码
- 使用开发者工具的 Workspaces（工作区）进行持久化保存
2. 源码阅读
- 暂无
3. leecode 刷题
### [删除排序数组中的重复项](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/x2gy9m/)
- removeDuplicates 删除重复项
### [买卖股票的最佳时机](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/x2zsx1/)
- 用处不大
### [旋转数组](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/x2skh7/)
### [存在重复元素](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/x248f5/)
### [只出现一次的数字](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/x21ib6/)
4. 项目进度
- 明确每人项目分工。
---
## 2021.8.11
1. 文章阅读
- [你不知道的Chrome调试工具技巧 第一天：console中的 '$'](https://juejin.cn/post/6844903732874854414)
- [chrome开发者工具各种骚技巧](https://juejin.cn/post/6844903604839514125)
2. 源码阅读
- git 部分代码尝试
3. leecode 刷题
- 暂无
4. 项目进度
- 暂无
---
# 李廉旭  
## 2021.9.17
1. 文章阅读
- 面试题阅读
2. 源码阅读
3. leecode 刷题
- [ 最接近的三数之和](https://leetcode-cn.com/problems/3sum-closest/)
- [ 旋转图像](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/xnhhkv/)
4. 项目进度
- 我的页面
---
## 2021.9.16
1. 文章阅读
- 面试题阅读
2. 源码阅读
3. leecode 刷题
4. 项目进度
- tab列表传参，排版进行中
---
## 2021.9.15
1. 文章阅读
- 面试题阅读
2. 源码阅读
3. leecode 刷题
4. 项目进度
- 写了Tab的标题，tab列表还在尝试传参，获取数据
---
## 2021.9.14
1. 文章阅读
- 面试题阅读
- 
2. 源码阅读
 - [  最后一个单词的长度](https://leetcode-cn.com/problems/length-of-last-word/)
3. leecode 刷题
4. 项目进度
- 数据没获取到
---
## 2021.9.13
1. 文章阅读
- [基于 NodeJS 构建 GraphQL API ](https://juejin.cn/book/6844733825164148744/section/6844733825214332942)
2. 源码阅读
- 暂无
3. leecode 刷题
- [实现 strStr()](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/xnr003/)
- [外观数列](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/xnpvdm/)
4. 项目进度
-暂无
## 2021.9.12
1. 文章阅读
### [大厂 H5 开发实战手册--试读两篇](https://juejin.cn/book/6844733712102326279)
2. 源码阅读
- 暂无
3. leecode 刷题
### [合并两个有序数组](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/xnumcr/)
4. 项目进度
- 支付练习
## 2021.9.10
1. 文章阅读
- [30分钟精通React Hooks](https://juejin.cn/post/6844903709927800846)
2. leetcode 刷题
- [删除排序数组中的重复项](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/x2gy9m/)   
3. 源码阅读
- 暂无
4. 项目进度  
- 
---
## 2021.9.8
1. 文章阅读
- [border 属性](https://www.w3school.com.cn/cssref/pr_border-style.asp)
2. 源码阅读
- 暂无
3. leecode 刷题
- [  最小栈](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/xnkq37/)
4. 项目进度
- 配路由
-评论页面
-评论页面通过未通过  
-评论页面删除功能 
## 2021.9.7
1. 文章阅读
- [Docker 部署](https://juejin.cn/book/6844733825164148744/section/6844733825226899463)
2. 源码阅读
- 暂无
3. leecode 刷题
- [  最后一个单词的长度](https://leetcode-cn.com/problems/length-of-last-word/)
4. 项目进度
- 配路由
-评论页面
-评论页面通过未通过  
-评论页面删除功能 
## 2021.9.6
1. 文章阅读
- [基于 NodeJS 构建 GraphQL API ](https://juejin.cn/book/6844733825164148744/section/6844733825214332942)
2. 源码阅读
- 暂无
3. leecode 刷题
- [ 质数](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/xnzlu6/)
- [ 三的幂](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/xnsdi2/)
4. 项目进度
- 配路由
-评论页面
-评论页面通过未通过  
-评论页面删除功能   
## 2021.9.5
1. 文章阅读
- [基于 NodeJS 构建 GraphQL API ](https://juejin.cn/book/6844733825164148744/section/6844733825214332942)
2. 源码阅读
- 暂无
3. leecode 刷题
- [ 最接近的三数之和](https://leetcode-cn.com/problems/3sum-closest/)
- [ 旋转图像](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/xnhhkv/)
4. 项目进度
- 配路由
-评论页面
-评论页面通过未通过  
-评论页面删除功能  
## 2021.9.3
1. 文章阅读
- [基于 NodeJS 构建 GraphQL API ](https://juejin.cn/book/6844733825164148744/section/6844733825214332942)
2. 源码阅读
- 暂无
3. leecode 刷题
- [ 最接近的三数之和](https://leetcode-cn.com/problems/3sum-closest/)
- [  搜索插入位置](https://leetcode-cn.com/problems/search-insert-position/)
4. 项目进度
- 配路由
-评论页面
-评论页面通过未通过  
-评论页面删除功能
## 2021.9.2
1. 文章阅读
- [基于 NodeJS 构建 GraphQL API 服务--前两节](https://juejin.cn/book/6844733825164148744)
2. leetcode 刷题
- 面试题
3. 源码阅读
- 暂无
4. 项目进度  
- 配路由
-评论页面
-评论页面通过未通过  
-评论页面删除功能   
## 2021.9.1
1. 文章阅读
- [使用UmiJS框架开发React](https://juejin.cn/post/6950584624108011551)
2. 源码阅读
- [bind-new.js](https://github.com/jasonandjay/js-code/blob/master/original/bind-new.js)
3. LeeCode刷题
    无
4. 项目进度
- 配路由
-评论页面
-评论页面通过未通过  
-评论页面删除功能
## 2021.8.31
1. 文章阅读
- [使用UmiJS框架开发React](https://juejin.cn/post/6950584624108011551)
2. 源码阅读
- [bind-new.js](https://github.com/jasonandjay/js-code/blob/master/original/bind-new.js)
3. LeeCode刷题
- [打乱数组](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/xn6gq1/)  
- [最小栈](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/xnkq37/)  
4. 项目进度
- 配路由
-评论页面
## 2021.8.30
1. 文章阅读
- [使用UmiJS框架开发React](https://juejin.cn/post/6950584624108011551)
2. 源码阅读
- [bind-new.js](https://github.com/jasonandjay/js-code/blob/master/original/bind-new.js)
3. LeeCode刷题
- [爬楼梯](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/xn854d/)  
- [打家窃舍](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/xnq4km/)  
4. 项目进度
- 配路由
## 2021.8.29
1. 文章阅读
- [使用UmiJS框架开发React](https://juejin.cn/post/6950584624108011551)
2. 源码阅读
- [bind-new.js](https://github.com/jasonandjay/js-code/blob/master/original/bind-new.js)
3. LeeCode刷题
- [买卖股票的最佳时机 II](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/x2zsx1/)
4. 项目进度
- 配路由
## 2021.8.27
1. 文章阅读
- [栈内存与堆内存，深浅拷贝](https://juejin.cn/post/6875859084600410119)
2. 源码阅读
- [bind-new.js](https://github.com/jasonandjay/js-code/blob/master/original/bind-new.js)
3. LeeCode刷题
- [整数反转](https://leecode-cn.com/leetbook/read/top-interview-questions-easy/xnx13t/)
4. 项目进度
- 
## 2021.8.26
1. 文章阅读
- [30分钟精通React Hooks](https://juejin.cn/book/6844733783166418958/section/6844733783216750605)
- [元素面板篇 - 颜色选择器](https://juejin.cn/book/6844733783166418958/section/6844733783220944909)
2. 源码阅读
- 暂无
3. leecode 刷题
- [回文数](https://leetcode-cn.com/problems/palindrome-number/)
- [寻找两个正序数组的中位数](https://leetcode-cn.com/problems/median-of-two-sorted-arrays/)
4. 项目进度
- [x] 项目需求到位
- [x] 首页开发
- [x] 路由配置
- [x] 归档页开发
---
## 2021.8.25
1. 文章阅读
- [30分钟精通React Hooks](https://juejin.cn/book/6844733783166418958/section/6844733783216750605)
- [元素面板篇 - 颜色选择器](https://juejin.cn/book/6844733783166418958/section/6844733783220944909)
2. 源码阅读
- 暂无
3. leecode 刷题
- [只出现一次的数字](https://leetcode-cn.com/problems/palindrome-number/)
- [寻找两个正序数组的中位数](https://leetcode-cn.com/problems/median-of-two-sorted-arrays/)
- [买卖股票的最佳时机 II](https://leetcode-cn.com/problems/string-to-integer-atoi/)
4. 项目进度
- 项目需求到位
---
 ### 2021.8.24  
1.文章阅读  
- [从零开发H5可视化搭建项目](https://juejin.cn/book/6930553086918262798/)
2.源码阅读  
* ### [html 元素](https://www.w3school.com.cn/tags/index.asp)
3.LeeCode刷题  
- [合并两个有序数组](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/xnumcr/)
- [爬楼梯](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/xn854d/)
4.项目进度  
- 搭框架  
- 关于页面渲染  
- 评论弹窗功能 
 ### 2021.8.23 
1.文章阅读  
- [从零开发H5可视化搭建项目](https://juejin.cn/book/6930553086918262798/)
2.源码阅读  
* ### [打乱数组](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/xn6gq1/)
3.LeeCode刷题  
- [合并两个有序数组](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/xnumcr/)
- [爬楼梯](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/xn854d/)
4.项目进度  
- 搭框架  
- 关于页面渲染  
- 评论弹窗功能 
 ### 2021.8.22  
1.文章阅读  
- [从零开发H5可视化搭建项目](https://juejin.cn/book/6930553086918262798/)
2.源码阅读  
* ### [html 元素](https://www.w3school.com.cn/tags/index.asp)
3.LeeCode刷题  
- [实现 strStr()](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/xnr003/)
- [外观数列](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/xnpvdm/)
4.项目进度  
- 搭框架  
- 关于页面渲染  
- 评论弹窗功能 
 ### 2021.8.20  
1. 文章阅读  
[从零开发H5可视化搭建项目](https://juejin.cn/book/6930553086918262798/)
2. 源码阅读  
* ### [html 元素](https://www.w3school.com.cn/tags/index.asp)
3. LeeCode刷题  
- [有效的字母异位词](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/xn96us/)
- [验证回文串](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/xne8id/)
4. 项目进度  
- 搭框架  
- 关于页面渲染  
- 评论弹窗功能  
 ### 2021.8.19  
1. 文章阅读  
- [从零开发H5可视化搭建项目](https://juejin.cn/book/6930553086918262798/)
2. 源码阅读  
* ### [html 元素](https://www.w3school.com.cn/tags/index.asp)
3. LeeCode刷题  
- [整数反转](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/xnx13t/)
- [字符串中的第一个唯一字符](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/xn5z8r/)
4. 项目进度  
- 搭框架  
- 关于页面渲染  
- 评论弹窗功能
 ### 2021.8.18  
1. 文章阅读  
- [从零开发H5可视化搭建项目](https://juejin.cn/book/6930553086918262798/)
2. 源码阅读  
* ### [html 元素](https://www.w3school.com.cn/tags/index.asp)
3. LeeCode刷题  
- [字符串中的第一个唯一字符](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/xn5z8r/)
- [反转字符串](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/xnhbqj/)
4. 项目进度  
- 搭框架  
- 关于页面渲染  
- 评论弹窗功能 
## 2021.08.17
1. 文章阅读
- [栈内存与堆内存，深浅拷贝](https://juejin.cn/post/6875859084600410119)
2. 源码阅读
- [bind-new.js](https://github.com/jasonandjay/js-code/blob/master/original/bind-new.js)
3. LeeCode刷题
- [整数反转](https://leecode-cn.com/leetbook/read/top-interview-questions-easy/xnx13t/)
4. 项目进度
- [x]排版
- [x]跳详情
 ### 2021.8.16  
1.文章阅读  
- (从零开发H5可视化搭建项目)[https://juejin.cn/book/6930553086918262798]
2.源码阅读  
* ### [react-redux 源码](https://www.jianshu.com/p/b039a062e021)
3.LeeCode刷题  
- [加一](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/x2cv1c/)
- [移动零](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/x2ba4i/)

4.项目进度  
- 搭框架  
- 关于页面渲染
 ### 2021.8.15  
1.文章阅读  
- [从零开发H5可视化搭建项目](https://juejin.cn/book/6930553086918262798)
2.源码阅读  
* ### [react-redux 源码](https://www.jianshu.com/p/b039a062e021)
3.LeeCode刷题  
- [只出现一次的数字](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/x21ib6/)
- [两个数组的交集 II](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/x2y0c2/)

4.项目进度  
暂无
### 2021.8.13   
1.文章阅读  
- [从零开发H5可视化搭建项目](https://juejin.cn/book/6930553086918262798)
2.源码阅读  
* ### [JavaScript中各种源码实现](https://blog.csdn.net/weixin_39878646/article/details/110723482)
3.LeeCode刷题  
- [旋转数组](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/x2skh7/)
- [存在重复元素](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/x248f5/)
4.项目进度 
- 暂无 
---
 ### 2021.8.12   
1. 文章阅读  
- [你不知道的 Chrome 调试技巧]('https://juejin.cn/book/6844733783166418958/section/6844733783187390477)
2.源码阅读  
- ### [JavaScript中各种源码实现(一)](https://blog.csdn.net/weixin_39878646/article/details/110723482)
3.LeeCode刷题  
- [删除排序数组中的重复项](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/x2gy9m/)
- [买卖股票的最佳时机](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/x2zsx1/)
4.项目进度  
- 暂无
---
## 2021.8.11
1. 文章阅读
- [你不知道的Chrome调试工具技巧 第一天：console中的 '$'](https://juejin.cn/post/6844903732874854414)
- [chrome开发者工具各种骚技巧](https://juejin.cn/post/6844903604839514125)
2. 源码阅读
* ### [JavaScript中各种源码实现(一)](https://blog.csdn.net/weixin_39878646/article/details/110723482)
3. leecode 刷题
- 暂无
4. 项目进度
- 暂无
---
=======
# 王宝玉  

## 2021.9.3
1. 文章阅读
- [基于 NodeJS 构建 GraphQL API 服务--前两节](https://juejin.cn/book/6844733825164148744)
2. leetcode 刷题
- 面试题
3. 源码阅读
* ### [JavaScript中各种源码实现(一)](https://blog.csdn.net/weixin_39878646/article/details/110723482)
4. 项目进度  
- 完善项目

## 2021.9.2
1. 文章阅读

- [从路由到 vue-router 源码，带你吃透前端路由](https://juejin.cn/post/6942520773156438023)

2. 源码阅读

- [call-apply实现](https://github.com/jasonandjay/js-code/blob/master/original/call-apply.js)

3. leecode 刷题

- [翻转字符串里的单词](https://leecode-cn.com/leetbook/read/array-and-string/crmp5/)

4. 项目进度

 ### 2021.9.1
1. 文章阅读  
- [从零开发H5可视化搭建项目](https://juejin.cn/book/6930553086918262798/)
2. 源码阅读  
* ### [html 元素](https://www.w3school.com.cn/tags/index.asp)
3. LeeCode刷题  
- [字符串中的第一个唯一字符](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/xn5z8r/)
- [反转字符串](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/xnhbqj/)
4. 项目进度  

- 关于页面渲染  
- 删除搜索功能

## 2021.8.31
1. 文章阅读
- 看看面试题
2. 源码阅读
### [hooks源码阅读]
3. leecode 刷题
4. 项目进度
- 页面排版

## 2021.8.30
1. 文章阅读
- [栈内存与堆内存，深浅拷贝](https://juejin.cn/post/6875859084600410119)
2. leetcode 刷题
3. 源码阅读
- [React源码解析(一):组件的实现与挂载](https://juejin.cn/post/6844903504528556040)
4. 项目进度  
## 2021.8.29
1. 文章阅读
- [使用UmiJS框架开发React](https://juejin.cn/post/6950584624108011551)
2. 源码阅读
3. leecode 刷题
### [合并两个有序数组](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/xnumcr/)
4. 项目进度
-
## 2021.8.27
1. 文章阅读
- [HTML5 游戏开发快速提升](https://juejin.cn/book/6901095904892321800)
2. leetcode 刷题
- 面试题vue
- [两数之和](https://leetcode-cn.com/problems/two-sum/)
3. 源码阅读
- [hooks](https://github.com/jasonandjay/tranining/blob/master/React%20Hooks/hooks/src/views/useState.js)
4. 项目进度  
- [x] `小楼又春风`
---
## 2021.08.26
1. 文章阅读
- [栈内存与堆内存，深浅拷贝](https://juejin.cn/post/6875859084600410119)
2. 源码阅读
- [深入分析Promise](https://juejin.cn/post/6945319439772434469)
3. LeeCode刷题
- [删除组中的重复项](https://leetcode-cn.com/problems/remove-duplicates-from-sorted-array/)
4. 项目进度
- [x]排版
- [x]跳详情
- [x]国际化
- [x]请求加载
- [x]图片预览
## 2021.8.25
1. 文章阅读
### [你不知道的 Chrome 调试技巧](https://juejin.cn/book/6844733783166418958/section/6844733783187390477)
2. 源码阅读
- 暂无
3. leecode 刷题
### [两数之和](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/x2jrse/)
### [有效的数独](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/x2f9gg/)
4. 项目进度
## 2021.08.24
1. 文章阅读
- [如何写一本掘金小册](https://juejin.cn/book/6844723704639782920)
2. 源码阅读
- [React源码解析(一):组件的实现与挂载](https://juejin.cn/post/6844903504528556040)
3. LeeCode刷题
4. 项目进度
- 
## 2021.08.23
1. 文章阅读
- [栈内存与堆内存，深浅拷贝](https://juejin.cn/post/6875859084600410119)
2. 源码阅读

3. LeeCode刷题
- [删除组中的重复项](https://leetcode-cn.com/problems/remove-duplicates-from-sorted-array/)
4. 项目进度
- [x] 排版
- [x] 跳详情
- [x] 场景切换
- [x] 排版 

## 2021.08.22
1. 文章阅读
- [ES5、Es6数组方法](https://juejin.cn/post/6945996368880091173)
2. 源码阅读
- [深入分析Promise](https://juejin.cn/post/6945319439772434469)
3. LeeCode刷题
4.项目进度
- [x] 详情页
- [x] 场景切换
- [x] 排版 
## 2021.08.20
1. 文章阅读
- [如何写一本掘金小册](https://juejin.cn/book/6844723704639782920)
2. 源码阅读
- [React源码解析(一):组件的实现与挂载](https://juejin.cn/post/6844903504528556040)
3. LeeCode刷题
4. 项目进度
- [x] 详情页
- [x] 场景切换
- [x] 排版  
## 2021.08.19
1. 文章阅读
- [JS原型与原型链](https://www.jianshu.com/p/dee9f8b14771)
2. 源码阅读
- [React源码解析(一):组件的实现与挂载](https://juejin.cn/post/6844903504528556040)
3. LeeCode刷题
4. 项目进度
- [x] 详情页
- [x] 场景切换
- [x] 排版  
## 2021.08.18
1. 文章阅读
- [栈内存与堆内存，深浅拷贝](https://juejin.cn/post/6875859084600410119)
2. 源码阅读
3. LeeCode刷题
- [整数反转](https://leecode-cn.com/leetbook/read/top-interview-questions-easy/xnx13t/)
4. 项目进度
- [x]排版
- [x]跳详情
- [x]国际化
- [x]请求加载
- [x]图片预览
## 2021.08.17
1. 文章阅读
- [Es6 对象解构的用法与用途](https://juejin.cn/post/6941597799121158157)
2. 源码阅读
- [React.children.map](https://blog.csdn.net/hjc256/article/details/99943772)
- [React-使用循环并实现删除和修改](https://blog.csdn.net/Candy_mi/article/details/90726577?utm_term=mapreact%E5%88%A0%E9%99%A4&utm_medium=distribute.pc_aggpage_search_result.none-task-blog-2~all~sobaiduweb~default-2-90726577&spm=3001.4430)
3. LeeCode 刷题
4. 项目进度
- [x]排版
- [x]跳详情
=======
## 2021.08.16
1. 文章阅读
- [console 篇 - console 中的 '$'](https://juejin.cn/book/6844733783166418958/section/6844733783208361991)
- [console 篇 - console.log 的 "bug" ?](https://juejin.cn/book/6844733783166418958/section/6844733783208361998)
2. 源码阅读
- 暂无
3. leecode 刷题
- [只出现一次的数字](https://leecode-cn.com/leetbook/read/top-interview-questions-easy/x21ib6/)
- [删除组中的重复项](https://leetcode-cn.com/problems/remove-duplicates-from-sorted-array/)
4. 项目进度
- 排版
- 跳详情
## 2021.08.15
1. 文章阅读
- [通用篇 - 代码块的使用](https://juejin.cn/book/6844733783166418958/section/6844733783208361992)
- [console 篇 - console 中的 '$'](https://juejin.cn/book/6844733783166418958/section/6844733783208361991)
- [console 篇 - console.log 的 "bug" ?](https://juejin.cn/book/6844733783166418958/section/6844733783208361998)
2. 源码阅读
- 暂无
3. leecode 刷题
- [删数相加](https://leetcode-cn.com/problems/add-two-numbers/)
- [删除组中的重复项](https://leetcode-cn.com/problems/remove-duplicates-from-sorted-array/)
- [字符串转换整数 (atoi)](https://leetcode-cn.com/problems/string-to-integer-atoi/)
4. 项目进度
暂无
## 2021.08.13
1. 文章阅读
- [React-Router使用总结](https://juejin.cn/post/6844903566553923591)
- [JavaScript 中哪一种循环最快呢？](https://juejin.cn/post/6930973929452339213)
2. 源码阅读
- [Swiper](https://github.com/nolimits4web/swiper/blob/master/src/react/swiper.js)
3. LeeCode刷题
- [位数1的个数](https://leecode-cn.com/problems/number-of-1-bits/)
4. 项目进度
## 2021.8.12
1. 文章阅读
- [这样入门js抽象语法树(AST)](https://juejin.cn/post/6942016231214055454)
2. 源码阅读
- [Vue源码为什么v-for的优先级比v-if的高？](https://juejin.cn/post/6941995130144587789)
3. LeeCode刷题
- [只出现一次的数字](https://leecode-cn.com/leetbook/read/top-interview-questions-easy/x21ib6/)
4. 项目进度
- 暂无
---
## 2021.8.11 
1. 文章阅读
- [怎么理解VUE，VUE的数据驱动原理是什么，解释MVVM框架](https://blog.csdn.net/AN0692/article/details/79209004)
- [Es6 对象解构的用法与用途](https://juejin.cn/post/6941597799121158157)
2. 源码阅读
- [JavaScript中各种源码实现(一)](https://blog.csdn.net/weixin_39878646/article/details/110723482)
3. LeeCode刷题
- 暂无
4. 项目进度
- [x] git仓库创建
- [x] git环境
- [x] git命令
---
# 1812B 安慕荣
---
## 2021.9.22
1. 文章阅读
- 
2. leetcode 刷题
- 无
3. 源码阅读
- 

4. 项目进度[一面而就](排版)
---
## 2021.9.21
1. 文章阅读
- 
2. leetcode 刷题
- 无
3. 源码阅读
- 

4. 项目进度[小程序](大牌特惠)
---
## 2021.9.17
1. 文章阅读
- 
2. leetcode 刷题
- 无
3. 源码阅读
- 

4. 项目进度[vue3,小程序](todolist,回到顶)
---
## 2021.9.16
1. 文章阅读
- 
2. leetcode 刷题
- 无
3. 源码阅读
- 

4. 项目进度 [微信小程序]（瀑布流）
---
## 2021.9.15
1. 文章阅读
-  [小程序第三方框架对比 ( wepy / mpvue / taro )]（https://tech.meituan.com/2017/04/27/vuex-code-analysis.html?utm_source=pocket_mylist）
2. leetcode 刷题
- 无
3. 源码阅读
- 
4. 项目进度 [微信小程序](首页，头部地址)
---
## 2021.9.14
1. 文章阅读
- [小程序第三方框架对比 ( wepy / mpvue / taro )]（https://tech.meituan.com/2017/04/27/vuex-code-analysis.html?utm_source=pocket_mylist）
2. leetcode 刷题
- 无
3. 源码阅读
- [vue源码](https://github1s.com/vuejs/vuex/tree/v3.6.2)

4. （项目进度 ）[微信小程序]（源码）
---
## 2021.9.13
1. 文章阅读
- 
2. leetcode 刷题
- 无
3. 源码阅读
- 
4. 项目进度 [微信小程序]（红烨找房）
---
## 2021.9.12
1. 文章阅读
- [uni-app官网](https://uniapp.dcloud.io/)
- [小程序第三方框架对比 ( wepy / mpvue / taro )
](https://juejin.cn/post/6844903767976984589)
2. leetcode 刷题
- 无
3. 源码阅读
- 暂无
4. 项目进度 [微信小程序]
---
## 2021.9.10
1. 文章阅读
- [Octal escape sequences are not allowed in strict mode.](https://so.csdn.net/so/search?q=Octal%20escape%20sequences%20are%20not%20allowed%20in%20strict%20mode.&t=&u=)
- 
2. leetcode 刷题
- [两个数组的交集]（https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/x2y0c2/）
3. 源码阅读
- 暂无
4. 项目进度 [文件上传，切片上传]
---
## 2021.9.9
1. 文章阅读
- [浅谈React合成事件](https://juejin.cn/post/6991645668934680584)
- 
2. leetcode 刷题
- [只出现一次的数字]（https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/x21ib6/）
3. 源码阅读
- 暂无
4. 项目进度 [支付]
---
## 2021.9.8
1. 文章阅读
- 面试题
- 
2. leetcode 刷题
- [存在重复元素]（https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/x248f5/）
3. 源码阅读
- 暂无
4. 项目进度 《工作台》[邮件]
---
## 2021.9.7
1. 文章阅读
- 面试题
-
2. leetcode 刷题
-  [存在重复元素]（https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/x248f5/）
3. 源码阅读
- 暂无
4. 项目进度 
---
## 2021.9.6
1. 文章阅读
- 面试题
- [旋转数组]（https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/x2skh7/）
2. leetcode 刷题
- 无
3. 源码阅读
- 暂无
4. 项目进度 
---
## 2021.9.5
1. 文章阅读
- 面试题  （html5）
- [语雀]（https://www.yuque.com/heinan/luckbody/degc0c）
2. leetcode 刷题
- 无
3. 源码阅读
- 暂无
4. 项目进度  《工作台》[评论的渲染]
-
---
## 2021.9.3
1. 文章阅读
- 面试题
2. leetcode 刷题
- 无
3. 源码阅读
- 暂无
4. 项目进度  《工作台》[实现文章的渲染]
---
## 2021.9.2
1. 文章阅读
- 面试题
- [hook中ref使用]（https://blog.csdn.net/weixin_33842328/article/details/93169197?ops_request_misc=%257B%2522request%255Fid%2522%253A%2522163057169216780262590563%2522%252C%2522scm%2522%253A%252220140713.130102334..%2522%257D&request_id=163057169216780262590563&biz_id=0&utm_medium=distribute.pc_search_result.none-task-blog-2~all~baidu_landing_v2~default-1-93169197.first_rank_v2_pc_rank_v29&utm_term=hooks%E9%87%8C%E7%9A%84ref&spm=1018.2226.3001.4187）
2. leetcode 刷题
- 无
3. 源码阅读
- 暂无
4. 项目进度  《工作台》《聊天室》[socket，编辑器中的抽屉]
---
## 2021.9.1
1. 文章阅读
- 面试题
- [Mobx React  最佳实践]（https://juejin.cn/post/6844903538280103950）
2. leetcode 刷题
- 无
3. 源码阅读
- 暂无
4. 项目进度  《工作台》[ 工作台跳转]
---
## 2021.8.31
1. 文章阅读
- 面试题
- [Mobx React  最佳实践]（https://juejin.cn/post/6844903538280103950）
2. leetcode 刷题
- 无
3. 源码阅读
- 暂无
4. 项目进度  《工作台》[login]
---
## 2021.8.30
1. 文章阅读
- 无
2. leetcode 刷题
- 无
3. 源码阅读
- 暂无
4. 项目进度  《工作台》[login]
---
## 2021.8.29
1. 文章阅读
- [HTML5 游戏开发快速提升](https://juejin.cn/book/6901095904892321800)
2. leetcode 刷题
- 面试题html
3. 源码阅读
- 暂无
4. 项目进度  《工作台》
---
## 2021.8.27
1. 文章阅读
- [HTML5 游戏开发快速提升](https://juejin.cn/book/6901095904892321800)
2. leetcode 刷题
- 面试题vue
3. 源码阅读
- 暂无
4. 项目进度  
---
## 2021.8.26
1. 文章阅读
-  [单页面应用](https://jasonandjay.github.io/study/zh/standard/Spa.html)
2. leetcode 刷题
3. 源码阅读
4. 项目进度  
- [x] `路由、支付宝`
---
## 2021.8.25
1. 文章阅读
-  [资源缓存](https://jasonandjay.github.io/study/zh/standard/Cache.html#%E8%B5%84%E6%BA%90%E8%AF%B7%E6%B1%82%E5%88%86%E7%B1%BB)
2. leetcode 刷题
3. 源码阅读
4. 项目进度  
- [x] `小楼又春风`
---
## 2021.8.24
1. 文章阅读
- [HTML5 游戏开发快速提升](https://juejin.cn/book/6901095904892321800)
2. leetcode 刷题
- 面试题vue
- [两数之和](https://leetcode-cn.com/problems/two-sum/)
3. 源码阅读
- [hooks](https://github.com/jasonandjay/tranining/blob/master/React%20Hooks/hooks/src/views/useState.js)
4. 项目进度  
- [x] `小楼又春风`
---
## 2021.8.23
1. 文章阅读
- [HTML5 游戏开发快速提升](https://juejin.cn/book/6901095904892321800)
2. leetcode 刷题
- 面试题vue
- [两数之和](https://leetcode-cn.com/problems/two-sum/)
3. 源码阅读
- [hooks](https://github.com/jasonandjay/tranining/blob/master/React%20Hooks/hooks/src/views/useState.js)
4. 项目进度  
- [x] `小楼又春风`
---
## 2021.8.22
1. 文章阅读
- [HTML5 游戏开发快速提升](https://juejin.cn/book/6901095904892321800)
2. leetcode 刷题
- 面试题vue
- [两数之和](https://leetcode-cn.com/problems/two-sum/)
3. 源码阅读
- 暂无
4. 项目进度  
- [x] `小楼又春风`mboard[回调渲染]
---
## 2021.8.20
1. 文章阅读
- [基于 NodeJS 构建 GraphQL API 服务--前两节](https://juejin.cn/book/6844733825164148744)
2. leetcode 刷题
- 面试题vue
3. 源码阅读
- 暂无
4. 项目进度  
- 拆分组件
## 2021.8.19
1. 文章阅读
- [HTML5 游戏开发快速提升](https://juejin.cn/book/6901095904892321800)
2. leetcode 刷题
- 面试题vue
3. 源码阅读
- 暂无
4. 项目进度  
- [x] `小楼又春风`mboard[数据添加]
---
## 2021.8.18
1. 文章阅读
- [HTML5 游戏开发快速提升](https://juejin.cn/book/6901095904892321800)
2. leetcode 刷题
- 面试题vue
3. 源码阅读
- 暂无
4. 项目进度  
- [x] `小楼又春风`mboard[数据添加]
---
## 2021.8.17
1. 文章阅读
- [HTML5 游戏开发快速提升](https://juejin.cn/book/6901095904892321800)
2. leetcode 刷题
- 无
3. 源码阅读
- 暂无
4. 项目进度  
- [x] `小楼又春风`mboard[弹框+分页]
---
## 2021.8.16
1. 文章阅读
- [umi路由篇](https://juejin.cn/post/6885978659845308423)
2. leetcode 刷题
- 无
3. 源码阅读
- 暂无
4. 项目进度  
- [x] `小楼又春风`mboard
---
## 2021.8.14 & 8.15
1. 文章阅读
- [使用UmiJS框架开发React](https://juejin.cn/post/6950584624108011551)
2. leetcode 刷题
- [买卖股票的最佳时机 II](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/x2zsx1/)
3. 源码阅读
- 暂无
4. 项目进度  
- [x] `小楼又春风`recommend 
- [x] `小楼又春风`arcitile
---
## 2021.8.13
1. 文章阅读
- [项目实战-UmiJS开发](https://juejin.cn/post/6901103416651546637)
2. leetcode 刷题
- [买卖股票的最佳时机 II](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/x2zsx1/)
3. 源码阅读
- 暂无
4. 项目进度  
- [x] `小楼又春风`基本配置
---
## 2021.8.12
1. 文章阅读
- [30分钟精通React Hooks](https://juejin.cn/post/6844903709927800846)
2. leetcode 刷题
- [删除排序数组中的重复项](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/x2gy9m/)   
- [买卖股票的最佳时机 II](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/x2zsx1/)
3. 源码阅读
- 暂无
4. 项目进度  
- [x] yarn 下载
- [x] Umijs 下载
- [x] hooks 了解
---
## 2021.8.11
1. 文章阅读
- [你不知道的 Chrome 调试技巧](https://juejin.cn/book/6844733783166418958/section/6844733783187390477)
2. leetcode 刷题
- 暂无    
3. 源码阅读
- 暂无
4. 项目进度
- [x] git仓库创建
- [x] git环境
- [x] git命令
---
# 王昭昭

## 2021.9.9
1. 文章阅读
- [基于 NodeJS 构建 GraphQL API 服务--前两节](https://juejin.cn/book/6844733825164148744)
2. leetcode 刷题
- 面试题
3. 源码阅读
* ### [JavaScript中各种源码实现(一)](https://blog.csdn.net/weixin_39878646/article/details/110723482)
4. 项目进度  
- 完善项目
## 2021.9.8
1. 文章阅读
- [优秀的前端团队是如何炼成的--试读前两章](https://juejin.cn/book/6844733800379842574)
2. 源码阅读
### [hooks源码阅读]
3. leecode 刷题
4. 项目进度
- 看录屏
- 练习老师写的邮件发送
---
## 2021.9.7
1. 文章阅读
### [写给普通人看的网页开发课](https://juejin.cn/book/6902816312620220427)
2. 源码阅读
* ### [html 元素](https://www.w3school.com.cn/tags/index.asp)
- 
3. leecode 刷题
### [删除链表中的节点](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/xnarn7/)
4. 项目进度
- 知识小册第二层详情页排版与渲染。
---
 ### 2021.9.6 
1. 文章阅读  
- [从零开发H5可视化搭建项目](https://juejin.cn/book/6930553086918262798/)
2. 源码阅读  
* ### [html 元素](https://www.w3school.com.cn/tags/index.asp)
3. LeeCode刷题  
- [字符串中的第一个唯一字符](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/xn5z8r/)
- [反转字符串](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/xnhbqj/)
4. 项目进度  
- 搭框架  
- 关于页面渲染  
- 评论弹窗功能 
----
## 2021.9.5
1. 文章阅读
- [怎么理解VUE，VUE的数据驱动原理是什么，解释MVVM框架](https://blog.csdn.net/AN0692/article/details/79209004)
- [Es6 对象解构的用法与用途](https://juejin.cn/post/6941597799121158157)
2. 源码阅读
- [JavaScript中各种源码实现(一)](https://blog.csdn.net/weixin_39878646/article/details/110723482)
3. LeeCode刷题
- 暂无
4. 项目进度
- [x] 小册页面实现抽屉功能
- [x] 增加点击新增按钮
- [x] 实现新增功能
---
## 2021.9.3
1. 文章阅读
- [30分钟精通React Hooks](https://juejin.cn/post/6844903709927800846)
2. leetcode 刷题
- [删除排序数组中的重复项](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/x2gy9m/)   
- [买卖股票的最佳时机 II](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/x2zsx1/)
3. 源码阅读
- 暂无
4. 项目进度  
- [x] 小册管理页面渲染
- [x] 数据获取
- [x] 搜索  增删改查 实现
---

## 2021.9.2
1. 文章阅读
### [大厂 H5 开发实战手册--试读两篇](https://juejin.cn/book/6844733712102326279)
2. 源码阅读
- 暂无
3. leecode 刷题
### [合并两个有序数组](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/xnumcr/)
4. 项目进度
- [x] 项目需求
- [x] 页面管理开发
- [x] 页面管理搜索，下拉列表，重置，新建，增删改查
---
## 2021.9.1
1. 文章阅读
 ### [你不知道的 Chrome 调试技巧](https://juejin.cn/book/6844733783166418958/section/6844733783187390477)
- Chrome 开发者工具  快捷键 Ctrl + Shift + I (Windows) 或 Cmd + Opt + I (Mac)
### 元素面板  
元素面板可以自由的操作 DOM 和 CSS 来迭代布局和设计页面
- 检查和调整页面
- 编辑样式
- 编辑 DOM
### console控制台
shell 在页面上与 JavaScript 交互
- 使用控制台面板
- 命令行交互
### 源面板  
设置断点来调试 JavaScript ，或者通过 Workspaces（工作区）连接本地文件来使用开发者工具的实时编辑器
- 断点调试
- 调试混淆的代码
- 使用开发者工具的 Workspaces（工作区）进行持久化保存
2. 源码阅读
- 暂无
3. leecode 刷题
### [删除排序数组中的重复项](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/x2gy9m/)
- removeDuplicates 删除重复项
### [买卖股票的最佳时机](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/x2zsx1/)
- 用处不大
### [旋转数组](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/x2skh7/)
### [存在重复元素](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/x248f5/)
### [只出现一次的数字](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/x21ib6/)
4. 项目进度
- 明确每人项目分工。
---
## 2021.8.31
1. 文章阅读
- [30分钟精通React Hooks](https://juejin.cn/book/6844733783166418958/section/6844733783216750605)
- [元素面板篇 - 颜色选择器](https://juejin.cn/book/6844733783166418958/section/6844733783220944909)
2. 源码阅读
- 暂无
3. leecode 刷题
- [回文数](https://leetcode-cn.com/problems/palindrome-number/)
- [寻找两个正序数组的中位数](https://leetcode-cn.com/problems/median-of-two-sorted-arrays/)
4. 项目进度
- [x] 项目需求到位
- [x] 文章阅读
- [x] 路由登录
- [x] 归档页开发
---
## 2021.8.30
1. 文章阅读
 ### [你不知道的 Chrome 调试技巧](https://juejin.cn/book/6844733783166418958/section/6844733783187390477)
- Chrome 开发者工具  快捷键 Ctrl + Shift + I (Windows) 或 Cmd + Opt + I (Mac)
### 元素面板  
元素面板可以自由的操作 DOM 和 CSS 来迭代布局和设计页面
- 检查和调整页面
- 编辑样式
- 编辑 DOM
### console控制台
shell 在页面上与 JavaScript 交互
- 使用控制台面板
- 命令行交互
### 源面板  
设置断点来调试 JavaScript ，或者通过 Workspaces（工作区）连接本地文件来使用开发者工具的实时编辑器
- 断点调试
- 调试混淆的代码
- 使用开发者工具的 Workspaces（工作区）进行持久化保存
2. 源码阅读
- 暂无
3. leecode 刷题
### [删除排序数组中的重复项](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/x2gy9m/)
- removeDuplicates 删除重复项
### [买卖股票的最佳时机](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/x2zsx1/)
- 用处不大
### [旋转数组](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/x2skh7/)
### [存在重复元素](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/x248f5/)
### [只出现一次的数字](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/x21ib6/)
4. 项目进度
- 明确每人项目分工。
---
### 2021.8.29
1. 文章阅读  
- [从零开发H5可视化搭建项目](https://juejin.cn/book/6930553086918262798/)
2. 源码阅读  
* ### [打乱数组](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/xn6gq1/)
3. LeeCode刷题  
- [合并两个有序数组](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/xnumcr/)
- [爬楼梯](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/xn854d/)
4. 项目进度  
- 搭框架  
- 关于页面渲染  
- 评论弹窗功能 
-----
## 2021.8.27
1. 文章阅读
- [GraphQL API 介绍](https://juejin.cn/book/6844733825164148744/section/6844733825201733645)
- 
2. 源码阅读
* ### [react-redux 源码](https://www.jianshu.com/p/b039a062e021)
3. leecode 刷题
- [罗马数字转整数](https://leetcode-cn.com/problems/roman-to-integer/)
- [ 移除元素](https://leetcode-cn.com/problems/string-to-integer-atoi/)
4. 项目进度
---
## 2021.8.26
1. 文章阅读
- [通用篇 - 代码块的使用](https://juejin.cn/book/6844733783166418958/section/6844733783208361992)
- [console 篇 - console 中的 '$'](https://juejin.cn/book/6844733783166418958/section/6844733783208361991)
- [console 篇 - console.log 的 "bug" ?](https://juejin.cn/book/6844733783166418958/section/6844733783208361998)
2. 源码阅读
- 暂无
3. leecode 刷题
- [删数相加](https://leetcode-cn.com/problems/add-two-numbers/)
- [删除组中的重复项](https://leetcode-cn.com/problems/remove-duplicates-from-sorted-array/)
- [字符串转换整数 (atoi)](https://leetcode-cn.com/problems/string-to-integer-atoi/)
4. 项目进度
- [x] 学习路由
- [x] 支付宝支付功能
- [x] 自己开发归档页
---
## 2021.8.25
1. 文章阅读
- [你不知道的Chrome调试技巧](https://juejin.cn/book/6844733783166418958)
2. 源码阅读
- 暂无
3. leecode 刷题
- [两数之和](https://https://leetcode-cn.com/problems/two-sum/)
- [删除有序数组中的重复项](https://https://leetcode-cn.com/problems/remove-duplicates-from-sorted-array/)
4. 项目进度
- [x] 班级管理页面布局
- [x] 添加班级 删除班级
- [x] 教室管理页面布局
- [x] 添加教室号 删除教室
- [x] 学生管理页面布局
--- 
## 2021.08.24
1. 文章阅读
- [React-Router使用总结](https://juejin.cn/post/6844903566553923591)
- [JavaScript 中哪一种循环最快呢？](https://juejin.cn/post/6930973929452339213)
2. 源码阅读
- [Swiper](https://github.com/nolimits4web/swiper/blob/master/src/react/swiper.js)
3. LeeCode刷题
- [位数1的个数](https://leecode-cn.com/problems/number-of-1-bits/)
4. 项目进度
---
## 2021.08.23
1. 文章阅读
- [Docker 部署](https://juejin.cn/book/6844733825164148744/section/6844733825226899463)
- [对象存储 ]https://juejin.cn/book/6844733825164148744/section/6844733825218510862)
- [邮件发送 ](https://juejin.cn/book/6844733825164148744/section/6844733825222705166)
2. 源码阅读
- 暂无
3. leecode 刷题
- [ 最接近的三数之和](https://leetcode-cn.com/problems/3sum-closest/)
- [  搜索插入位置](https://leetcode-cn.com/problems/search-insert-position/)
- [  最后一个单词的长度](https://leetcode-cn.com/problems/length-of-last-word/)
4. 项目进度
- [x] 归档页面右侧数渲染
- [x] 点击跳转详情动态id
- [x] 归档页右上册推荐阅读功能模块
- [x] 详情页的分享功能
---
## 2021.08.22
1. 文章阅读
- [基于 NodeJS 构建 GraphQL API ](https://juejin.cn/book/6844733825164148744/section/6844733825214332942)
- [用户注册登录 ](https://juejin.cn/book/6844733825164148744/section/6844733825214316557)
- [第三方注册登录 ](https://juejin.cn/book/6844733825164148744/section/6844733825214332936)
2. 源码阅读
- 暂无
3. leecode 刷题
- [ 最接近的三数之和](https://leetcode-cn.com/problems/3sum-closest/)
- [  搜索插入位置](https://leetcode-cn.com/problems/search-insert-position/)
- [  最后一个单词的长度](https://leetcode-cn.com/problems/length-of-last-word/)
4. 项目进度
- [x] 归档页面右侧数渲染
- [x] 点击跳转详情动态id
- [x] 归档页右上册推荐阅读功能模块
- [x] 详情页的分享功能
---
## 2021.08.20
1. 文章阅读
- [深入浅出TypeScript：从基础知识到类型编程](https://juejin.cn/book/6844733813021491207)
2. 源码阅读
- 暂无
3. leecode 刷题
- [爬楼梯](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/xn854d/)
4. 项目进度
- [x] 归档页面右侧数渲染
- [x] 点击跳转详情动态id
- [x] 归档页面渲染
- [x] 详情页渲染和排版
---
## 2021.08.19
1. 文章阅读
- [console 篇 - console 中的 '$'](https://juejin.cn/book/6844733783166418958/section/6844733783208361991)
- [console 篇 - console.log 的 "bug" ?](https://juejin.cn/book/6844733783166418958/section/6844733783208361998)
2. 源码阅读
- 暂无
3. leecode 刷题
- [只出现一次的数字](https://leecode-cn.com/leetbook/read/top-interview-questions-easy/x21ib6/)
- [删除组中的重复项](https://leetcode-cn.com/problems/remove-duplicates-from-sorted-array/)
4. 项目进度
- 排版
- [x] 归档页面右侧数渲染
- [x] 点击跳转详情动态id
- [x] 归档页面渲染
- [x] 详情页渲染和排版
---
## 2021.8.18
1. 文章阅读
- [开篇：Egg.js 构建 GraphQL Api 服务](https://juejin.cn/book/6844733825164148744/section/6844733825134624776)
- [GraphQL API 介绍](https://juejin.cn/book/6844733825164148744/section/6844733825201733645)
- 
2. 源码阅读
* ### [react-redux 源码](https://www.jianshu.com/p/b039a062e021)
3. leecode 刷题
- [罗马数字转整数](https://leetcode-cn.com/problems/roman-to-integer/)
- [合并两个有序链表](https://leetcode-cn.com/problems/merge-two-sorted-lists/)
- [ 移除元素](https://leetcode-cn.com/problems/string-to-integer-atoi/)
4. 项目进度
- [x] 归档页面右侧数渲染
- [x] 点击跳转详情动态id
- [x] 归档页面渲染
- [x] 详情页渲染和排版
---
## 2021.8.17
1. 文章阅读
- [从零开发H5可视化搭建项目](https://juejin.cn/book/6844733783166418958/section/6844733783216750605)
- [Network 篇 - Network 的骚操作](https://juejin.cn/book/6844733783166418958/section/6844733783216766989)
- 
2. 源码阅读
- 暂无
3. leecode 刷题
- [删除组中的重复项](https://leetcode-cn.com/problems/palindrome-number/)
- [寻找两个正序数组的中位数](https://leetcode-cn.com/problems/median-of-two-sorted-arrays/)
- [删除排序数组中的重复项](https://leetcode-cn.com/problems/string-to-integer-atoi/)
4. 项目进度
- [x] 项目需求到位
- [x] 点击跳转详情
- [x] 归档页面渲染
- [x] 归档页跳转详情
---
## 2021.8.16
1. 文章阅读
- [30分钟精通React Hooks](https://juejin.cn/book/6844733783166418958/section/6844733783216750605)
- [你不知道的 Chrome 调试技巧](https://juejin.cn/book/6844733783166418958/section/6844733783216766989)
4.项目进度  
- 搭框架  
- 关于页面渲染  
- 评论弹窗功能 
## 2021.8.15
1. 文章阅读
- [元素面板篇 - 技巧集合](https://juejin.cn/book/6844733783166418958/section/6844733783216766989)
- [实现 strStr()](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/xnr003/)
- [外观数列](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/xnpvdm/)

4. 项目进度
- 搭框架  
- 关于页面渲染  
- 评论弹窗功能 
- 项目需求到位
---
- 项目需求到位
---
## 2021.8.13
1. 文章阅读
- [通用篇 - 代码块的使用](https://juejin.cn/book/6844733783166418958/section/6844733783208361992)
- [console 篇 - console 中的 '$'](https://juejin.cn/book/6844733783166418958/section/6844733783208361991)
- [console 篇 - console.log 的 "bug" ?](https://juejin.cn/book/6844733783166418958/section/6844733783208361998)
2. 源码阅读
- 暂无
3. leecode 刷题
- [删数相加](https://leetcode-cn.com/problems/add-two-numbers/)
- [删除组中的重复项](https://leetcode-cn.com/problems/remove-duplicates-from-sorted-array/)
- [字符串转换整数 (atoi)](https://leetcode-cn.com/problems/string-to-integer-atoi/)
4. 项目进度
- [x] 搭载umijs
- [x] 首页开发
- [x] 自己开发归档页
- [x] todolist
---
## 2021.8.12
1. 文章阅读
- [通用篇 - copying & saving](https://juejin.cn/book/6844733783166418958/section/6844733783204167693)
- [通用篇 - 快捷键和通用技巧](https://juejin.cn/book/6844733783166418958/section/6844733783204167687)
- [从 Chrome 说起](https://juejin.cn/book/6844733783166418958/section/6844733783187390477)
2. 源码阅读
- 暂无
3. leecode 刷题  
- [两数之和](https://https://leetcode-cn.com/problems/two-sum/)
- [删除有序数组中的重复项](https://https://leetcode-cn.com/problems/remove-duplicates-from-sorted-array/)
4. 项目进度
- [x] 下载yarn
- [x] 完成umijs框架
- [x] 配置依赖包
- [x] hooks
---
## 2021.8.11
1. 文章阅读
- [你不知道的Chrome调试技巧](https://juejin.cn/book/6844733783166418958)
2. 源码阅读
- 暂无
3. leecode 刷题
- [两数之和](https://https://leetcode-cn.com/problems/two-sum/)
- [删除有序数组中的重复项](https://https://leetcode-cn.com/problems/remove-duplicates-from-sorted-array/)
4. 项目进度
- [x] 班级管理页面布局
- [x] 添加班级 删除班级
- [x] 教室管理页面布局
- [x] 添加教室号 删除教室
- [x] 学生管理页面布局
--- 



