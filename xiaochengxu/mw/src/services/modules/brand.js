import request from '@/utils/request';

// 列表标题区
export function getBrandTags(){
    return request.post('/basic/pageConfig/tabs', {showPage:1, platform:0})
}


// 列表区
export function getBrandItem(body={}){
    return request.post('/c_msh/mLife/goods/list/queryByTab', {showPage:1, platform:0, ...body})
}
// 获取所有的数据
export function getAll(body={}) {
    return request.post('/msh/app/index.php?i=5&t=0&v=1.0.1&from=wxapp&c=entry&a=wxapp&do=index&&m=zofui_sales&sign=0031ba39134668c2712cf9d72022c6dc', {
        op: "info",
        mwtoken: "uPCQvKl9RhX5r9SGP2noD0Ei0D3egOAN8IrKQ9lIY-BS0nZ8NKqZIMOel3oNkoUYqt8HcINLZxhDwV7Q4Vh5OTpzjZnUY-B7fswX-C52EwX-CzY-BX-CZCzIRtY8VoCtw18qcuQiO8WcpPSh2U3ZsXghpyUOPKS3xnWdK7xPZCNxpq7ea1wAIFLp68WuKRXAXnLCgQY7t92Yrm1u2ZIbmPpChaX-Ce3QuHcg-Z-Z",
        from: "wxapp",
        lat: 39.90469,
        lng: 116.40717,
        zfid: 0,
        isnew: 1,
        isposter: 0,
        shopid: 0,
        ...body
    },{headers:{'Content-Type':'application/x-www-form-urlencoded'}})
}