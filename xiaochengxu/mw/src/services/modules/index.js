import request from '@/utils/request';

// 首页轮播图
export function getBanner(){
    return request.post('/basic/app/banner')
}

// 操作指南
export function homeWidget(){
    return request.post('/basic/app/homeFunctionWidget')
}

// 操作指南
export function homeOperation(){ 
    return request.post('/basic/app/operatList')
}
// 城市
export function getcity(){ 
    return request.post('/basic/app/getCityList')
}


// 页面tab
export function pageTabs(){
    return request.post('/basic/pageConfig/tabs', {showPage:0, platform:0})
}

// 获取店铺列表
export function getShopsByTab(body={}){
    return request.post('/c_msh/mLife/goods/list/queryByTab', {showPage:0, platform:0, ...body})
}
