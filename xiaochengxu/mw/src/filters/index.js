export function getDistance(distance) {
    return distance.toFixed(2);
}
