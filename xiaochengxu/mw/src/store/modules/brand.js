import {getBrandTags,getBrandItem,getAll} from '@/services'

const state = {
    BrandBanners:[],
    BrandTags: [], 
    BrandItem:[],
    brandQuery: {
        "tabId": 328,
        "showSort": 1,
        "showSortMode": 1,
        "pageNo": 1,
        "pageSize": 10,
    },
    brandCount:0,
    brandAllQ:{
        op: "info",
        mwtoken: "uPCQvKl9RhX5r9SGP2noD0Ei0D3egOAN8IrKQ9lIY-BS0nZ8NKqZIMOel3oNkoUYqt8HcINLZxhDwV7Q4Vh5OTpzjZnUY-B7fswX-C52EwX-CzY-BX-CZCzIRtY8VoCtw18qcuQiO8WcpPSh2U3ZsXghpyUOPKS3xnWdK7xPZCNxpq7ea1wAIFLp68WuKRXAXnLCgQY7t92Yrm1u2ZIbmPpChaX-Ce3QuHcg-Z-Z",
        from: "wxapp",
        lat: 39.90469,
        lng: 116.40717,
        zfid: 0,
        isnew: 1,
        isposter: 0,
        shopid: 0,
    },
    allArr:[],
    navlist:[],
    brandGoods:[],
    brandGoods2:[]
    
};

const mutations = {
    update(state, payload){
        for (let key in payload) {
            state[key] = payload[key];
        }
    }
};

const actions = {
    // 数据标题
    async getBrandTags({commit},payload){
        let result = await getBrandTags();
        if (result.errNo === 0) {
            commit('update', {
                BrandTags: result.data
            })
        }
    },

   // 通过tab获取店铺列表
    async getBrandItem({commit, state}){
        let result = await getBrandItem(state.brandQuery);
        if (result.errNo === 0) {
            let BrandItem = result.data.data;
            if (state.brandQuery.pageNo > 1){
                BrandItem = [...state.BrandItem, ...BrandItem]
            }
            commit('update', {
                BrandItem,
                brandCount: result.data.totalCount
            })
        }
    },
    //所有数据
    async getAll({commit, state}){
        let result = await getAll(state.brandAllQ);
        console.log('1',result.data.page[1].params.data);
        console.log('2',result.data.page[2].params.data);
        console.log('3',result.data.page[3].params.data);
        console.log('4',result.data.page[4].params.data);
        console.log(result);
        if (result.errno === 0) {
            commit('update', {
                allArr:result.data,
                BrandBanners:result.data.page[1].params.data,
                navlist:result.data.page[2].params.data,
                brandGoods:result.data.page[3].params.data,
                brandGoods2:result.data.page[4].params.data
            })
        }
    }
};

export default {
    namespaced: true,
    state,
    mutations,
    actions
}