import {userGet} from '@/services'

const state = {
    userdata:{},
    opp:{
        "checkMobile": 1,
    }
//    redpointD:{}
};

const mutations = {
    update(state, payload){
        for (let key in payload) {
            state[key] = payload[key];
        }
    }
};

const actions = {
    async userGet({commit}, payload){
        let result = await userGet(opp);
        console.log('result....',result);
        if (result.errNo === 0) {
            commit('update', {
                userdata: result.data
            })
        }
    },
  
 
};

export default {
    namespaced: true,
    state,
    mutations,
    actions
}