import {getBanner, homeWidget, homeOperation, pageTabs, getShopsByTab} from '@/services'

const state = {
    banners: [], 
    widgets: [],
    operation: [],
    pageTabs: [],
    bytab:{},
    shops: [],
    shopCount: 0,
    shopQuery: {
        "tabId": 41,
        "showSort": 1,
        "showSortMode": 1,
        "pageNo": 1,
        "pageSize": 10,
    }

};

const mutations = {
    update(state, payload){
        for (let key in payload) {
            state[key] = payload[key];
        }
    }
};

const actions = {
    // 获取轮播图
    async getBanner({commit}, payload){
        let result = await getBanner();
        if (result.errNo === 0) {
            commit('update', {
                banners: result.data
            })
        }
    },
    // 获取首页组件
    async homeWidget({commit}){
        let result = await homeWidget();
        if (result.errNo === 0) {
            commit('update', {
                widgets: result.data.items
            })
        }
    },
    // 获取操作指南
    async homeOperation({commit}){
        let result = await homeOperation();
        if (result.errNo === 0) {
            commit('update', {
                operation: result.data
            })
        }
    },
    
    // 获取页面tab配置
    async pageTabs({commit}){
        let result = await pageTabs();
        if (result.errNo === 0) {
            commit('update', {
                pageTabs: result.data
            })
        }
    },
    // 通过tab获取店铺列表
    async getShopsByTab({commit, state}){
        let result = await getShopsByTab(state.shopQuery);
        if (result.errNo === 0) {
            let shops = result.data.data;
            if (state.shopQuery.pageNo > 1){
                shops = [...state.shops, ...shops]
            }
            commit('update', {
                shops,
                shopCount: result.data.totalCount
            })
        }
    }

};

export default {
    namespaced: true,
    state,
    mutations,
    actions
}