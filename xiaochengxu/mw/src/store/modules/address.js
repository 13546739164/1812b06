import {getcity} from '@/services'

const state = {
    citylist:[]
};

const mutations = {
    update(state, payload){
        for (let key in payload) {
            state[key] = payload[key];
        }
    }
};

const actions = {
    // 获取城市列表
    async getcity({commit}){
        let result = await getcity();
        if (result.errNo === 0) {
            commit('update', {
                citylist: result.data
            })
        }
    }
};

export default {
    namespaced: true,
    state,
    mutations,
    actions
}