import Vuex from 'vuex';
import Vue from 'vue';
import createLogger from 'vuex/dist/logger'
 
// 引入子模块
import my from './modules/my'
import index from './modules/index'
import brand from './modules/brand'
import address from './modules/address'

Vue.use(Vuex);

export default new Vuex.Store({
    modules: {
        my,
        index,
        brand,
        address
    },
    plugins: [createLogger()]
});