import Vuex from 'vuex';
import Vue from 'vue';
import createLogger from 'vuex/dist/logger'
 
// 引入子模块
import my from './modules/my'
import sign from './modules/sign'
Vue.use(Vuex);

export default new Vuex.Store({
    modules: {
        my,
        sign
    },
    plugins: [createLogger()]
});