import {userGet} from '@/services'

const state = {
    userInfo:{},
    isLogin:false
};

const mutations = {
    update(state, payload){
        for (let key in payload) {
            state[key] = payload[key];
        }
    }
};

const actions = {
    async userGet({commit}, payload){
        let result = await userGet(payload);
        if (result.code === 0) {
            commit('update', {
                userInfo: result.data,
                isLogin:true
            })
        }
        // 存储登陆态到本地存储
        // oUy9p5PTxwYaLmwBn41pn0HzNM1U
        wx.setStorageSync('openid','oUy9p5PTxwYaLmwBn41pn0HzNM1U');
    },
  
 
};

export default {
    namespaced: true,
    state,
    mutations,
    actions
}