import {getSignList} from '@/services'

const state = {
    signList: []
};

const mutations = {
    update(state, payload){
        for (let key in payload) {
            state[key] = payload[key];
        }
    }
};

const actions = {
    async getSignList({commit}, payload){
        let result = await getSignList();
        if(result.code==0){
            commit('update',{
                signList:result.data
            })
        }

        
    },
  
 
};

export default {
    namespaced: true,
    state,
    mutations,
    actions
}